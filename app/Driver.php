<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Driver.
 *
 * @author  The scaffold-interface created at 2018-10-08 06:20:03pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Driver extends Model
{
	protected $fillable = [
		'name',
		'address',
		'schedule',
		'salary',
		'phone',
		'cellphone',
		'unity'
	];

	use SoftDeletes;

	protected $dates = ['deleted_at'];


    protected $table = 'drivers';


}
