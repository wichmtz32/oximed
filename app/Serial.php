<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Serial.
 *
 * @author  The scaffold-interface created at 2019-01-30 05:32:11pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Serial extends Model
{
	
	use SoftDeletes;

	protected $dates = ['deleted_at'];
    
	
    protected $table = 'serials';

	
	public function product()
	{
		return $this->belongsTo('App\Product','product_id');
	}

	
}
