<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Payment.
 *
 * @author  The scaffold-interface created at 2018-10-08 06:27:03pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Payment extends Model
{

	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $table = 'payments';

		public function contract()
		{
				return $this->belongsTo(Contract::class);
		}

}
