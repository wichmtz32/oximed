<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Serial;


/**
 * Class Product.
 *
 * @author  The scaffold-interface created at 2018-10-24 04:37:41pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Product extends Model
{
	protected $fillable = [
		'name',
		'cost',
		'costmonth',
		'extraprice1',
		'extraprice2',
		'extraprice3',
		'extraprice4',
		'product_value'
	];

	use SoftDeletes;

	protected $dates = ['deleted_at'];
  protected $table = 'products';

	public function serials()
	{
		$serials = $this->hasMany(Serial::class);
		return $serials;
	}

}
