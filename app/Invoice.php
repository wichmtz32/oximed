<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Invoice.
 *
 * @author  The scaffold-interface created at 2019-05-31 03:37:32pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Invoice extends Model
{
	
	use SoftDeletes;

	protected $dates = ['deleted_at'];
    
	
    protected $table = 'invoices';
}
