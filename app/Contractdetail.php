<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contractdetail.
 *
 * @author  The scaffold-interface created at 2018-11-06 05:12:34pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Contractdetail extends Model
{
	
	use SoftDeletes;

	protected $dates = ['deleted_at'];
    
	
    protected $table = 'contractdetails';

	
	public function contract()
	{
		return $this->belongsTo('App\Contract','contract_id');
	}

	
	public function product()
	{
		return $this->belongsTo('App\Product','product_id');
	}

	
}
