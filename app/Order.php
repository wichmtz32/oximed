<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Repair;
/**
 * Class Order.
 *
 * @author  The scaffold-interface created at 2019-05-27 01:28:38pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Order extends Model
{

	use SoftDeletes;

	protected $dates = ['deleted_at'];


  protected $table = 'orders';

  public function reparaciones()
  {
    return $this->hasMany(Repair::class);
  }



}
