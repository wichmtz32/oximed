<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Repairdetail.
 *
 * @author  The scaffold-interface created at 2019-05-13 02:32:03pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Repairdetail extends Model
{
	
	use SoftDeletes;

	protected $dates = ['deleted_at'];
    
	
    protected $table = 'repairdetails';

	
	public function repair()
	{
		return $this->belongsTo('App\Repair','repair_id');
	}

	
}
