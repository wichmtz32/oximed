<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Contractdetail;
use App\Payment;


/**
 * Class Contract.
 *
 * @author  The scaffold-interface created at 2018-10-08 06:36:26pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Contract extends Model
{
protected $fillable = [
	'date',
	'validity',
	'product_id',
	'extra',
	'status',
	'charge',
	'serie',
	'recolect_date',
	'client',
	'flete',
	'address',
	'phone',
	'cellphone',
	'cost',
	'driver_id',
	'driver_id2',
	'notes'
	];


	use SoftDeletes;

	protected $dates = ['deleted_at'];
	protected $table = 'contracts';

	public function client()
	{
		return $this->belongsTo('App\Client','client_id');
	}

	public function driver()
	{
		return $this->belongsTo('App\Driver','driver_id');
	}
	public function driver2()
	{
		return $this->belongsTo('App\Driver','driver_id2');
	}

	public function payments()
	{
		return $this->hasMany(Payment::class);
	}
	public function products()
	{
		return $this->hasMany(Contractdetail::class);
	}

	public function toArray() {
			$str3 =  date("Y-m-d");
			$str4 = date('Y-m-d', strtotime('+1 days', strtotime($str3)));
      $str5 = date('Y-m-d', strtotime('+2 days', strtotime($str3)));
      $str6 = date('Y-m-d', strtotime('+3 days', strtotime($str3)));
			$today =  new \DateTime($str3." 00:00:00");
			$tomorrow =  new \DateTime($str4." 00:00:00");
			$tomorrow2 =  new \DateTime($str5." 00:00:00");
			$tomorrow3 =  new \DateTime($str6." 00:00:00");



      $data = parent::toArray();
      if(!is_null($this->products())) {
					$details = Contractdetail::where('contract_id', $this->id)->get();
					$i = 0;
					foreach ($details as $d) {
						$data['products'][$i]['id'] = $d->product->id;
						$data['products'][$i]['name'] = $d->product->name;
						$i++;
					}
      } else {
          $data['products'] = null;
      }
			if(!is_null($this->payments())) {
					$payments = Payment::where('contract_id', $this->id)->whereNull('deleted_at')->get();
					$i = 0;
					foreach ($payments as $p) {
							$data['payments'][$i]['id'] = $p->id;
							$data['payments'][$i]['validate'] = $p->validate;
							$data['payments'][$i]['bill'] = $p->bill;
							$i++;
					}
      } else {
          $data['payments'] = null;
      }

			if($this->driver2) {
					$data['driver2_name'] = $this->driver2->name;
			} else {
					$data['driver2_name'] = null;
			}

			$validity = \DateTime::createFromFormat('Y-m-d H:i:s', $this->validity);
			$validity->setTime(0,0,0);

			if($validity == $today || $validity == $tomorrow || $validity == $tomorrow2 || $validity == $tomorrow3)
				$data['threedays'] = 1;
			else
				$data['threedays'] = 0;
      return $data;
  }
}
