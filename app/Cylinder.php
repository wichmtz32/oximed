<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cylinder.
 *
 * @author  The scaffold-interface created at 2019-05-15 04:29:27pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Cylinder extends Model
{
	
	use SoftDeletes;

	protected $dates = ['deleted_at'];
    
	
    protected $table = 'cylinders';

	
}
