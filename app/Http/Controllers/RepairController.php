<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repair;
use Amranidev\Ajaxis\Ajaxis;
use URL;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use App\Contract;
use App\Client;
use App\Driver;
use App\Payment;
use App\Product;
use App\Serial;
use App\Contractdetail;

use App\Repairdetail;
use App\Observation;



use NumeroALetras;

/**
 * Class RepairController.
 *
 * @author  The scaffold-interface created at 2019-05-13 02:23:56pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class RepairController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {


        $title = 'Index - repair';
        $repairs = Repair::all();
        return view('repair.index',compact('repairs','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - repair';

        $clients = Client::all();
        $drivers = Driver::all();
        $products = Product::all();
        $realSerials = array();
        $serials = Serial::all();
        $contracts = Contract::where('status', '!=', 'cancelado')->get();
        foreach ($serials as $s) {
          $band = false;
          foreach ($contracts as $c) {
            foreach ($c->products as $p) {
              if($p->serie == $s->number)
                $band = true;
            }
          }
          if(!$band){
            $arr = [];
            $arr['number'] = $s->number;
            $arr['id'] =   $s->product_id;
            $realSerials = array_merge($realSerials, array($arr));
          }
        }
        $serials = $realSerials;
        $nueva = true;
        $show = false;
        return view('repair.create',compact('title','clients' , 'drivers', 'products', 'nueva', 'show', 'serials'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      $repair = new Repair();
      if(!is_null($request->date))
        $fecha = \DateTime::createFromFormat('j M, Y', $request->date);
      else
        $fecha = new \DateTime();
      if(!is_null($request->fecha2))
      {
        $fecha2 = \DateTime::createFromFormat('j M, Y', $request->fecha2);
        $repair->deliverDate =$fecha2;

        }
      else
        $repair->client = $request->client;
        $repair->date = $fecha;
        $repair->address = $request->address;
        $repair->phone = $request->phone;
        $repair->cellphone = $request->cellphone;
        $repair->product = $request->product;
        $repair->serialNumber = $request->nserie;
        $repair->faults = $request->faults;


        $repair->status = "Espera";
        $repair->repairman = $request->reparador;
        $repair->save();
        if(isset($request->descripcion))
        {
          for ($i=0; $i < count($request->descripcion); $i++) {
            if($request->descripcion[$i] != 'null' || !is_null($request->descripcion[$i])){
              $rd = new Repairdetail();
              $rd->repair_id = $repair->id;

                $rd->status = $request->estatus[$i];
              $rd->description = $request->descripcion[$i];
              $rd->quantity = intval($request->cantidad[$i]);
              $rd->unitPrice = floatval($request->precio[$i]);
              $rd->total = floatval($request->totalp[$i]);
              if(floatval($request->precio[$i]) == 0)
                $rd->warranty=true;
              else
                $rd->warranty=false;
              $rd->save();
            }
          }
        }
        if(isset($request->descripcion1))
        {
          for ($i=0; $i < count($request->descripcion1); $i++) {
            if($request->descripcion1[$i] != 'null' || !is_null($request->descripcion1[$i])){
              $ob = new Observation();
              $ob->repair_id = $repair->id;
              $ob->description = $request->descripcion1[$i];
              if(!is_null($request->fechas4[$i]))
                $fecha = \DateTime::createFromFormat('j M, Y', $request->fechas4[$i]);

              $ob->fecha=$fecha;
              $ob->save();
            }
          }
        }
        return redirect('repair');
    }
    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - repair';

        if($request->ajax())
        {
            return URL::to('repair/'.$id);
        }

        $repair = Repair::findOrfail($id);
        return view('repair.show',compact('title','repair'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Editar - Reparaciones';

        $clients = Client::all();
        $drivers = Driver::all();
        $products = Product::all();
        $realSerials = array();
        $serials = Serial::all();
        $contracts = Contract::where('status', '!=', 'cancelado')->get();

        $repair = Repair::findOrfail($id);

        foreach ($serials as $s) {
          $band = false;
          foreach ($contracts as $c) {
            foreach ($c->products as $p) {
              if($p->serie == $s->number)
                $band = true;
            }
          }
          if(!$band){
            $arr = [];
            $arr['number'] = $s->number;
            $arr['id'] =   $s->product_id;
            $realSerials = array_merge($realSerials, array($arr));
          }
        }
        $serials = $realSerials;
        $nueva = false;
        $show = false;

        return view('repair.create',compact('title','clients' ,'repair','drivers' ,'products', 'nueva', 'show', 'serials'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {



        $repair = Repair::findOrfail($id);
        if(!is_null($request->date))
          $fecha = \DateTime::createFromFormat('j M, Y', $request->date);

        if(!is_null($request->fecha2))
        {
          $fecha2 = \DateTime::createFromFormat('j M, Y', $request->fecha2);
          $repair->deliverDate =$fecha2;
        }

          $repair->client = $request->client;
          //$repair->date = $fecha;
          $repair->address = $request->address;
          $repair->phone = $request->phone;
          $repair->cellphone = $request->cellphone;
          $repair->product = $request->product;
          $repair->serialNumber = $request->nserie;
          $repair->faults = $request->faults;

          $repair->status = "Espera";
          $repair->repairman = $request->reparador;


          $repair->detalles()->delete();
          $repair->observaciones()->delete();
          $repair->save();



          if(isset($request->descripcion))
          {
            for ($i=0; $i < count($request->descripcion); $i++) {
              if($request->descripcion[$i] != 'null' || !is_null($request->descripcion[$i])){
                $rd = new Repairdetail();
                $rd->repair_id = $repair->id;
                if(is_null($request->estatus[$i]))
                    $rd->status = "En espera";
                else
                  $rd->status = $request->estatus[$i];
                $rd->description = $request->descripcion[$i];
                $rd->quantity = intval($request->cantidad[$i]);
                $rd->unitPrice = floatval($request->precio[$i]);
                $rd->total = floatval($request->totalp[$i]);
                if(floatval($request->precio[$i]) == 0)
                  $rd->warranty=true;
                else
                  $rd->warranty=false;
                $rd->save();
              }
            }
          }
          if(isset($request->descripcion1))
          {
            for ($i=0; $i < count($request->descripcion1); $i++) {
              if($request->descripcion1[$i] != 'null' || !is_null($request->descripcion1[$i])){
                $ob = new Observation();
                $ob->repair_id = $repair->id;
                $ob->description = $request->descripcion1[$i];
                if(!is_null($request->fechas4[$i]))
                  $fecha = \DateTime::createFromFormat('j M, Y', $request->fechas4[$i]);

                $ob->fecha=$fecha;
                $ob->save();
              }
            }
          }

        return redirect('repair');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Advertencia!!','Deseas eliminar el registro?','/repair/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$repair = Repair::findOrfail($id);

      $repair->detalles()->delete();
      $repair->observaciones()->delete();

     	$repair->delete();
        return URL::to('repair');
    }
}
