<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contractdetail;
use Amranidev\Ajaxis\Ajaxis;
use URL;

use App\Contract;


use App\Product;


/**
 * Class ContractdetailController.
 *
 * @author  The scaffold-interface created at 2018-11-06 05:12:34pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class ContractdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - contractdetail';
        $contractdetails = Contractdetail::paginate(6);
        return view('contractdetail.index',compact('contractdetails','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - contractdetail';
        
        $contracts = Contract::all()->pluck('date','id');
        
        $products = Product::all()->pluck('name','id');
        
        return view('contractdetail.create',compact('title','contracts' , 'products'  ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contractdetail = new Contractdetail();

        
        $contractdetail->cost = $request->cost;

        
        $contractdetail->product_value = $request->product_value;

        
        $contractdetail->extra = $request->extra;

        
        $contractdetail->serie = $request->serie;

        
        
        $contractdetail->contract_id = $request->contract_id;

        
        $contractdetail->product_id = $request->product_id;

        
        $contractdetail->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new contractdetail has been created !!']);

        return redirect('contractdetail');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - contractdetail';

        if($request->ajax())
        {
            return URL::to('contractdetail/'.$id);
        }

        $contractdetail = Contractdetail::findOrfail($id);
        return view('contractdetail.show',compact('title','contractdetail'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - contractdetail';
        if($request->ajax())
        {
            return URL::to('contractdetail/'. $id . '/edit');
        }

        
        $contracts = Contract::all()->pluck('date','id');

        
        $products = Product::all()->pluck('name','id');

        
        $contractdetail = Contractdetail::findOrfail($id);
        return view('contractdetail.edit',compact('title','contractdetail' ,'contracts', 'products' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $contractdetail = Contractdetail::findOrfail($id);
    	
        $contractdetail->cost = $request->cost;
        
        $contractdetail->product_value = $request->product_value;
        
        $contractdetail->extra = $request->extra;
        
        $contractdetail->serie = $request->serie;
        
        
        $contractdetail->contract_id = $request->contract_id;

        
        $contractdetail->product_id = $request->product_id;

        
        $contractdetail->save();

        return redirect('contractdetail');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/contractdetail/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$contractdetail = Contractdetail::findOrfail($id);
     	$contractdetail->delete();
        return URL::to('contractdetail');
    }
}
