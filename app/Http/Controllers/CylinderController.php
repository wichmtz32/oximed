<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cylinder;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class CylinderController.
 *
 * @author  The scaffold-interface created at 2019-05-15 04:29:30pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class CylinderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - cylinder';
        $cylinders = Cylinder::paginate(6);
        return view('cylinder.index',compact('cylinders','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - cylinder';
        
        return view('cylinder.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cylinder = new Cylinder();

        
        $cylinder->sizeOrWeight = $request->sizeOrWeight;

        
        $cylinder->description = $request->description;

        
        $cylinder->quantity = $request->quantity;

        
        
        $cylinder->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new cylinder has been created !!']);

        return redirect('cylinder');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - cylinder';

        if($request->ajax())
        {
            return URL::to('cylinder/'.$id);
        }

        $cylinder = Cylinder::findOrfail($id);
        return view('cylinder.show',compact('title','cylinder'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - cylinder';
        if($request->ajax())
        {
            return URL::to('cylinder/'. $id . '/edit');
        }

        
        $cylinder = Cylinder::findOrfail($id);
        return view('cylinder.edit',compact('title','cylinder'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $cylinder = Cylinder::findOrfail($id);
    	
        $cylinder->sizeOrWeight = $request->sizeOrWeight;
        
        $cylinder->description = $request->description;
        
        $cylinder->quantity = $request->quantity;
        
        
        $cylinder->save();

        return redirect('cylinder');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/cylinder/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$cylinder = Cylinder::findOrfail($id);
     	$cylinder->delete();
        return URL::to('cylinder');
    }
}
