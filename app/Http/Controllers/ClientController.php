<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class ClientController.
 *
 * @author  The scaffold-interface created at 2018-10-08 06:23:07pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(\Auth::user()->can('c_cliente')){
            $title = 'Index - client';
            $clients = Client::all();
            return view('client.index',compact('clients','title'));
        }
        else {
          session()->flash('message', 'Acceso denegado: No tiene los permisos necesarios');
          return redirect('/');
        }
    }

    public function getclientesajax(Request $request){
      $clients = Client::all();
      return response()->json($clients);
    }

    public function busqueda($busqueda, Request $request){
      $busqueda = "%" . $busqueda . "%";
      $clients = Client::where(function ($query) use($busqueda) {$query->where('address',"LIKE", $busqueda)->orWhere('name','LIKE',$busqueda)->orWhere('suburb','LIKE',$busqueda);})->whereNull('deleted_at')->orderBy('id', 'desc')->get()->toArray();

      return response($clients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - client';
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new Client();
        $client->name = $request->name;
        $client->address = $request->address;
        $client->phone = $request->phone;
        $client->email = $request->email;
        $client->cellphone = $request->cellphone;
        $client->person = $request->person;
        $client->suburb = $request->suburb;
        $client->cp = $request->cp;
        $client->rfc = $request->rfc;
        $client->save();

        session()->flash('message', 'Cliente registrado correctamente');
        return redirect('client');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - client';
        if($request->ajax())
            return URL::to('client/'.$id);

        $client = Client::findOrfail($id);
        return view('client.show',compact('title','client'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - client';
        if($request->ajax())
            return URL::to('client/'. $id . '/edit');

        $client = Client::findOrfail($id);
        return view('client.edit',compact('title','client'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $client = Client::findOrfail($id);
        $client->name = $request->name;
        $client->address = $request->address;
        $client->phone = $request->phone;
        $client->email = $request->email;
        $client->cellphone = $request->cellphone;
        $client->person = $request->person;
        $client->save();
        session()->flash('message', 'Cliente modificado correctamente');
        return redirect('client');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Advertencia!!','Esta apunto de eliminar este registro','/client/'. $id . '/delete');
        if($request->ajax())
            return $msg;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$client = Client::findOrfail($id);
     	$client->delete();
      session()->flash('message', 'Cliente eliminado correctamente');
      return URL::to('client');
    }
}
