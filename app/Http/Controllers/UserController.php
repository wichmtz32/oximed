<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
  public function index()
  {
    if(\Auth::user()->can('c_usuario')){
      $users = User::orderBy('id', 'desc')->paginate(12);
      return view('users.index')->with(['users'=>$users]);
    }
    else {
      session()->flash('message', 'Acceso denegado: No tiene los permisos necesarios');
      return redirect('/');
    }
  }
  public function create()
  {
    $roles = Role::orderBy('id', 'desc')->get();
    return view('users.user')->with([
                        'roles' => $roles,
                        'nueva' => true,
                      ]);
  }
  public function store(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required',
      'correo' => 'email',
      'password' => 'required|min:6|confirmed|string',
    ]);
    session()->flash('message', 'Usuario registrado correctamente');
    $user = User::create([
        'name' => strtoupper($request['nombre']),
        'email' => $request['email'],
        'phone' =>$request['telefono'],
        'password' => bcrypt($request['password']),
    ]);
    $user->assignRole($request['rol']);
    return redirect()->route('users_index');
  }

  public function edit(User $user)
  {
    $roles = Role::orderBy('id', 'desc')->get();
    return view('users.user')->with([
          'user' => $user,
          'roles' => $roles,
          'nueva' => false,
        ]);
  }

  public function update(User $user, Request $request)
  {
      if(!is_null($request['password'])){
        $this->validate($request, [
          'nombre' => 'required',
          'correo' => 'email',
          'password' => 'min:6|confirmed|string',
        ]);
        $user->update([
            'name' => strtoupper($request['nombre']),
            'phone' =>$request['telefono'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);
      }
      else {
        $user->update([
            'name' => strtoupper($request['nombre']),
            'phone' =>$request['telefono'],
            'email' => $request['email'],
        ]);
      }
      session()->flash('message', 'Usuario modificado correctamente');
      $user->roles()->detach();
      $user->assignRole($request['rol']);
      return redirect()->route('users_index');
  }
  public function delete(User $user)
  {
      $user->roles()->detach();
      session()->flash('message', 'Usuario eliminado correctamente');
      $user->delete();
      return redirect()->route('users_index');
  }
  public function find(Request $request)
  {
    $users= User::where('name','like','%'.$request->search.'%')->paginate(8);
    return view('users.index')->with(['users'=>$users]);
  }
}
