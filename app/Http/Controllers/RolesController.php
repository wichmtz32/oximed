<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;


class RolesController extends Controller
{
    public function index()
    {
      if(\Auth::user()->can('c_role')){
        $roles = Role::orderBy('id', 'desc')->paginate(12);
        return view('roles.index')->with(['roles'=>$roles]);
      }
      else{
        session()->flash('message', 'Acceso denegado: No tiene los permisos necesarios');
        return redirect('/');
      }
    }

    public function create()
    {
        return view('roles.role')->with(['nueva'=> true]);
    }
    public function edit(Role $role)
    {
        return view('roles.role')->with(['role' => $role, 'nueva' => false]);
    }
    public function delete(Role $role)
    {

      $users = User::role($role)->get();
      foreach($users as $u)
        $u->roles()->detach();
      $role->revokePermissionTo(Permission::all());
      $role->delete();
      session()->flash('message', 'Role eliminado correctamente');
      return redirect()->route('roles_index');
    }
    public function update(Request $request, Role $role)
    {
        $role->update(['name' => $request['nombre']]);
        if(isset($request['permisos'])){
          $role->syncPermissions($request['permisos']);
        }
        session()->flash('message', 'Role modificado correctamente');
        return redirect()->route('roles_index');
    }
    public function store(Request $request)
    {
      $role = Role::create(['name' => strtoupper($request['nombre'])]);
      if(isset($request['permisos'])){
        $role->givePermissionTo($request['permisos']);
      }
      session()->flash('message', 'Role registrado correctamente');
      return redirect()->route('roles_index');
    }
    public function buscar(Request $request)
    {

    }

}
