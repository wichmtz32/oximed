<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Driver;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class DriverController.
 *
 * @author  The scaffold-interface created at 2018-10-08 06:20:03pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
      if(\Auth::user()->can('c_conductor')){
        $title = 'Index - driver';
        $drivers = Driver::paginate(9);
        return view('driver.index',compact('drivers','title'));
      }
      else {
        session()->flash('message', 'Acceso denegado: No tiene los permisos necesarios');
        return redirect('/');
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
      $title = 'Create - driver';
      return view('driver.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $driver = new Driver();
      $driver->name = $request->name;
      $driver->address = $request->address;
      $driver->schedule = $request->schedule;
      $driver->salary = $request->salary;
      $driver->phone = $request->phone;
      $driver->cellphone = $request->cellphone;


      $driver->save();
      session()->flash('message', 'Conductor registrado correctamente');
      return redirect('driver');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
      $title = 'Show - driver';
      if($request->ajax())
          return URL::to('driver/'.$id);
      $driver = Driver::findOrfail($id);
      return view('driver.show',compact('title','driver'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
      $title = 'Edit - driver';
      if($request->ajax())
          return URL::to('driver/'. $id . '/edit');

      $driver = Driver::findOrfail($id);
      return view('driver.edit',compact('title','driver'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
      $driver = Driver::findOrfail($id);
      $driver->name = $request->name;
      $driver->address = $request->address;
      $driver->schedule = $request->schedule;
      $driver->salary = $request->salary;
      $driver->phone = $request->phone;
      $driver->cellphone = $request->cellphone;
      $driver->save();
      session()->flash('message', 'Conductor modificado correctamente');
      return redirect('driver');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
      $msg = Ajaxis::MtDeleting('Advertencia!!','Esta a punto de eliminar este registro','/driver/'. $id . '/delete');
      if($request->ajax())
          return $msg;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$driver = Driver::findOrfail($id);
     	$driver->delete();
      session()->flash('message', 'Conductor eliminado correctamente');
      return URL::to('driver');
    }
}
