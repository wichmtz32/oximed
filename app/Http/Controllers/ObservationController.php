<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Observation;
use Amranidev\Ajaxis\Ajaxis;
use URL;

use App\Repair;


/**
 * Class ObservationController.
 *
 * @author  The scaffold-interface created at 2019-05-13 02:35:42pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class ObservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - observation';
        $observations = Observation::paginate(6);
        return view('observation.index',compact('observations','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - observation';
        
        $repairs = Repair::all()->pluck('product','id');
        
        return view('observation.create',compact('title','repairs'  ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $observation = new Observation();

        
        $observation->description = $request->description;

        
        
        $observation->repair_id = $request->repair_id;

        
        $observation->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new observation has been created !!']);

        return redirect('observation');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - observation';

        if($request->ajax())
        {
            return URL::to('observation/'.$id);
        }

        $observation = Observation::findOrfail($id);
        return view('observation.show',compact('title','observation'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - observation';
        if($request->ajax())
        {
            return URL::to('observation/'. $id . '/edit');
        }

        
        $repairs = Repair::all()->pluck('product','id');

        
        $observation = Observation::findOrfail($id);
        return view('observation.edit',compact('title','observation' ,'repairs' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $observation = Observation::findOrfail($id);
    	
        $observation->description = $request->description;
        
        
        $observation->repair_id = $request->repair_id;

        
        $observation->save();

        return redirect('observation');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/observation/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$observation = Observation::findOrfail($id);
     	$observation->delete();
        return URL::to('observation');
    }
}
