<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repairdetail;
use Amranidev\Ajaxis\Ajaxis;
use URL;

use App\Repair;


/**
 * Class RepairdetailController.
 *
 * @author  The scaffold-interface created at 2019-05-13 02:32:03pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class RepairdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - repairdetail';
        $repairdetails = Repairdetail::paginate(6);
        return view('repairdetail.index',compact('repairdetails','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - repairdetail';
        
        $repairs = Repair::all()->pluck('product','id');
        
        return view('repairdetail.create',compact('title','repairs'  ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $repairdetail = new Repairdetail();

        
        $repairdetail->unitPrice = $request->unitPrice;

        
        $repairdetail->quantity = $request->quantity;

        
        $repairdetail->total = $request->total;

        
        $repairdetail->status = $request->status;

        
        $repairdetail->warranty = $request->warranty;

        
        
        $repairdetail->repair_id = $request->repair_id;

        
        $repairdetail->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new repairdetail has been created !!']);

        return redirect('repairdetail');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - repairdetail';

        if($request->ajax())
        {
            return URL::to('repairdetail/'.$id);
        }

        $repairdetail = Repairdetail::findOrfail($id);
        return view('repairdetail.show',compact('title','repairdetail'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - repairdetail';
        if($request->ajax())
        {
            return URL::to('repairdetail/'. $id . '/edit');
        }

        
        $repairs = Repair::all()->pluck('product','id');

        
        $repairdetail = Repairdetail::findOrfail($id);
        return view('repairdetail.edit',compact('title','repairdetail' ,'repairs' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $repairdetail = Repairdetail::findOrfail($id);
    	
        $repairdetail->unitPrice = $request->unitPrice;
        
        $repairdetail->quantity = $request->quantity;
        
        $repairdetail->total = $request->total;
        
        $repairdetail->status = $request->status;
        
        $repairdetail->warranty = $request->warranty;
        
        
        $repairdetail->repair_id = $request->repair_id;

        
        $repairdetail->save();

        return redirect('repairdetail');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/repairdetail/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$repairdetail = Repairdetail::findOrfail($id);
     	$repairdetail->delete();
        return URL::to('repairdetail');
    }
}
