<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Serial;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class ProductController.
 *
 * @author  The scaffold-interface created at 2018-10-24 04:37:41pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {

      if(\Auth::user()->can('c_producto')){
        $title = 'Index - product';
        $products = Product::paginate(9);
        return view('product.index',compact('products','title'));
      }
      else {
        session()->flash('message', 'Acceso denegado: No tiene los permisos necesarios');
        return redirect('/');
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $nueva = true;
        return view('product.create',compact('nueva'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product();
        $product->name = $request->name;
        $product->cost = $request->cost;
        $product->costmonth = $request->costmonth;
        $product->extraprice1 = $request->extraprice1;
        $product->extraprice2 = $request->extraprice2;
        $product->extraprice3 = $request->extraprice3;
        $product->extraprice4 = $request->extraprice4;
        $product->product_value = $request->product_value;
        $product->save();

        if(isset($request->serials) && count($request->serials) > 0){
            for ($i=0; $i < count($request->serials); $i++) {
              $serial = new Serial();
              $serial->product_id = $product->id;
              $serial->number = $request->serials[$i];
              $serial->save();
            }
        }
        session()->flash('message', 'Producto registrado correctamente');
        return redirect('product');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - product';

        if($request->ajax())
        {
            return URL::to('product/'.$id);
        }

        $product = Product::findOrfail($id);
        return view('product.show',compact('title','product'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        if($request->ajax())
            return URL::to('product/'. $id . '/edit');
        $product = Product::findOrfail($id);
        $serials = $product->serials();
        $nueva = false;
        return view('product.create',compact('product', 'serials', 'nueva' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $product = Product::findOrfail($id);
        $product->name = $request->name;
        $product->cost = $request->cost;
        $product->costmonth = $request->costmonth;
        $product->extraprice1 = $request->extraprice1;
        $product->extraprice2 = $request->extraprice2;
        $product->extraprice3 = $request->extraprice3;
        $product->extraprice4 = $request->extraprice4;
        $product->product_value = $request->product_value;
        $product->serials()->delete();
        $product->save();
        if(isset($request->serials) && count($request->serials) > 0){
            for ($i=0; $i < count($request->serials); $i++) {
              $serial = new Serial();
              $serial->product_id = $product->id;
              $serial->number = $request->serials[$i];
              $serial->save();
            }
        }

        session()->flash('message', 'Producto modificado correctamente');
        return redirect('product');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Advertencia!!','Esta apunto de eliminar este registro','/product/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$product = Product::findOrfail($id);
     	$product->delete();
      session()->flash('message', 'Producto eliminado correctamente');
        return URL::to('product');
    }
}
