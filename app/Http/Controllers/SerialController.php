<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Serial;
use Amranidev\Ajaxis\Ajaxis;
use URL;

use App\Product;


/**
 * Class SerialController.
 *
 * @author  The scaffold-interface created at 2019-01-30 05:32:11pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class SerialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - serial';
        $serials = Serial::paginate(6);
        return view('serial.index',compact('serials','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - serial';
        
        $products = Product::all()->pluck('name','id');
        
        return view('serial.create',compact('title','products'  ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $serial = new Serial();

        
        $serial->number = $request->number;

        
        
        $serial->product_id = $request->product_id;

        
        $serial->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new serial has been created !!']);

        return redirect('serial');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - serial';

        if($request->ajax())
        {
            return URL::to('serial/'.$id);
        }

        $serial = Serial::findOrfail($id);
        return view('serial.show',compact('title','serial'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - serial';
        if($request->ajax())
        {
            return URL::to('serial/'. $id . '/edit');
        }

        
        $products = Product::all()->pluck('name','id');

        
        $serial = Serial::findOrfail($id);
        return view('serial.edit',compact('title','serial' ,'products' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $serial = Serial::findOrfail($id);
    	
        $serial->number = $request->number;
        
        
        $serial->product_id = $request->product_id;

        
        $serial->save();

        return redirect('serial');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/serial/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$serial = Serial::findOrfail($id);
     	$serial->delete();
        return URL::to('serial');
    }
}
