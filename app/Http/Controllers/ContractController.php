<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contract;
use Amranidev\Ajaxis\Ajaxis;
use URL;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use App\Client;
use App\Driver;
use App\Payment;
use App\Product;
use App\Serial;
use App\Contractdetail;
use NumeroALetras;

use PDF;

/**
 * Class ContractController.
 *
 * @author  The scaffold-interface created at 2018-10-08 06:36:26pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index(){
      if(\Auth::user()->can('c_contrato')){
        $contracts = Contract::all();
        $title = 'Index - contract';
        $products = Product::all();
        return view('contract.index',compact('contracts','title', 'products'));
      }
      else {
        session()->flash('message', 'Acceso denegado: No tiene los permisos necesarios');
        return redirect('/');
      }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - contract';
        $clients = Client::all();
        $drivers = Driver::all();
        $products = Product::all();
        $realSerials = array();
        $serials = Serial::all();
        $contracts = Contract::where('status', '!=', 'cancelado')->get();
        foreach ($serials as $s) {
          $band = false;
          foreach ($contracts as $c) {
            foreach ($c->products as $p) {
              if($p->serie == $s->number)
                $band = true;
            }
          }
          if(!$band){
            $arr = [];
            $arr['number'] = $s->number;
            $arr['id'] =   $s->product_id;
            $realSerials = array_merge($realSerials, array($arr));
          }
        }
        $serials = $realSerials;
        $nueva = true;
        $show = false;
        return view('contract.create',compact('title','clients' , 'drivers', 'products', 'nueva', 'show', 'serials'));
    }

    public function search(Request $request, $busqueda, $status){
      $busqueda = "%".$busqueda."%";
      if($status == 'activo')
        $contracts = Contract::where(function ($query) use($busqueda) {$query->where('address',"LIKE", $busqueda)->orWhere('client','LIKE',$busqueda);})->whereIn('status', [$status, 'recoleccion'])->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
      else
        $contracts = Contract::where(function ($query) use($busqueda) {$query->where('address',"LIKE", $busqueda)->orWhere('client','LIKE',$busqueda);})->where('status', $status)->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();

      return response($contracts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request){

        if(!is_null($request->date))
          $fecha = \DateTime::createFromFormat('j M, Y', $request->date);
        else
          $fecha = new \DateTime();

        if(!is_null($request->recolect_date))
          $fecha_recolecta = \DateTime::createFromFormat('j M, Y', $request->recolect_date);
        else
          $fecha_recolecta = null;

        if(!is_null($request->validity))
          $fecha_vigencia = \DateTime::createFromFormat('j M, Y', $request->validity);
        else
          $fecha_vigencia = null;

        $last = DB::table('contracts')->whereNull('deleted_at')->orderBy('id', 'desc')->first();
        if(is_null($last)){
          $folio = '00001';
        }
        else{
          $folio = intval($last->folio) + 1;
          $folio = str_pad($folio, 5, "0", STR_PAD_LEFT);
        }

        $contract = new Contract();
        $contract->folio = $folio;
        $contract->date = $fecha;
        $contract->validity = $fecha_vigencia;
        $contract->recolect_date = $fecha_recolecta;
        $contract->status = "activo";
        $contract->client = $request->client;
        $contract->address = $request->address;
        $contract->phone = $request->phone;
        $contract->cellphone = $request->cellphone;
        $contract->cost = $request->total;
        $contract->charge = $request->charge;
        $contract->notes = $request->notes;
        $contract->deposit = $request->deposit;
        $contract->flete = $request->flete;
        $contract->user = Auth::user()->name;



        if(!is_null($request->driver_id) && $request->driver_id != 'null')
          $contract->driver_id = intval($request->driver_id);
        if(!is_null($request->driver_id2) && $request->driver_id2 != 'null')
          $contract->driver_id2 = intval($request->driver_id2);
        $contract->save();

        for ($i=0; $i < count($request->product_id); $i++) {
          if($request->product_id[$i] != 'null' || !is_null($request->product_id[$i])){
            $cd = new Contractdetail();
            $cd->contract_id = $contract->id;
            $cd->product_id = intval($request->product_id[$i]);
            $cd->cost = floatval($request->cost[$i]);
            $cd->product_value = floatval($request->product_value[$i]);
            $cd->serie = $request->serie[$i];
            $cd->extra = $request->extra[$i];
            $cd->save();
          }
        }

        session()->flash('message', 'Contrato registrado correctamente');
        return redirect('contract');
    }
    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request){
      $title = 'Show - contract';
      $clients = Client::all();
      $drivers = Driver::all();
      $products = Product::all();
      $contract = Contract::findOrfail($id);
      $realSerials = array();
      $serials = Serial::all();
      $contracts = Contract::where('status', '!=', 'cancelado')->get();
      foreach ($serials as $s) {
        $band = false;
        foreach ($contracts as $c) {
          foreach ($c->products as $p) {
            if($p->serie == $s->number)
              $band = true;
          }
        }
        if(!$band){
          $arr = [];
          $arr['number'] = $s->number;
          $arr['id'] =   $s->product_id;
          $realSerials = array_merge($realSerials, array($arr));
        }
      }

      $serials = $realSerials;

      foreach ($contract->products as $p) {
        if(!is_null($p->serie) && $p->serie != "null"){
          $arr = [];
          $arr['number'] = $p->serie;
          $arr['id'] =   $p->product_id;
          $serials = array_merge($serials, array($arr));
        }
      }

      $nueva = false;
      $show = true;
      return view('contract.create',compact('title','clients' , 'drivers', 'products','nueva', 'contract', 'show', 'serials'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request){
      $title = 'Create - contract';
      $clients = Client::all();
      $drivers = Driver::all();
      $products = Product::all();
      $contract = Contract::findOrfail($id);

      $realSerials = array();
      $serials = Serial::all();
      $contracts = Contract::where('status', '!=', 'cancelado')->get();
      foreach ($serials as $s) {
        $band = false;
        foreach ($contracts as $c) {
          foreach ($c->products as $p) {
            if($p->serie == $s->number)
              $band = true;
          }
        }
        if(!$band){
          $arr = [];
          $arr['number'] = $s->number;
          $arr['id'] =   $s->product_id;
          $realSerials = array_merge($realSerials, array($arr));
        }
      }

      $serials = $realSerials;

      foreach ($contract->products as $p) {
        if(!is_null($p->serie) && $p->serie != "null"){
          $arr = [];
          $arr['number'] = $p->serie;
          $arr['id'] =   $p->product_id;
          $serials = array_merge($serials, array($arr));
        }
      }

      $nueva = false;
      $show = false;

      return view('contract.create',compact('title','clients' , 'drivers', 'nueva', 'products','show','contract','serials'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request){
      $contract = Contract::findOrfail($id);
      if(!is_null($request->recolect_date))
        $fecha_recolecta = \DateTime::createFromFormat('j M, Y', $request->recolect_date);
      else
        $fecha_recolecta = null;
      $fecha_vigencia = \DateTime::createFromFormat('j M, Y', $request->validity);

      $contract->validity = $fecha_vigencia;
      $contract->recolect_date = $fecha_recolecta;
      $contract->client = $request->client;
      $contract->address = $request->address;
      $contract->phone = $request->phone;
      $contract->cellphone = $request->cellphone;
      $contract->cost = $request->total;
      $contract->charge = $request->charge;
      $contract->notes = $request->notes;
      $contract->deposit = $request->deposit;
      $contract->flete = $request->flete;


      if(!is_null($request->driver_id) && $request->driver_id != 'null')
        $contract->driver_id = intval($request->driver_id);

      if(!is_null($request->driver_id2) && $request->driver_id2 != 'null')
        $contract->driver_id2 = intval($request->driver_id2);

      $contract->products()->delete();
      $contract->save();

      for ($i=0; $i < count($request->product_id); $i++) {
        if($request->product_id[$i] != 'null' || !is_null($request->product_id[$i])){
          $cd = new Contractdetail();
          $cd->contract_id = $contract->id;
          $cd->product_id = intval($request->product_id[$i]);
          $cd->cost = floatval($request->cost[$i]);
          $cd->product_value = floatval($request->product_value[$i]);
          $cd->serie = $request->serie[$i];
          $cd->extra = $request->extra[$i];
          $cd->save();
        }
      }


      session()->flash('message', 'Contrato modificado correctamente');
      return redirect('contract');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Advertencia!!','Esta a punto de eliminar este registro','/contract/'. $id . '/delete');
        if($request->ajax())
            return $msg;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id){
     	$contract = Contract::findOrfail($id);
     	$contract->delete();
      session()->flash('message', 'Contrato eliminado correctamente');
      return URL::to('contract');
    }
    public function getcontratosajax($status, $producto){
      $str3 =  date("Y-m-d");
      $str4 = date('Y-m-d', strtotime('+1 days', strtotime($str3)));
      $str5 = date('Y-m-d', strtotime('+2 days', strtotime($str3)));
      $str6 = date('Y-m-d', strtotime('+3 days', strtotime($str3)));
      $str7 = date('Y-m-d', strtotime('-1 days', strtotime($str3)));


      $yesterday =  new \DateTime($str7." 00:00:00");
      $today =  new \DateTime($str3." 00:00:00");
      $tomorrow =  new \DateTime($str4." 00:00:00");
      $tomorrow2 =  new \DateTime($str5." 00:00:00");
      $tomorrow3 =  new \DateTime($str6." 00:00:00");



      if($producto == 'null' && $status != 'porvencer'){
        if($status == 'activo')
          $contracts = Contract::where(function ($query) use($status) {$query->where('status', $status)->orWhere('status','recoleccion');})->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
        else{
          if($status == 'vencidos'){
            $all =  Contract::where('status', 'activo')->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
            $contracts = array();
            for ($i=0; $i < count($all); $i++) {
              $validity = \DateTime::createFromFormat('Y-m-d H:i:s', $all[$i]['validity']);
              $validity->setTime(0,0,0);
              if(($validity == $yesterday ||  strtotime($validity->format('Y-m-d H:i:s')) < strtotime($today->format('Y-m-d H:i:s')) ) && $all[$i]['status'] != 'cancelado')
                array_push($contracts, $all[$i]);
            }
          }
          else
            $contracts = Contract::where('status', $status)->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
        }
      }
      elseif($producto != 'null' && $status != 'porvencer'){
          if($status == 'activo')
            $all = Contract::where(function ($query) use($status) {$query->where('status', $status)->orWhere('status','recoleccion');})->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
          else{
            if($status == 'vencidos'){
              $all2 = Contract::where('status', 'activo')->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
              $all = array();
              for ($i=0; $i < count($all2); $i++) {
                $validity = \DateTime::createFromFormat('Y-m-d H:i:s', $all2[$i]['validity']);
                $validity->setTime(0,0,0);
                if(($validity == $yesterday ||  strtotime($validity->format('Y-m-d H:i:s')) < strtotime($today->format('Y-m-d H:i:s')) ) && $all2[$i]['status'] != 'cancelado')
                  array_push($all, $all2[$i]);
              }
            }
            else
              $all = Contract::where('status', $status)->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
          }

          $contracts = array();
          for ($i=0; $i < count($all); $i++) {
            for ($j=0; $j < count($all[$i]['products']); $j++) {
              if(intval($all[$i]['products'][$j]['id']) == intval($producto)){
                array_push($contracts, $all[$i]);
                break;
            }
          }
        }
      }

      elseif($producto == 'null' && $status == 'porvencer'){
        $all = Contract::whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
        $contracts = array();
        for ($i=0; $i < count($all); $i++) {
          $validity = \DateTime::createFromFormat('Y-m-d H:i:s', $all[$i]['validity']);
          $validity->setTime(0,0,0);

          if(($validity == $today || $validity == $tomorrow || $validity == $tomorrow2 || $validity == $tomorrow3) && $all[$i]['status'] != 'cancelado')
            array_push($contracts, $all[$i]);
          }
      }
      elseif($producto != 'null' && $status == 'porvencer'){
        $all = Contract::whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
        $contracts = array();
        for ($i=0; $i < count($all); $i++) {
          $validity = \DateTime::createFromFormat('Y-m-d H:i:s', $all[$i]['validity']);
          $validity->setTime(0,0,0);
          if(($validity == $today || $validity == $tomorrow || $validity == $tomorrow2 || $validity == $tomorrow3) && $all[$i]['status'] != 'cancelado')
          for ($j=0; $j < count($all[$i]['products']); $j++) {
            if(intval($all[$i]['products'][$j]['id']) == intval($producto)){
              array_push($contracts, $all[$i]);
            }
          }
        }
      }



      return response($contracts);
    }

    public function printContracts(Request $request, $status, $producto){
      $str3 =  date("Y-m-d");
      $str4 = date('Y-m-d', strtotime('+1 days', strtotime($str3)));
      $str5 = date('Y-m-d', strtotime('+2 days', strtotime($str3)));
      $str6 = date('Y-m-d', strtotime('+3 days', strtotime($str3)));
      $str7 = date('Y-m-d', strtotime('-1 days', strtotime($str3)));


      $yesterday =  new \DateTime($str7." 00:00:00");
      $today =  new \DateTime($str3." 00:00:00");
      $tomorrow =  new \DateTime($str4." 00:00:00");
      $tomorrow2 =  new \DateTime($str5." 00:00:00");
      $tomorrow3 =  new \DateTime($str6." 00:00:00");



      if($producto == 'null' && $status != 'porvencer'){
        if($status == 'activo')
          $contracts = Contract::where(function ($query) use($status) {$query->where('status', $status)->orWhere('status','recoleccion');})->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
        else{
          if($status == 'vencidos'){
            $all =  Contract::where('status', 'activo')->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
            $contracts = array();
            for ($i=0; $i < count($all); $i++) {
              $validity = \DateTime::createFromFormat('Y-m-d H:i:s', $all[$i]['validity']);
              $validity->setTime(0,0,0);
              if(($validity == $yesterday ||  strtotime($validity->format('Y-m-d H:i:s')) < strtotime($today->format('Y-m-d H:i:s')) ) && $all[$i]['status'] != 'cancelado')
                array_push($contracts, $all[$i]);
            }
          }
          else
            $contracts = Contract::where('status', $status)->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
        }
      }
      elseif($producto != 'null' && $status != 'porvencer'){
          if($status == 'activo')
            $all = Contract::where(function ($query) use($status) {$query->where('status', $status)->orWhere('status','recoleccion');})->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
          else{
            if($status == 'vencidos'){
              $all2 = Contract::where('status', 'activo')->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
              $all = array();
              for ($i=0; $i < count($all2); $i++) {
                $validity = \DateTime::createFromFormat('Y-m-d H:i:s', $all2[$i]['validity']);
                $validity->setTime(0,0,0);
                if(($validity == $yesterday ||  strtotime($validity->format('Y-m-d H:i:s')) < strtotime($today->format('Y-m-d H:i:s')) ) && $all2[$i]['status'] != 'cancelado')
                  array_push($all, $all2[$i]);
              }
            }
            else
              $all = Contract::where('status', $status)->whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
          }

          $contracts = array();
          for ($i=0; $i < count($all); $i++) {
            for ($j=0; $j < count($all[$i]['products']); $j++) {
              if(intval($all[$i]['products'][$j]['id']) == intval($producto)){
                array_push($contracts, $all[$i]);
                break;
            }
          }
        }
      }

      elseif($producto == 'null' && $status == 'porvencer'){
        $all = Contract::whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
        $contracts = array();
        for ($i=0; $i < count($all); $i++) {
          $validity = \DateTime::createFromFormat('Y-m-d H:i:s', $all[$i]['validity']);
          $validity->setTime(0,0,0);

          if(($validity == $today || $validity == $tomorrow || $validity == $tomorrow2 || $validity == $tomorrow3) && $all[$i]['status'] != 'cancelado')
            array_push($contracts, $all[$i]);
          }
      }
      elseif($producto != 'null' && $status == 'porvencer'){
        $all = Contract::whereNull('deleted_at')->orderBy('folio', 'desc')->get()->toArray();
        $contracts = array();
        for ($i=0; $i < count($all); $i++) {
          $validity = \DateTime::createFromFormat('Y-m-d H:i:s', $all[$i]['validity']);
          $validity->setTime(0,0,0);
          if(($validity == $today || $validity == $tomorrow || $validity == $tomorrow2 || $validity == $tomorrow3) && $all[$i]['status'] != 'cancelado')
          for ($j=0; $j < count($all[$i]['products']); $j++) {
            if(intval($all[$i]['products'][$j]['id']) == intval($producto)){
              array_push($contracts, $all[$i]);
            }
          }
        }
      }
      $pdf = PDF::loadView('contract.printbyfilter', array(
        'contracts' => $contracts,
        'today' => $today,
        'status' => ($status == 'activo' ? 'ACTIVOS' : ($status == 'porvencer' ? 'POR VENCER' : ($status == 'vencidos' ? 'VENCIDOS' : ($status == 'recoleccion' ? 'EN RECOLECCION' : ($status == 'cancelado' ? 'CANCELADOS' : ''))))),
      ))->setPaper('letter', 'portrait');
      return $pdf->stream();
    }

    public function report(Request $request){
      $arr = DB::select("SELECT P.name AS EQUIPO, count(*) as CONTADOR FROM contracts C INNER JOIN contractdetails CD ON C.id = CD.contract_id INNER JOIN products P ON CD.product_id = P.id WHERE (C.status = 'activo' OR C.status = 'recoleccion') AND C.deleted_at is NULL and P.deleted_at is NULL and CD.deleted_at is NULL GROUP BY P.name");

      $pdf = PDF::loadView('contract.report', array(
        'arr' => $arr,
      ))->setPaper('letter', 'portrait');
      return $pdf->stream();
    }
    public function print($id, Request $request){
      $contract = Contract::findOrfail($id);
      $products = Product::all();

      $valor_total = 0;
      $renta_mensual = 0;
      $flete = 0;

      foreach ($contract->products as $p) {
        $valor_total+=$p->product_value;
        foreach ($products as $prod) {
          if($prod->id == $p->product_id){
            $renta_mensual+=$prod->costmonth;
            break;
          }
        }
      }

      $pdf = PDF::loadView('contract.pdf', array(
        'contract' => $contract,
        'valor_total' => $valor_total,
        'valor_letra' => NumeroALetras::convertir($valor_total, 'PESOS', 'CENTAVOS'),
        'deposit_letra' => NumeroALetras::convertir($contract->deposit, 'PESOS', 'CENTAVOS'),
        'primer_renta_letra' =>  NumeroALetras::convertir($contract->cost, 'PESOS', 'CENTAVOS'),
        'renta_mensual' => $renta_mensual,
        'renta_mensual_letra' => NumeroALetras::convertir($renta_mensual, 'PESOS', 'CENTAVOS'),
        'flete_letra' => NumeroALetras::convertir($contract->flete, 'PESOS', 'CENTAVOS'),
        'flete' => $flete,

      ))->setPaper('letter', 'portrait');
		  return $pdf->stream();
    }
    public function updatestatus($id, $status, Request $request){
      $contract = Contract::findOrfail($id);
      $contract->status = $status;
      $contract->save();
      return response()->json("correcto");
    }

    //PAYMENT METHODS
    public function addpayment($id, $factura, $notas, $monto, $periodo, $unidad,$porpagar, $actualizarvigencia, Request $request){

      $fecha = new \DateTime();
      $pay = new Payment();


      $contract = Contract::findOrfail($id);
      if(floatval($porpagar) <= 0)
        $contract->topay = 0;
      else
        $contract->topay = $porpagar;

      $pay->contract_id = $id;
      $pay->date_payment = $fecha;
      $pay->period = $periodo;
      $pay->amount = $monto;
      $pay->bill = $factura;
      $pay->notes = $notas;
      $pay->unity = $unidad;
      $pay->validate = 'false';
      $pay->save();

      // if(floatval($porpagar) <=0 && count($contract->payments) > 1)
      //   $contract->validity = Carbon::parse($contract->validity)->addMonth(1);
      // else
      //   $contract->validity = Carbon::parse($contract->validity);


      if(count($contract->payments) == 1)
        $contract->validity = Carbon::parse($contract->validity);
      elseif($actualizarvigencia == 'true')
        $contract->validity = Carbon::parse($contract->validity)->addMonth(1);
      else
        $contract->validity = Carbon::parse($contract->validity);

      $contract->save();
      $pay['validity'] = $contract->validity;
      return response()->json($pay);
    }
    public function updatepayment($id, $factura, $notas, $monto, $periodo, $unidad, $idpago, Request $request){
      $pay = Payment::findOrfail($idpago);
      $payment = Payment::where("contract_id", intval($id))->orderBy('id', 'desc')->first();
      $contract = Contract::findOrfail($id);

      if($payment->id == intval($idpago)){
        $cant = $pay->amount - $monto;
        $before = $contract->topay;

        $contract->topay = $contract->topay + $cant;
        // if($before > 0 && $contract->topay <= 0){
        //   $contract->topay = 0;
        //   $contract->validity = Carbon::parse($contract->validity)->addMonth(1);
        // }elseif($before <= 0 && $contract->topay > 0){
        //   $contract->validity = Carbon::parse($contract->validity)->subMonth(1);
        // }
      }

      $pay->period = $periodo;
      $pay->amount = $monto;
      $pay->bill = $factura;
      $pay->notes = $notas;
      $pay->unity = $unidad;
      $pay->save();
      $contract->save();
      return response()->json($contract->payments);
    }
    public function statuspayment($id, $idpago, $band, Request $request){
      $pay = Payment::findOrfail($idpago);
      if($band == 'true')
        $pay->validate = 'false';
      if($band == 'false')
        $pay->validate = 'true';

      $pay->save();
      return response()->json("correct");
    }
    public function deletepayment($id, $band, $idpago, Request $request){
      $contract = Contract::findOrfail($id);
      $payment = Payment::where("contract_id", intval($id))->orderBy('id', 'desc')->first();

      if($payment->id == intval($idpago)){
        $contract->topay = 0;
        if($band == 'true'){
          $contract->validity = Carbon::parse($contract->validity)->subMonth(1);
        }
      }
      $pay = Payment::findOrfail($idpago);
      $pay->delete();

      $contract->save();

      return response()->json($contract->payments);
    }
}
