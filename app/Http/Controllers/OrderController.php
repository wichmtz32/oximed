<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use Amranidev\Ajaxis\Ajaxis;
use URL;




use App\Contract;
use App\Client;
use App\Driver;
use App\Payment;
use App\Product;
use App\Serial;
use App\Repair;
use App\Contractdetail;

use App\Repairdetail;
use App\Observation;

/**
 * Class OrderController.
 *
 * @author  The scaffold-interface created at 2019-05-27 01:28:39pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - order';
        $orders = Order::paginate();




        return view('order.index',compact('orders','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
      $title = 'Create - Ordere';

      $clients = Client::all();
      $drivers = Driver::all();
      $products = Product::all();
      $realSerials = array();
      $serials = Serial::all();
      $contracts = Contract::where('status', '!=', 'cancelado')->get();
      $order = Order::all()->last();
      $folio = $order->folio + 1;
      foreach ($serials as $s) {
        $band = false;
        foreach ($contracts as $c) {
          foreach ($c->products as $p) {
            if($p->serie == $s->number)
              $band = true;
          }
        }
        if(!$band){
          $arr = [];
          $arr['number'] = $s->number;
          $arr['id'] =   $s->product_id;
          $realSerials = array_merge($realSerials, array($arr));
        }
      }
      $serials = $realSerials;
      $nueva = true;
      $show = false;
      return view('order.create',compact('title','clients' , 'drivers', 'products', 'nueva', 'show', 'serials', 'folio'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order2 = Order::all()->last();
        $folio = $order2->folio + 1;

        $order = new Order();
        $order->client = $request->client;
        $order->address = $request->address;
        $order->phone = $request->phone;
        $order->cellphone = $request->cellphone;

        if(!is_null($request->date))
          $fecha = \DateTime::createFromFormat('j M, Y', $request->date);
        else
          $fecha = new \DateTime();
        if(!is_null($request->fecha2))
        {
          $fecha2 = \DateTime::createFromFormat('j M, Y', $request->fecha2);
          $order->deliverDate =$fecha2;
        }
        $order->date = $fecha;
        
        if(isset($request->product))
        {
          for ($j=0; $j < count($request->product); $j++)
          {
            $repair = new Repair();
            $repair->order_id = $order->id;
            $repair->product = $request->product[$j];
            if(isset($request->ppago[$j]))
              $repair->pago_reparador = $request->ppago[$j];
            $repair->serialNumber = $request->nserie[$j];
            $repair->faults = $request->faults[$j];
            $repair->status = $request->estatus[$j];

            $repair->tipo = $request->tipos[$j];

            $repair->repairman = $request->reparador[$j];
            $repair->save();
            if(isset($request->descripcion[$j]))
            {
              for ($k=0; $k < count($request->descripcion[$j]); $k++)
              {
                if($request->descripcion[$j][$k] != 'null' || !is_null($request->descripcion[$j][$k]))
                {
                  $rd = new Repairdetail();
                  $rd->repair_id = $repair->id;
                  $rd->description = $request->descripcion[$j][$k];
                  //$rd->quantity = intval($request->cantidad[$j][$k]);
                  $rd->unitPrice = floatval($request->precio[$j][$k]);
                  //$rd->total = floatval($request->totalp[$j][$k]);
                  if(floatval($request->precio[$j][$k]) == 0)
                    $rd->warranty=true;
                  else
                    $rd->warranty=false;

                  $rd->save();
                }
              }
            }
            if(isset($request->descripcion1[$j]))
            {
              for ($l=0; $l < count($request->descripcion1[$j]); $l++) {
                if($request->descripcion1[$l] != 'null' || !is_null($request->descripcion1[$j][$l])){
                  $ob = new Observation();
                  $ob->repair_id = $repair->id;
                  $ob->description = $request->descripcion1[$j][$l];
                  if(!is_null($request->fechas4[$j][$l]))
                   $fecha = \DateTime::createFromFormat('j M, Y', $request->fechas4[$j][$l]);
                  $ob->fecha=$fecha;
                  $ob->save();
                }
              }
            }
          }
        }
        $order2 = Order::all()->last();
        $folio = $order2->folio + 1;
        $order->folio = $folio;
        $order->save();
        return redirect('order');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - order';

        if($request->ajax())
        {
            return URL::to('order/'.$id);
        }

        $order = Order::findOrfail($id);
        $folio = $order->folio;
        return view('order.show',compact('title','order','folio'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - order';



        $order = Order::findOrfail($id);
        $folio = $order->folio;
        $title = 'Create - Ordere';

        $clients = Client::all();
        $drivers = Driver::all();
        $products = Product::all();
        $realSerials = array();
        $serials = Serial::all();
        $contracts = Contract::where('status', '!=', 'cancelado')->get();
        foreach ($serials as $s) {
          $band = false;
          foreach ($contracts as $c) {
            foreach ($c->products as $p) {
              if($p->serie == $s->number)
                $band = true;
            }
          }
          if(!$band){
            $arr = [];
            $arr['number'] = $s->number;
            $arr['id'] =   $s->product_id;
            $realSerials = array_merge($realSerials, array($arr));
          }
        }
        $serials = $realSerials;
        $nueva = false;
        $show = false;


        return view('order.create',compact('order','title','clients' , 'drivers', 'products', 'nueva', 'show', 'serials', 'folio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $order = Order::findOrfail($id);



        $order->client = $request->client;
        $order->address = $request->address;
        $order->phone = $request->phone;
        $order->cellphone = $request->cellphone;


        if(!is_null($request->fecha2))
        {
          $fecha2 = \DateTime::createFromFormat('j M, Y', $request->fecha2);
          $order->deliverDate =$fecha2;
        }
        $order->reparaciones()->forceDelete();
        $order->save();
        if(isset($request->product))
        {
          for ($j=0; $j < count($request->product); $j++)
          {
            $repair = new Repair();
            $repair->order_id = $order->id;
            $repair->product = $request->product[$j];
            if(isset($request->ppago[$j]))
              $repair->pago_reparador = $request->ppago[$j];
            $repair->serialNumber = $request->nserie[$j];
            $repair->faults = $request->faults[$j];
            $repair->status = $request->estatus[$j];

            $repair->tipo = $request->tipos[$j];

            $repair->repairman = $request->reparador[$j];
            $repair->save();
            if(isset($request->descripcion[$j]))
            {
              for ($k=0; $k < count($request->descripcion[$j]); $k++)
              {
                if($request->descripcion[$j][$k] != 'null' || !is_null($request->descripcion[$j][$k]))
                {
                  $rd = new Repairdetail();
                  $rd->repair_id = $repair->id;
                  $rd->description = $request->descripcion[$j][$k];
                  //$rd->quantity = intval($request->cantidad[$j][$k]);
                  $rd->unitPrice = floatval($request->precio[$j][$k]);
                  //$rd->total = floatval($request->totalp[$j][$k]);
                  if(floatval($request->precio[$j][$k]) == 0)
                    $rd->warranty=true;
                  else
                    $rd->warranty=false;

                  $rd->save();
                }
              }
            }
            if(isset($request->descripcion1[$j]))
            {
              for ($l=0; $l < count($request->descripcion1[$j]); $l++) {
                if($request->descripcion1[$l] != 'null' || !is_null($request->descripcion1[$j][$l])){
                  $ob = new Observation();
                  $ob->repair_id = $repair->id;
                  $ob->description = $request->descripcion1[$j][$l];
                  if(!is_null($request->fechas4[$j][$l]))
                  {
                   $fecha = \DateTime::createFromFormat('j M, Y', $request->fechas4[$j][$l]);

                  }
                  $ob->fecha=$fecha;
                  $ob->save();
                }
              }
            }
          }
        }
        return redirect('order');
    }
    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/order/'. $id . '/delete');
        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$order = Order::findOrfail($id);
     	$order->delete();
        return URL::to('order');
    }
}
