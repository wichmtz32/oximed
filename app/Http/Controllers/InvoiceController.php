<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invoice;
use Amranidev\Ajaxis\Ajaxis;
use URL;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use App\Person;
use App\Product;

use PDF;

/**
 * Class InvoiceController.
 *
 * @author  The scaffold-interface created at 2019-05-31 03:37:33pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - invoice';
        $invoices = Invoice::all();
        $persons = Person::all();
        return view('invoice.index',compact('invoices','title','persons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - invoice';

        $persons = Person::all();
        
        return view('invoice.create',compact('title','persons'  ));
    }

    
    public function search(Request $request, $busqueda, $tipo){
        $invoiceN = Invoice::where('invoiceNumber','=', $busqueda)->where('type','=',$tipo)->get();
        //$invoiceN = Invoice::where(function ($query) use($busqueda) {$query->where('invoiceNumber','=', $bus);})->get()->toArray();
  
        return response($invoiceN);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $invoice = new Invoice();

        
        $invoice->invoiceNumber = $request->invoiceNumber;

        $invoice->type = $request->type;

        $invoice->person_id = $request->person_id;
        
        $invoice->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new invoice has been created !!']);

        return redirect('invoice');
    }

    public function store2(Request $request, $busqueda, $tipo, $person_id)
    {
        $existe = Invoice::where('invoiceNumber','=',$busqueda)->where('type','=',$tipo)->get();
        if(count($existe) > 0)
        {
            $existe = 0;
        }
        else {
            $invoice = new Invoice();
            $invoice->invoiceNumber = $busqueda;
            $invoice->type = $tipo;
            $invoice->person_id = $person_id;
            $invoice->save();
        }
        $pusher = App::make('pusher');
        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new invoice has been created !!']);

        session()->flash('message', 'Contrato registrado correctamente');
        return response($invoice);
    }

    public function missing(Request $request, $fechaInicial, $fechaFinal, $checkBoxes)
    {
        $fechaInicial = date_create($fechaInicial);
        $fechaInicial = date_format($fechaInicial, 'Y-m-d');
        $fechaFinal = date_create($fechaFinal);
        $fechaFinal = date_format($fechaFinal, 'Y-m-d');
        $arr = [];
        $arreglo = array();

        if(strpos($checkBoxes, ',') !== false)
        {
            $tipo = explode(",", $checkBoxes);
            for($i = 0; $i < count($tipo); $i++) {
                if($tipo[$i] === 'REM')
                {
                    $pri = Invoice::where('type','=','REM')->whereDate('created_at', '<', $fechaInicial . ' 00:00:00')->orderBy('invoiceNumber','desc')->get();

                    $ult = Invoice::where('type','=','REM')->whereDate('created_at', '>', $fechaFinal . ' 23:59:59')->orderBy('invoiceNumber','asc')->get();

                    $vista = Invoice::where('type','=','REM')->whereBetween('created_at', [$fechaInicial . ' 00:00:00', $fechaFinal . ' 23:59:59'])->orderBy('invoiceNumber','asc')->get();

                    if(count($vista) > 0)
                    {
                        $iterador = 0;
                        if(count($pri) > 0)
                            $primero = $pri[0]->invoiceNumber  + 1;
                        else 
                            $primero = $vista[0]->invoiceNumber;
                        if(count($ult) > 0)
                            $ultimo = $ult[0]->invoiceNumber - 1;
                        else 
                            $ultimo = $vista[count($vista) - 1]->invoiceNumber;
                        foreach (range($primero, $ultimo) as $numero) {
                            if($iterador >= count($vista) || $numero != $vista[$iterador]->invoiceNumber)
                            {
                                $arr['type'] = 'REM';
                                $arr['invoiceNumber'] = $numero;
                                $arreglo = array_merge($arreglo, array($arr));
                            }
                            else 
                                $iterador++;
                        }
                    }
                }
                else if($tipo[$i] === 'NV')
                {
                    $pri = Invoice::where('type','=','NV')->whereDate('created_at', '<', $fechaInicial . ' 00:00:00')->orderBy('invoiceNumber','desc')->get();

                    $ult = Invoice::where('type','=','NV')->whereDate('created_at', '>', $fechaFinal . ' 23:59:59')->orderBy('invoiceNumber','asc')->get();

                    $vista = Invoice::where('type','=','NV')->whereBetween('created_at', [$fechaInicial . ' 00:00:00', $fechaFinal . ' 23:59:59'])->orderBy('invoiceNumber','asc')->get();

                    $iterador = 0;
                    if(count($vista) > 0)
                    {
                        if(count($pri) > 0)
                            $primero = $pri[0]->invoiceNumber  + 1;
                        else 
                            $primero = $vista[0]->invoiceNumber;
                        if(count($ult) > 0)
                            $ultimo = $ult[0]->invoiceNumber - 1;
                        else 
                            $ultimo = $vista[count($vista) - 1]->invoiceNumber;
                        foreach (range($primero, $ultimo) as $numero) {
                            if($iterador >= count($vista) || $numero != $vista[$iterador]->invoiceNumber)
                            {
                                $arr['type'] = 'NV';
                                $arr['invoiceNumber'] = $numero;
                                $arreglo = array_merge($arreglo, array($arr));
                            }
                            else 
                                $iterador++;
                        }
                    }
                }
                else if($tipo[$i] === 'FCON')
                {
                    $pri = Invoice::where('type','=','FCON')->whereDate('created_at', '<', $fechaInicial . ' 00:00:00')->orderBy('invoiceNumber','desc')->get();

                    $ult = Invoice::where('type','=','FCON')->whereDate('created_at', '>', $fechaFinal . ' 23:59:59')->orderBy('invoiceNumber','asc')->get();

                    $vista = Invoice::where('type','=','FCON')->whereBetween('created_at', [$fechaInicial . ' 00:00:00', $fechaFinal . ' 23:59:59'])->orderBy('invoiceNumber','asc')->get();

                    if(count($vista) > 0)
                    {
                        $iterador = 0;
                        if(count($pri) > 0)
                            $primero = $pri[0]->invoiceNumber  + 1;
                        else 
                            $primero = $vista[0]->invoiceNumber;
                        if(count($ult) > 0)
                            $ultimo = $ult[0]->invoiceNumber - 1;
                        else 
                            $ultimo = $vista[count($vista) - 1]->invoiceNumber;
                        foreach (range($primero, $ultimo) as $numero) {
                            if($iterador >= count($vista) || $numero != $vista[$iterador]->invoiceNumber)
                            {
                                $arr['type'] = 'FCON';
                                $arr['invoiceNumber'] = $numero;
                                $arreglo = array_merge($arreglo, array($arr));
                            }
                            else 
                                $iterador++;
                        }
                    }
                }
                else if($tipo[$i] === 'FCR')
                {
                    $pri = Invoice::where('type','=','FCR')->whereDate('created_at', '<', $fechaInicial . ' 00:00:00')->orderBy('invoiceNumber','desc')->get();

                    $ult = Invoice::where('type','=','FCR')->whereDate('created_at', '>', $fechaFinal . ' 23:59:59')->orderBy('invoiceNumber','asc')->get();

                    $vista = Invoice::where('type','=','FCR')->whereBetween('created_at', [$fechaInicial . ' 00:00:00', $fechaFinal . ' 23:59:59'])->orderBy('invoiceNumber','asc')->get();
                    if(count($vista) > 0)
                    {
                        $iterador = 0;
                        if(count($pri) > 0)
                            $primero = $pri[0]->invoiceNumber  + 1;
                        else
                            $primero = $vista[0]->invoiceNumber;
                        if(count($ult) > 0)
                            $ultimo = $ult[0]->invoiceNumber - 1;
                        else
                            $ultimo = $vista[count($vista) - 1]->invoiceNumber;
                        foreach (range($primero, $ultimo) as $numero) {
                            if($iterador >= count($vista) || $numero != $vista[$iterador]->invoiceNumber)
                            {
                                $arr['type'] = 'FCR';
                                $arr['invoiceNumber'] = $numero;
                                $arreglo = array_merge($arreglo, array($arr));
                            }
                            else 
                                $iterador++;
                        }
                    }
                }
            }
        }
        else {
            $pri = Invoice::where('type','=',$checkBoxes)->whereDate('created_at', '<', $fechaInicial . ' 00:00:00')->orderBy('invoiceNumber','desc')->get();

            $ult = Invoice::where('type','=',$checkBoxes)->whereDate('created_at', '>', $fechaFinal . ' 23:59:59')->orderBy('invoiceNumber','asc')->get();

            $vista = Invoice::where('type','=',$checkBoxes)->whereBetween('created_at', [$fechaInicial . ' 00:00:00', $fechaFinal . ' 23:59:59'])->orderBy('invoiceNumber','asc')->get();

            if(count($vista) > 0)
            {
                $iterador = 0;
                if(count($pri) > 0)
                    $primero = $pri[0]->invoiceNumber  + 1;
                else 
                    $primero = $vista[0]->invoiceNumber;
                if(count($ult) > 0)
                    $ultimo = $ult[0]->invoiceNumber - 1;
                else 
                    $ultimo = $vista[count($vista) - 1]->invoiceNumber;
                foreach (range($primero, $ultimo) as $numero) {
                    if($iterador >= count($vista) || $numero != $vista[$iterador]->invoiceNumber)
                    {
                        $arr['type'] = $checkBoxes;
                        $arr['invoiceNumber'] = $numero;
                        $arreglo = array_merge($arreglo, array($arr));
                    }
                    else 
                        $iterador++;
                }
            }
        }
        

        return response($arreglo);
    }

    /**
     * Print invoices missing.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function printMissing(Request $request, $fechaInicial, $fechaFinal, $checkBoxes){
        $fInicial = $fechaInicial;
        $fFinal = $fechaFinal;
        $fechaInicial = date_create($fechaInicial);
        $fechaInicial = date_format($fechaInicial, 'Y-m-d');
        $fechaFinal = date_create($fechaFinal);
        $fechaFinal = date_format($fechaFinal, 'Y-m-d');
        $arr = [];
        $arreglo = array();

        if(strpos($checkBoxes, ',') !== false)
        {
            $tipo = explode(",", $checkBoxes);
            for($i = 0; $i < count($tipo); $i++) {
                if($tipo[$i] === 'REM')
                {
                    $pri = Invoice::where('type','=','REM')->whereDate('created_at', '<', $fechaInicial . ' 00:00:00')->orderBy('invoiceNumber','desc')->get();

                    $ult = Invoice::where('type','=','REM')->whereDate('created_at', '>', $fechaFinal . ' 23:59:59')->orderBy('invoiceNumber','asc')->get();

                    $vista = Invoice::where('type','=','REM')->whereBetween('created_at', [$fechaInicial . ' 00:00:00', $fechaFinal . ' 23:59:59'])->orderBy('invoiceNumber','asc')->get();

                    if(count($vista) > 0)
                    {
                        $iterador = 0;
                        if(count($pri) > 0)
                            $primero = $pri[0]->invoiceNumber  + 1;
                        else 
                            $primero = $vista[0]->invoiceNumber;
                        if(count($ult) > 0)
                            $ultimo = $ult[0]->invoiceNumber - 1;
                        else 
                            $ultimo = $vista[count($vista) - 1]->invoiceNumber;
                        foreach (range($primero, $ultimo) as $numero) {
                            if($iterador >= count($vista) || $numero != $vista[$iterador]->invoiceNumber)
                            {
                                $arr['type'] = 'REM';
                                $arr['invoiceNumber'] = $numero;
                                $arreglo = array_merge($arreglo, array($arr));
                            }
                            else 
                                $iterador++;
                        }
                    }
                }
                else if($tipo[$i] === 'NV')
                {
                    $pri = Invoice::where('type','=','NV')->whereDate('created_at', '<', $fechaInicial . ' 00:00:00')->orderBy('invoiceNumber','desc')->get();

                    $ult = Invoice::where('type','=','NV')->whereDate('created_at', '>', $fechaFinal . ' 23:59:59')->orderBy('invoiceNumber','asc')->get();

                    $vista = Invoice::where('type','=','NV')->whereBetween('created_at', [$fechaInicial . ' 00:00:00', $fechaFinal . ' 23:59:59'])->orderBy('invoiceNumber','asc')->get();

                    $iterador = 0;
                    if(count($vista) > 0)
                    {
                        if(count($pri) > 0)
                            $primero = $pri[0]->invoiceNumber  + 1;
                        else 
                            $primero = $vista[0]->invoiceNumber;
                        if(count($ult) > 0)
                            $ultimo = $ult[0]->invoiceNumber - 1;
                        else 
                            $ultimo = $vista[count($vista) - 1]->invoiceNumber;
                        foreach (range($primero, $ultimo) as $numero) {
                            if($iterador >= count($vista) || $numero != $vista[$iterador]->invoiceNumber)
                            {
                                $arr['type'] = 'NV';
                                $arr['invoiceNumber'] = $numero;
                                $arreglo = array_merge($arreglo, array($arr));
                            }
                            else 
                                $iterador++;
                        }
                    }
                }
                else if($tipo[$i] === 'FCON')
                {
                    $pri = Invoice::where('type','=','FCON')->whereDate('created_at', '<', $fechaInicial . ' 00:00:00')->orderBy('invoiceNumber','desc')->get();

                    $ult = Invoice::where('type','=','FCON')->whereDate('created_at', '>', $fechaFinal . ' 23:59:59')->orderBy('invoiceNumber','asc')->get();

                    $vista = Invoice::where('type','=','FCON')->whereBetween('created_at', [$fechaInicial . ' 00:00:00', $fechaFinal . ' 23:59:59'])->orderBy('invoiceNumber','asc')->get();

                    if(count($vista) > 0)
                    {
                        $iterador = 0;
                        if(count($pri) > 0)
                            $primero = $pri[0]->invoiceNumber  + 1;
                        else 
                            $primero = $vista[0]->invoiceNumber;
                        if(count($ult) > 0)
                            $ultimo = $ult[0]->invoiceNumber - 1;
                        else 
                            $ultimo = $vista[count($vista) - 1]->invoiceNumber;
                        foreach (range($primero, $ultimo) as $numero) {
                            if($iterador >= count($vista) || $numero != $vista[$iterador]->invoiceNumber)
                            {
                                $arr['type'] = 'FCON';
                                $arr['invoiceNumber'] = $numero;
                                $arreglo = array_merge($arreglo, array($arr));
                            }
                            else 
                                $iterador++;
                        }
                    }
                }
                else if($tipo[$i] === 'FCR')
                {
                    $pri = Invoice::where('type','=','FCR')->whereDate('created_at', '<', $fechaInicial . ' 00:00:00')->orderBy('invoiceNumber','desc')->get();

                    $ult = Invoice::where('type','=','FCR')->whereDate('created_at', '>', $fechaFinal . ' 23:59:59')->orderBy('invoiceNumber','asc')->get();

                    $vista = Invoice::where('type','=','FCR')->whereBetween('created_at', [$fechaInicial . ' 00:00:00', $fechaFinal . ' 23:59:59'])->orderBy('invoiceNumber','asc')->get();
                    if(count($vista) > 0)
                    {
                        $iterador = 0;
                        if(count($pri) > 0)
                            $primero = $pri[0]->invoiceNumber  + 1;
                        else
                            $primero = $vista[0]->invoiceNumber;
                        if(count($ult) > 0)
                            $ultimo = $ult[0]->invoiceNumber - 1;
                        else
                            $ultimo = $vista[count($vista) - 1]->invoiceNumber;
                        foreach (range($primero, $ultimo) as $numero) {
                            if($iterador >= count($vista) || $numero != $vista[$iterador]->invoiceNumber)
                            {
                                $arr['type'] = 'FCR';
                                $arr['invoiceNumber'] = $numero;
                                $arreglo = array_merge($arreglo, array($arr));
                            }
                            else 
                                $iterador++;
                        }
                    }
                }
            }
        }
        else {
            $pri = Invoice::where('type','=',$checkBoxes)->whereDate('created_at', '<', $fechaInicial . ' 00:00:00')->orderBy('invoiceNumber','desc')->get();

            $ult = Invoice::where('type','=',$checkBoxes)->whereDate('created_at', '>', $fechaFinal . ' 23:59:59')->orderBy('invoiceNumber','asc')->get();

            $vista = Invoice::where('type','=',$checkBoxes)->whereBetween('created_at', [$fechaInicial . ' 00:00:00', $fechaFinal . ' 23:59:59'])->orderBy('invoiceNumber','asc')->get();

            if(count($vista) > 0)
            {
                $iterador = 0;
                if(count($pri) > 0)
                    $primero = $pri[0]->invoiceNumber  + 1;
                else 
                    $primero = $vista[0]->invoiceNumber;
                if(count($ult) > 0)
                    $ultimo = $ult[0]->invoiceNumber - 1;
                else 
                    $ultimo = $vista[count($vista) - 1]->invoiceNumber;
                foreach (range($primero, $ultimo) as $numero) {
                    if($iterador >= count($vista) || $numero != $vista[$iterador]->invoiceNumber)
                    {
                        $arr['type'] = $checkBoxes;
                        $arr['invoiceNumber'] = $numero;
                        $arreglo = array_merge($arreglo, array($arr));
                    }
                    else 
                        $iterador++;
                }
            }
        }

        $str3 =  date("Y-m-d");
  
        $today =  new \DateTime($str3." 00:00:00");
  
        $pdf = PDF::loadView('invoice.printMissing', array(
          'faltantes' => $arreglo,
          'today' => $today,
          'fechaInicial' => $fInicial,
          'fechaFinal' => $fFinal,
          'tipos' => $checkBoxes,
        ))->setPaper('letter', 'portrait');
        return $pdf->stream();
      }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - invoice';

        if($request->ajax())
        {
            return URL::to('invoice/'.$id);
        }

        $invoice = Invoice::findOrfail($id);
        return view('invoice.show',compact('title','invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - invoice';
        if($request->ajax())
        {
            return URL::to('invoice/'. $id . '/edit');
        }

        $persons = Person::all()->pluck('name','id');
        
        $invoice = Invoice::findOrfail($id);
        return view('invoice.edit',compact('title','invoice', 'persons' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $invoice = Invoice::findOrfail($id);
    	
        $invoice->invoiceNumber = $request->invoiceNumber;
        
        $invoice->type = $request->type;

        $invoice->person_id = $request->person_id;
        
        $invoice->save();

        return redirect('invoice');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/invoice/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$invoice = Invoice::findOrfail($id);
     	$invoice->delete();
        return URL::to('invoice');
    }
}
