<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Observation.
 *
 * @author  The scaffold-interface created at 2019-05-13 02:35:42pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Observation extends Model
{
	
	use SoftDeletes;

	protected $dates = ['deleted_at'];
    
	
    protected $table = 'observations';

	
	public function repair()
	{
		return $this->belongsTo('App\Repair','repair_id');
	}

	
}
