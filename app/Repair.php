<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Repairdetail;

use App\Observation;
use App\Order;

/*
 * Class Repair.
 *
 * @author  The scaffold-interface created at 2019-05-13 02:23:56pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Repair extends Model
{

	use SoftDeletes;

	protected $dates = ['deleted_at'];


    protected $table = 'repairs';

	public function client()
	{
		return $this->belongsTo('App\Client','client_id');
	}

  public function detalles()
  {
    return $this->hasMany(Repairdetail::class);


  }


  public function observaciones()
  {
    return $this->hasMany(Observation::class);


  }
  public function orden()
	{
		return $this->belongsTo('App\Order','order_id');
	}





  public function repair()
  {
    return $this->belongsTo('App\Repair','repair_id');
  }


}
