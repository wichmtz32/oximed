<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Client.
 *
 * @author  The scaffold-interface created at 2018-10-08 06:23:07pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Client extends Model
{
	protected $dates = ['deleted_at'];
	protected $table = 'clients';
	protected $fillable = [
		'name',
		'address',
		'suburb',
		'cp',
		'phone',
		'email',
		'cellphone',
		'person',
		'rfc'
	];

	use SoftDeletes;



}
