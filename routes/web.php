<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@index')->name('home');
Auth::routes();
Route::group(['middleware' => 'auth'], function()
{
  //driver Routes
    Route::resource('driver','\App\Http\Controllers\DriverController');
    Route::post('driver/{id}/update','\App\Http\Controllers\DriverController@update');
    Route::get('driver/{id}/delete','\App\Http\Controllers\DriverController@destroy');
    Route::get('driver/{id}/deleteMsg','\App\Http\Controllers\DriverController@DeleteMsg');

  //client Routes
    Route::resource('client','\App\Http\Controllers\ClientController');
    Route::get('client/ajax', '\App\Http\Controllers\ClientController@getclientesajax')->name('getclientesajax');
    Route::get('client/search/{busqueda}','\App\Http\Controllers\ClientController@busqueda');
    Route::post('client/{id}/update','\App\Http\Controllers\ClientController@update');
    Route::get('client/{id}/delete','\App\Http\Controllers\ClientController@destroy');
    Route::get('client/{id}/deleteMsg','\App\Http\Controllers\ClientController@DeleteMsg');

    //Usuarios
    Route::get('/users', 'UserController@index')->name('users_index');
    Route::get('/users/create', 'UserController@create')->name('users_create');
    Route::post('/users', 'UserController@store')->name('users_store');
    Route::get('/users/edit/{user}', 'UserController@edit')->name('users_edit');
    Route::put('/users/edit/{user}', 'UserController@update')->name('users_update');
    Route::delete('/users/{user}', 'UserController@delete')->name('users_delete');

    //Roles
    Route::get('/roles', 'RolesController@index')->name('roles_index');
    Route::get('/roles/create', 'RolesController@create')->name('roles_create');
    Route::post('/roles', 'RolesController@store')->name('roles_store');
    Route::get('/roles/edit/{role}', 'RolesController@edit')->name('roles_edit');
    Route::put('/roles/edit/{role}', 'RolesController@update')->name('roles_update');
    Route::delete('/roles/{role}', 'RolesController@delete')->name('roles_delete');

  //payment Routes
    Route::resource('payment','\App\Http\Controllers\PaymentController');
    Route::post('payment/{id}/update','\App\Http\Controllers\PaymentController@update');
    Route::get('payment/{id}/delete','\App\Http\Controllers\PaymentController@destroy');
    Route::get('payment/{id}/deleteMsg','\App\Http\Controllers\PaymentController@DeleteMsg');

  //contract Routes
    Route::get('contracts/report','\App\Http\Controllers\ContractController@report');
    Route::resource('contract','\App\Http\Controllers\ContractController');
    Route::get('/contract/ajax/{status}/{producto}', '\App\Http\Controllers\ContractController@getcontratosajax')->name('getcontratosajax');
    Route::get('/contract/print/{status}/{producto}', '\App\Http\Controllers\ContractController@printContracts');
    Route::get('/contract/search/{busqueda}/{status}','\App\Http\Controllers\ContractController@search');
    Route::post('contract/{id}/update','\App\Http\Controllers\ContractController@update');
    Route::get('contract/{id}/delete','\App\Http\Controllers\ContractController@destroy');
    Route::get('contract/{id}/deleteMsg','\App\Http\Controllers\ContractController@DeleteMsg');
    Route::get('contract/{id}/addpayment/{factura}/{notas}/{monto}/{periodo}/{unidad}/{porpagar}/{actualizarvigencia}','\App\Http\Controllers\ContractController@addpayment');
    Route::get('contract/{id}/updatepayment/{factura}/{notas}/{monto}/{periodo}/{unidad}/{idpago}','\App\Http\Controllers\ContractController@updatepayment');
    Route::get('contract/{id}/statuspayment/{idpago}/{band}','\App\Http\Controllers\ContractController@statuspayment');
    Route::get('contract/{id}/deletepayment/{band}/{idpago}','\App\Http\Controllers\ContractController@deletepayment');
    Route::get('contract/{id}/updatestatus/{status}','\App\Http\Controllers\ContractController@updatestatus');
    Route::get('contract/{id}/print','\App\Http\Controllers\ContractController@print');


    Route::resource('product','\App\Http\Controllers\ProductController');
    Route::post('product/{id}/update','\App\Http\Controllers\ProductController@update');
    Route::get('product/{id}/delete','\App\Http\Controllers\ProductController@destroy');
    Route::get('product/{id}/deleteMsg','\App\Http\Controllers\ProductController@DeleteMsg');
});

//contractdetail Routes
Route::group(['middleware'=> 'auth'],function(){
  Route::resource('contractdetail','\App\Http\Controllers\ContractdetailController');
  Route::post('contractdetail/{id}/update','\App\Http\Controllers\ContractdetailController@update');
  Route::get('contractdetail/{id}/delete','\App\Http\Controllers\ContractdetailController@destroy');
  Route::get('contractdetail/{id}/deleteMsg','\App\Http\Controllers\ContractdetailController@DeleteMsg');
});

//serial Routes
Route::group(['middleware'=> 'auth'],function(){
  Route::resource('serial','\App\Http\Controllers\SerialController');
  Route::post('serial/{id}/update','\App\Http\Controllers\SerialController@update');
  Route::get('serial/{id}/delete','\App\Http\Controllers\SerialController@destroy');
  Route::get('serial/{id}/deleteMsg','\App\Http\Controllers\SerialController@DeleteMsg');
});


//repair Routes
Route::group(['middleware'=> 'auth'],function(){
  Route::resource('repair','\App\Http\Controllers\RepairController');
  Route::post('repair/{id}/update','\App\Http\Controllers\RepairController@update');
  Route::get('repair/{id}/delete','\App\Http\Controllers\RepairController@destroy');
  Route::get('repair/{id}/deleteMsg','\App\Http\Controllers\RepairController@DeleteMsg');
});

//repairdetail Routes
Route::group(['middleware'=> 'auth'],function(){
  Route::resource('repairdetail','\App\Http\Controllers\RepairdetailController');
  Route::post('repairdetail/{id}/update','\App\Http\Controllers\RepairdetailController@update');
  Route::get('repairdetail/{id}/delete','\App\Http\Controllers\RepairdetailController@destroy');
  Route::get('repairdetail/{id}/deleteMsg','\App\Http\Controllers\RepairdetailController@DeleteMsg');
});

//observation Routes
Route::group(['middleware'=> 'auth'],function(){
  Route::resource('observation','\App\Http\Controllers\ObservationController');
  Route::post('observation/{id}/update','\App\Http\Controllers\ObservationController@update');
  Route::get('observation/{id}/delete','\App\Http\Controllers\ObservationController@destroy');
  Route::get('observation/{id}/deleteMsg','\App\Http\Controllers\ObservationController@DeleteMsg');
});

//order Routes
Route::group(['middleware'=> 'auth'],function(){
  Route::resource('order','\App\Http\Controllers\OrderController');
  Route::post('order/{id}/update','\App\Http\Controllers\OrderController@update');
  Route::get('order/{id}/delete','\App\Http\Controllers\OrderController@destroy');
  Route::get('order/{id}/deleteMsg','\App\Http\Controllers\OrderController@DeleteMsg');
});



Route::group(['middleware'=> 'auth'],function(){
  Route::resource('invoice','\App\Http\Controllers\InvoiceController');
  Route::post('invoice/{id}/update','\App\Http\Controllers\InvoiceController@update');
  Route::get('invoice/{id}/delete','\App\Http\Controllers\InvoiceController@destroy');
  Route::get('invoice/{id}/deleteMsg','\App\Http\Controllers\InvoiceController@DeleteMsg');
  Route::get('invoice/search/{busqueda}/{tipo}','\App\Http\Controllers\InvoiceController@search');
  Route::post('invoice/store2/{busqueda}/{tipo}/{person_id}','\App\Http\Controllers\InvoiceController@store2');
  Route::get('invoice/missing/{fechaInicial}/{fechaFinal}/{checkBoxes}','\App\Http\Controllers\InvoiceController@missing');
  Route::get('invoice/printMissing/{fechaInicial}/{fechaFinal}/{checkBoxes}','\App\Http\Controllers\InvoiceController@printMissing');
});


//person Routes
Route::group(['middleware'=> 'auth'],function(){
  Route::resource('person','\App\Http\Controllers\PersonController');
  Route::post('person/{id}/update','\App\Http\Controllers\PersonController@update');
  Route::get('person/{id}/delete','\App\Http\Controllers\PersonController@destroy');
  Route::get('person/{id}/deleteMsg','\App\Http\Controllers\PersonController@DeleteMsg');
});
