@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Create payment
    </h1>
    <form method = 'get' action = '{!!url("payment")!!}'>
        <button class = 'btn blue'>payment Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("payment")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="date_payment" name = "date_payment" type="text" class="validate">
            <label for="date_payment">date_payment</label>
        </div>
        <div class="input-field col s6">
            <input id="bill" name = "bill" type="text" class="validate">
            <label for="bill">bill</label>
        </div>
        <div class="input-field col s6">
            <input id="number" name = "number" type="text" class="validate">
            <label for="number">number</label>
        </div>
        <div class="input-field col s6">
            <input id="amount" name = "amount" type="text" class="validate">
            <label for="amount">amount</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection