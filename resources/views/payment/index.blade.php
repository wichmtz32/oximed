@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        payment Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("payment")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New payment</button>
        </form>
    </div>
    <table class="table bordered">
        <thead>
            <th>date_payment</th>
            <th>bill</th>
            <th>number</th>
            <th>amount</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($payments as $payment)
            <tr>
                <td>{!!$payment->date_payment!!}</td>
                <td>{!!$payment->bill!!}</td>
                <td>{!!$payment->number!!}</td>
                <td>{!!$payment->amount!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/payment/{!!$payment->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/payment/{!!$payment->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/payment/{!!$payment->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $payments->render() !!}

</div>
@endsection
