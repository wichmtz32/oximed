@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show payment
    </h1>
    <form method = 'get' action = '{!!url("payment")!!}'>
        <button class = 'btn blue'>payment Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>date_payment : </i></b>
                </td>
                <td>{!!$payment->date_payment!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>bill : </i></b>
                </td>
                <td>{!!$payment->bill!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>number : </i></b>
                </td>
                <td>{!!$payment->number!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>amount : </i></b>
                </td>
                <td>{!!$payment->amount!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection