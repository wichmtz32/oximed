@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div>
  <div class="col m12">
    <h2>Informacion producto</h2>
  </div>
  <br>
  <table class = 'highlight bordered'>
      <tbody>
          <tr>
              <td>
                  <b><i>Nombre: </i></b>
              </td>
              <td>{!!$product->name!!}</td>
          </tr>
          <tr>
              <td>
                  <b><i>Renta 1er mes: </i></b>
              </td>
              <td>${{number_format($product->cost, 2)}}</td>
          </tr>
          <tr>
              <td>
                  <b><i>Renta apartir 2do mes : </i></b>
              </td>
              <td>${{number_format($product->costmonth, 2)}}</td>
          </tr>
          <tr>
              <td>
                  <b><i>Valor del equipo : </i></b>
              </td>
              <td>${{number_format($product->product_value, 2)}}</td>
          </tr>
          <tr>
              <td>
                  <b><i>Precio extra 1: </i></b>
              </td>
              <td>${{number_format($product->extraprice1, 2)}}</td>
          </tr>
          <tr>
              <td>
                  <b><i>Precio extra 2:</i></b>
              </td>
              <td>${{number_format($product->extraprice2, 2)}}</td>
          </tr>
          <tr>
              <td>
                  <b><i>Precio extra 3: </i></b>
              </td>
              <td>${{number_format($product->extraprice3, 2)}}</td>
          </tr>
          <tr>
              <td>
                  <b><i>Precio extra 4: </i></b>
              </td>
              <td>${{number_format($product->extraprice4, 2)}}</td>
          </tr>
      </tbody>
  </table>
</div>
@endsection
