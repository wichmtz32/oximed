@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div id="vue-app">
  <div class="col m12">
    @if($nueva)
    <h2>Nuevo producto</h2>
    @else
    <h2>Editar producto</h2>
    @endif
  </div>
  <br>
  @if($nueva)
  <form method = 'POST' action = '{!!url("product")!!}' @submit="validar">
  @else
  <form method = 'POST' action = '{!! url("product")!!}/{!!$product->id!!}/update' @submit="validar">
  @endif
      <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
      <div class="input-field col s6">
          <input id="name" name = "name" type="text" class="validate" v-model="name" required>
          <label for="name">Nombre</label>
      </div>
      <div class="input-field col s2">
          <input id="cost" name = "cost" type="number" step="any" class="validate" v-model="cost" required>
          <label for="cost">Renta 1er mes</label>
      </div>
      <div class="input-field col s2">
          <input id="costmonth" name = "costmonth" type="number" step="any" class="validate" v-model="costmonth" required>
          <label for="costmonth">Renta apartir 2do mes</label>
      </div>
      <div class="input-field col s2">
          <input id="product_value" name = "product_value" type="number" step="any" class="validate" v-model="product_value" required>
          <label for="product_value">Valor equipo</label>
      </div>
      <div style="clear:both"></div>
      <div class="input-field col s2">
          <input id="extraprice1" name = "extraprice1" type="number" step="any" class="validate" v-model="extraprice1">
          <label for="extraprice1">Costo extra 1</label>
      </div>
      <div class="input-field col s2">
          <input id="extraprice2" name = "extraprice2" type="number" step="any" class="validate" v-model="extraprice2">
          <label for="extraprice2">Costo extra 2</label>
      </div>
      <div class="input-field col s2">
          <input id="extraprice3" name = "extraprice3" type="number" step="any" class="validate" v-model="extraprice3">
          <label for="extraprice3">Costo extra 3</label>
      </div>
      <div class="input-field col s2">
          <input id="extraprice4" name = "extraprice4" type="number" step="any" class="validate" v-model="extraprice4">
          <label for="extraprice4">Costo extra 4</label>
      </div>
      <div style="clear:both"></div>
      <div class="col s12">
        <h5 class="col s3">Numeros de serie</h5>
        <div class="col s3 center">
          <button type="button" class="btn btn-small green" @click="addSerial()">agregar numero</button>
        </div>
      </div>
      <div class="col s4" v-for="(s, index) in serials">
          <br>
          <div class="col s1">
            @{{index + 1}}.
          </div>
          <div class="input-field col s8">
            <input id="serials" name = "serials[]" v-model="s.number" type="text" class="validate" required>
            <label for="serials">Numero de serie</label>
          </div>
          <div class="col s3 center">
            <button type="button" class="btn red btn-small col s12" @click="deleteSerial(index)"><i class="material-icons">clear</i></button>
          </div>
      </div>
      <div style="clear:both"></div>
      <br><br><br>
      <button class = 'btn red' type ='submit'>Guardar</button>
  </form>
</div>
@endsection

@section('scripts')
  <script type="text/javascript">
  var app = new Vue({
    el: '#vue-app',
    data: {
      name: '',
      cost: '',
      costmonth: '',
      extraprice1: '',
      extraprice2: '',
      extraprice3: '',
      extraprice4: '',
      product_value: '',
      serials: []
    },
    mounted: function () {
      let t = this;
      @if($nueva)
      @else
        t.name = '{{$product->name}}';
        t.cost = '{{$product->cost}}';
        t.costmonth = {{$product->costmonth or 'null'}};
        t.extraprice1 = {{$product->extraprice1 or 'null'}};
        t.extraprice2 = {{$product->extraprice2 or 'null'}};
        t.extraprice3 = {{$product->extraprice3 or 'null'}};
        t.extraprice4 = {{$product->extraprice4 or 'null'}};
        t.product_value = {{$product->product_value or 'null'}};

        @foreach($product->serials as $s)
          t.serials.push({
            number: '{{$s->number}}',
          });
        @endforeach
      @endif
    },
    methods:{
      addSerial: function(){
        this.serials.push({
          number: '',
        });
      },
      deleteSerial: function(index){
        this.serials.splice(index, 1);
      },
      validar: function(e){
        for (var i = 0; i < this.serials.length; i++) {
          for (var j = 0; j < this.serials.length; j++) {
            if(i != j){
              if(this.serials[i].number == this.serials[j].number){
                e.preventDefault();
                alert("El numero de serie en la posicion " + (i+1).toString() + " se encuentra repetido en la posicion " + (j+1).toString() + ", eliminelo o modifiquelo antes de continuar.");
                return false;
              }
            }
          }
        }
        return true;
      }
    },
  });
  </script>
@endsection