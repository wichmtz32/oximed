@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')
  <style media="screen">
    body{
      font-size: 13px;
    }
    #tabla tr td, th{
      margin: 0px !important;
      padding: 1px !important;
    }
  </style>
<div>
  <div class="col m10">
    <h4 style="padding: 0px !important; margin: 0px !important;">Productos</h4><br>
  </div>
  <div class="col m2">
      <form class = 'col s3' method = 'get' action = '{!!url("product")!!}/create'>
        @can('a_producto')
        <button class='btn' type = 'submit'>NUEVO</button>
        @endcan
      </form>
  </div>
  <table class="striped" id="tabla">
      <thead>
          <th>Producto</th>
          <th>Valor del equipo</th>
          <th>Renta 1er mes</th>
          <th>Renta apartir 2do mes</th>
          <th></th>
      </thead>
      <tbody>
          @foreach($products as $product)
          <tr>
              <td>{!!$product->name!!}</td>
              <td>${{number_format($product->product_value, 2)}}</td>
              <td>${{number_format($product->cost, 2)}}</td>
              <td>${{number_format($product->costmonth,2)}}</td>
              <td>
                  <div class = 'row'>
                      @can('m_producto')
                      <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/product/{!!$product->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                      @endcan
                      @can('b_producto')
                      <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/product/{!!$product->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                      @endcan
                      <a href = '#' class = 'viewShow btn-floating orange' data-link = '/product/{!!$product->id!!}'><i class = 'material-icons'>info</i></a>
                  </div>
              </td>
          </tr>
          @endforeach
      </tbody>
  </table>
  {!! $products->render() !!}
</div>
@endsection
