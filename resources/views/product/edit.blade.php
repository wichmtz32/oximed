@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div>
  <div class="col m12">
    <h2>Editar producto</h2>
  </div>
    <br>
    <form method = 'POST' action = '{!! url("product")!!}/{!!$product->id!!}/update'>
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="name" name = "name" type="text" value="{!!$product->name!!}" class="validate" required>
            <label for="name">Nombre</label>
        </div>
        <div class="input-field col s2">
            <input id="cost" name = "cost" value="{!!$product->cost!!}"  type="number" step="any" class="validate" required>
            <label for="cost">Renta 1er mes</label>
        </div>
        <div class="input-field col s2">
            <input id="costmonth" name = "costmonth" value="{!!$product->costmonth!!}"  type="number" step="any" class="validate" required>
            <label for="costmonth">Renta apartir 2do mes</label>
        </div>
        <div class="input-field col s2">
            <input id="product_value" name = "product_value" value="{!!$product->product_value!!}" type="number" step="any" class="validate" required>
            <label for="product_value">Valor equipo</label>
        </div>
        <div style="clear:both"></div>
        <div class="input-field col s2">
            <input id="extraprice1" name = "extraprice1" value="{!!$product->extraprice1!!}"  type="number" step="any" class="validate">
            <label for="extraprice1">Costo extra 1</label>
        </div>
        <div class="input-field col s2">
            <input id="extraprice2" name = "extraprice2" value="{!!$product->extraprice2!!}"  type="number" step="any" class="validate">
            <label for="extraprice2">Costo extra 2</label>
        </div>
        <div class="input-field col s2">
            <input id="extraprice3" name = "extraprice3" value="{!!$product->extraprice3!!}"  type="number" step="any" class="validate">
            <label for="extraprice3">Costo extra 2</label>
        </div>
        <div class="input-field col s2">
            <input id="extraprice4" name = "extraprice4" value="{!!$product->extraprice4!!}"  type="number" step="any" class="validate">
            <label for="extraprice4">Costo extra 4</label>
        </div>
        <div style="clear:both"></div>
        <br><br><br>
        <button class = 'btn red' type ='submit'>guardar</button>
    </form>
</div>
@endsection
