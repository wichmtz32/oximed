<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">



		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

		<title>@yield('title')</title>

		<style>

		@media only screen and (min-width: 900px) {
				#contendor{
					padding-left: 320px;
				}
			}

			table tr td{
				padding-bottom: 0px !important;
				padding-top: 5px !important;
				text-align: center !important;

			}
			table tr th{
				text-align: center !important;
			}
			.picker__box{
					color: #004d99;
			}
			.picker__date-display, .picker__weekday-display{
					background-color: #004d99;
			}
			div.picker__day.picker__day--infocus.picker__day--selected.picker__day--highlighted {
					background-color: #004d99;
			}
			button.picker__today, button.picker__close {
					color: #004d99;
			}
			ul.dropdown-content.select-dropdown li span {
					color: #004d99;
			}
		 input[type=text]:focus + label {
			 color: #004d99 !important;
		 }
		 input[type=number]:focus + label {
			 color: #004d99 !important;
		 }
		 input[type=email]:focus + label {
			 color: #004d99 !important;
		 }
		 input[type=password]:focus + label {
			 color: #004d99 !important;
		 }
		 input[type=text]:focus {
			 border-bottom: 1px solid #004d99 !important;
			 box-shadow: 0 1px 0 0 #004d99 !important;
		 }
		 textarea:focus {
			 border-bottom: 1px solid #004d99 !important;
			 box-shadow: 0 1px 0 0 #004d99 !important;
		 }
		 input[type=number]:focus {
			 border-bottom: 1px solid #004d99 !important;
			 box-shadow: 0 1px 0 0 #004d99 !important;
		 }
		 input[type=email]:focus {
			 border-bottom: 1px solid #004d99 !important;
			 box-shadow: 0 1px 0 0 #004d99 !important;
		 }
		 input[type=password]:focus {
			 border-bottom: 1px solid #004d99 !important;
			 box-shadow: 0 1px 0 0 #004d99 !important;
		 }
		 .checkbox[type="checkbox"].filled-in:checked + label:after{
			     border: 2px solid #004d99;
			     background-color: #004d99;
			}
		 .input-field .prefix.active {
			 color: #004d99 !important;
		 }
		 .btn{
			 background-color: #004d99;
		 }
		 .btn:hover{
			 background-color: #004d99;
		 }

	 .check:checked + label:before {
			border-right: 2px solid #004d99; /* You need to change the colour here */
			border-bottom: 2px solid #004d99; /* And here */
		}

			html{
				 font-family: GillSans, Calibri, Trebuchet, sans-serif;
			}
			#slide-out li a
			{
					color: white;
					background-color: #00264d;
			}
			#slide-out li a:hover{
				background-color: #004d99;
			}
			i.icon-white {
			    color: white !important;
			}
			i.icon-black {
			    color: black !important;
			}
			.tab a
      {
        font-weight: bold;
				color: white !important;
      }
			.tab a.active
      {
      	background-color: white !important;
        font-weight: bold;
				color: #004d99 !important;
      }

			.btn-small {
        height: 26px;
        line-height: 24px;
        padding-top: 0px;
        padding-bottom: 0px;
        padding-left: 15px;
        padding-right: 15px;
      }
			.center {
          margin-left: auto;
          margin-right: auto;
          display: block;
          margin-top: 15px;
      }
			#toast-container {
				top: auto !important;
				right: auto !important;
				bottom: 10%;
				left:25%;
			}
			.swal-button:focus {
				outline:0;
				background-color: #26a69a !important;
				color: white;
			}
			.logo:hover{
				background-color: inherit !important;
			}


			</style>
	</head>

	<body>
	<div >
		<ul id="slide-out" class="side-nav fixed z-depth-5" style="display: flex; flex-direction: column; background-color: #00264d">
      		<li>
				<div style="text-align:center">
          			<a class="logo" href="/"><img src="/images/logo.png" height="110px" width="auto"></a>
        		</div>
      		</li>
      		<br>
			<li><a href="/client"><i class="material-icons icon-white">person</i>Clientes</a></li>
			<li><a href="/driver"><i class="material-icons icon-white">local_shipping</i>Conductores</a></li>
			<li><a href="/contract"><i class="material-icons icon-white">description</i>Contratos</a></li>
			<li><a href="/product"><i class="material-icons icon-white">local_hospital</i>Productos</a></li>
			<li style="padding-left: 17px">
        <ul class="collapsible collapsible-accordion">
          <li><a class="collapsible-header"><i class="material-icons icon-white">settings</i><i class="material-icons icon-white right">arrow_drop_down</i>Configuración</a>
            <div class="collapsible-body">
              <ul>
								<li><a href="/users"><i class="material-icons icon-white">group</i>Usuarios</a></li>
								<li><a href="/roles"><i class="material-icons icon-white">group_add</i>Roles</a></li>
              </ul>
            </div>
          </li>
        </ul>
		<li><div class="divider blue accent-1"></div></li>
		<li>
			<div style="text-align:center">
          		<a class="logo" href="/"><img src="/images/solder.png" height="110px" width="60%"></a>
        	</div>
      	</li>
		<li><a href="/order"><i class="material-icons icon-white">build</i>Reparaciones</a></li>
		<li><a href="/invoice"><i class="material-icons icon-white">playlist_add_check</i>Almacen</a></li>
      <li>
			<div class="divider blue-grey darken-3"></div></li>
      @guest
          <li><a href="{{ route('login') }}"><i class="material-icons icon-white">exit_to_app</i>Login</a></li>
      @else
          <li><a style="text-decoration:none; font-weight:bold;"><i class="material-icons icon-white">person_outline</i>{{Auth::user()->name}}</a></li>
          <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <i class="material-icons icon-white">exit_to_app</i>Salir
            </a>
          </li>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
             {{ csrf_field() }}
          </form>
      @endguest
			<li style="margin-top:auto"><a target="_blank" href="http://www.codgo.com.mx"><img src="/images/codgo.png" height="16px" width="auto"></a></li>
    </ul>
    <a href="" data-activates="slide-out" class="button-collapse"><i class="material-icons icon-black">menu</i></a>

		<div class="row">
      <div class="col s12" id="contendor">
        @yield('content')
      </div>
			<div id="modal1" class="modal">
				<div class = "row AjaxisModal">
				</div>
			</div>
    </div>
	</div>


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>


   <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>



		<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
		<script src="https://unpkg.com/axios/dist/axios.min.js"></script>


		<script>  var baseURL = "{{ URL::to('/') }}"</script>
		<script src = "{{URL::asset('js/AjaxisMaterialize.js')}}"></script>
		<script src = "{{URL::asset('js/scaffold-interface-js/customA.js')}}"></script>
		<script type="text/javascript">
			$( document ).ready(function()
			{
    		$('.tooltipped').tooltip();
				$('select').material_select();
				$(".button-collapse").sideNav();
				$('.modal').modal();
				$('.tabs').tabs();
				$('.datepicker').pickadate();
			});
			$(document).ready(function(){
				@if(session()->has('message'))
					var message = "{{session()->get('message')}}";
					Materialize.toast(message, 4000);
				@endif
			});


		</script>
    <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		@yield('scripts')

	</body>
</html>
