@extends('scaffold-interface.layouts.defaultMaterialize')
@section('content')
  <style media="screen">
    table td, th{
      padding: 4px !important;
    }
  </style>
  <div>
      <h4 class="col m10">Usuarios</h4><br>
      <div class="col m2">
        @can('a_usuario')
        <a href="{{route('users_create')}}" class="btn">nuevo</a>
        @endcan
      </div>
      <div class="col m12">
        <table class="responsive-table striped">
          <thead>
            <tr>
                <th style="width: 30%">Nombre</th>
                <th style="width: 25%">Email</th>
                <th style="width: 20%">Role</th>
                <th style="width: 5%" colspan="3"></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($users as $user)
            <tr>
              <td>{{$user->name}}</td>
              <td>{{$user->email}}</td>
              <td  style="padding-top: 10px !important"><div class="chip blue darken-4 white-text">{{$user->getRoleNames()->first()}}</div></td>
              <td >
                @can('m_usuario')
                <a href='{{route('users_edit', ['user' => $user->id])}}' class = 'viewEdit btn-floating blue' ><i class = 'material-icons'>edit</i></a>
                @endcan
              </td>
              <td>
                @can('b_usuario')
                <form action="{{route('users_delete', ['user' => $user->id])}}" method="post">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
                  <button class = 'delete btn-floating red' type="submit" name="button" ><i class = 'material-icons'>delete</i></button>
                </form>
                @endcan
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    <div class="row">
      <div class="col m12">
        {{$users->render()}}
      </div>
    </div>
</div>
@endsection
