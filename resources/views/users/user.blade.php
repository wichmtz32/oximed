@extends('scaffold-interface.layouts.defaultMaterialize')


@section('content')
  <div id='vue-app'>
    @if(count($errors) > 0)
    <div class="row">
      <div class="col m10 offset-m2 alert-danger alert">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
    </div>
    @endif
    <div class="col m12">
      @if($nueva)
        <h2>Nuevo Usuario</h2>
      @else
        <h2>Editar Usuario</h2>
      @endif
    </div>
    <div class="col m12">
      @if($nueva)
      <form class="col m12" method="post" action="{{route('users_store')}}">
      @else
      <form class="col m12" method="post" action="{{route('users_update', ['user' => $user->id])}}">
        {{ method_field('PUT') }}
      @endif
        {{ csrf_field() }}
        <div class="input-field col m5">
          <input id="nombre" type="text" name="nombre" v-model="usuario[0].nombre" class="validate" required>
          <label for="nombre">Nombre</label>
        </div>
        <div class="input-field col m4">
          <input id="correo" type="email" name="email"  v-model="usuario[0].email" class="validate" required>
          <label for="correo">Correo</label>
        </div>
        <div class="input-field col m3">
          <input id="telefono" type="text" name="telefono"  v-model="usuario[0].telefono"  class="validate">
          <label for="telefono">Telefono</label>
        </div>
        <div class="input-field col m4">
          <input id="password" type="password" name="password" class="validate">
          @if($nueva)
            <label for="password">Contraseña</label>
          @else
            <label for="password">Nueva contraseña</label>
          @endif
        </div>
        <div class="input-field col m4">
          <input id="password2" type="password" name="password_confirmation" class="validate">
          <label for="password2">Repetir contraseña</label>
        </div>
        <div class="col m4">
          <label>Role</label>
          <select name="rol" v-model="usuario[0].role" class="browser-default">
            <option v-for="rol in roles" v-bind:value="rol.nombre">@{{rol.nombre}}</option>
          </select>
        </div>
        <div class="col m2">
          <br><br>
          <button type="submit" name="button" class="btn red">Guardar</button>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
        $('select').material_select();
      });
      var app = new Vue({
        el: '#vue-app',
        data: {
          roles: [],
          usuario: []
        },
        mounted: function () {
          this.$nextTick(function () {
            let t = this;

            @foreach($roles as $rol)
              var reg = {
                nombre: '{{$rol->name}}'
              };
              t.roles.push(reg);
            @endforeach

            @if($nueva)
              var reg = {
                nombre: "{{old('nombre')}}",
                email: "{{old('email')}}",
                telefono: "{{old('telefono')}}",
                role: '{{$roles[0]->name}}',
              };
              t.usuario.push(reg);
            @else
            var reg = {
              nombre:   '{{$user->name}}',
              email:    '{{$user->email}}',
              telefono: '{{$user->phone}}',
              role:     '{{$user->roles()->first()->name}}',
            };
            t.usuario.push(reg);
            @endif
          });
        }
      });

  </script>

@endsection
