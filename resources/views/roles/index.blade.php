
@extends('scaffold-interface.layouts.defaultMaterialize')
@section('content')

  <div>
    <style media="screen">
      table td, th{
        padding: 2px !important;
      }
    </style>
    <div class="col m12">
        <h4 class="col m10">Roles</h4><br>
        <div class="col m2">
          @can('a_role')
          <a href="{{route('roles_create')}}" class="btn">Nuevo</a>
          @endcan
        </div>
        <div class="col m12">
          <table class="responsive-table striped">
            <thead>
              <tr>
                  <th style="width: 85%">Nombre del Rol</th>
                  <th style="width: 10%" colspan="2"></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($roles as $role)
              <tr>
                <td style="width: 80%">{{$role->name}}</td>
                <td>
                  @can('m_role')
                  <a href="{{route('roles_edit', ['role' => $role->id])}}" class="viewEdit btn-floating blue"><i class="material-icons">edit</i></a>
                  @endcan
                </td>
                <td>
                  @can('b_role')
                  <form action="{{route('roles_delete', ['role' => $role->id])}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit" class="delete btn-floating red" name="button"><i class='material-icons'>delete</i></button>
                  </form>
                  @endcan
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
    <div class="row">
      <div class="col m12">
        {{$roles->render()}}
      </div>
    </div>
  </div>


@endsection
