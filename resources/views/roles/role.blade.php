@extends('scaffold-interface.layouts.defaultMaterialize')
@section('content')
  <style media="screen">
    table td, th{
      padding: 5px !important;
    }
  </style>
  <div id="vue-app" >
    <div class="row">
      <div class="col m4">
        @if($nueva)
          <h2>Nuevo Role</h2>
        @else
          <h2>Editar Role</h2>
        @endif
      </div>
    </div>
      <div class="col m12">
        @if($nueva)
        <form class="col m12" method="post" action="{{route('roles_store')}}">
        @else
        <form class="col m12" method="post" action="{{route('roles_update', ['role' => $role->id])}}">
        {{ method_field('PUT') }}
        @endif
          {{ csrf_field() }}
          <div class="row">
            <div class="input-field col m5">
              <input type="text" name="nombre" v-model="role[0].nombre" class="validate" maxlength="25" required>
              <label>Nombre Role</label>
            </div>
            <div style="clear: both;"></div>
            @if($nueva)
            <h5> Asignar permisos </h5>
            @else
            <h5> Editar permisos </h5>
            @endif
            <table class="striped">
              <thead>
                <tr>
                  <th style="width: 40%"></th>
                  <th style="width: 15%">Alta</th>
                  <th style="width: 15%">Baja</th>
                  <th style="width: 15%">Modificación</th>
                  <th style="width: 15%">Consulta</th>
                  <th style="width: 15%">Validacion</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>Usuarios</th>
                  <td><input type="checkbox" class="check" name="permisos[]" value="a_usuario" v-model="role[0].a_usuario" id="1"/><label for="1"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="b_usuario" v-model="role[0].b_usuario" id="2"/><label for="2"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="m_usuario" v-model="role[0].m_usuario" id="3"/><label for="3"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="c_usuario" v-model="role[0].c_usuario" id="4"/><label for="4"></label></p></td>
                  <td><h5>-&nbsp;&nbsp;</h5></td>
                </tr>
                <tr>
                  <th>Roles</th>
                  <td><input type="checkbox" class="check" name="permisos[]" value="a_role" v-model="role[0].a_role" id="5"/><label for="5"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="b_role" v-model="role[0].b_role" id="6"/><label for="6"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="m_role" v-model="role[0].m_role" id="7"/><label for="7"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="c_role" v-model="role[0].c_role" id="8"/><label for="8"></label></p></td>
                  <td><h5>-&nbsp;&nbsp;&nbsp;</h5> </td>
                </tr>
                <tr>
                  <th>Clientes</th>
                  <td><input type="checkbox" class="check" name="permisos[]" value="a_cliente" v-model="role[0].a_cliente" id="9"/><label for="9"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="b_cliente" v-model="role[0].b_cliente" id="10"/><label for="10"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="m_cliente" v-model="role[0].m_cliente" id="11"/><label for="11"></label></p></td>
                  <td><input type="checkbox"  class="check"name="permisos[]" value="c_cliente" v-model="role[0].c_cliente" id="12"/><label for="12"></label></p></td>
                  <td><h5>-&nbsp;&nbsp;&nbsp;</h5> </td>
                </tr>
                <tr>
                  <th>Contratos</th>
                  <td><input type="checkbox" class="check" name="permisos[]" value="a_contrato" v-model="role[0].a_contrato" id="13"/><label for="13"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="b_contrato" v-model="role[0].b_contrato" id="14"/><label for="14"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="m_contrato" v-model="role[0].m_contrato" id="15"/><label for="15"></label></p></td>
                  <td><input type="checkbox"  class="check"name="permisos[]" value="c_contrato" v-model="role[0].c_contrato" id="16"/><label for="16"></label></p></td>
                  <td><h5>-&nbsp;&nbsp;&nbsp;</h5> </td>
                </tr>
                <tr>
                  <th>Conductores</th>
                  <td><input type="checkbox" class="check" name="permisos[]" value="a_conductor" v-model="role[0].a_conductor" id="17"/><label for="17"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="b_conductor" v-model="role[0].b_conductor" id="18"/><label for="18"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="m_conductor" v-model="role[0].m_conductor" id="19"/><label for="19"></label></p></td>
                  <td><input type="checkbox"  class="check"name="permisos[]" value="c_conductor" v-model="role[0].c_conductor" id="20"/><label for="20"></label></p></td>
                  <td><h5>-&nbsp;&nbsp;&nbsp;</h5> </td>
                </tr>
                <tr>
                  <th>Productos</th>
                  <td><input type="checkbox" class="check" name="permisos[]" value="a_producto" v-model="role[0].a_producto" id="21"/><label for="21"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="b_producto" v-model="role[0].b_producto" id="22"/><label for="22"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="m_producto" v-model="role[0].m_producto" id="23"/><label for="23"></label></p></td>
                  <td><input type="checkbox"  class="check"name="permisos[]" value="c_producto" v-model="role[0].c_producto" id="24"/><label for="24"></label></p></td>
                </tr>
                <tr>
                  <th>Pagos</th>
                  <td><input type="checkbox" class="check" name="permisos[]" value="a_pagos" v-model="role[0].a_pagos" id="25"/><label for="25"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="b_pagos" v-model="role[0].b_pagos" id="26"/><label for="26"></label></p></td>
                  <td><input type="checkbox" class="check" name="permisos[]" value="m_pagos" v-model="role[0].m_pagos" id="27"/><label for="27"></label></p></td>
                  <td><input type="checkbox"  class="check"name="permisos[]" value="c_pagos" v-model="role[0].c_pagos" id="28"/><label for="28"></label></p></td>
                  <td><input type="checkbox"  class="check"name="permisos[]" value="v_pagos" v-model="role[0].v_pagos" id="29"/><label for="29"></label></p></td>
                </tr>
              </tbody>
            </table>
          </div>
          <br>
          <div class="col m3">
            @if($nueva)
            <button class = 'btn red' type ='submit'>Crear</button>
            @else
            <button class = 'btn red' type ='submit'>Guardar</button>
            @endif
          </div>
        </form>
      </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
  var app = new Vue({
    el: '#vue-app',
    data: {
      role: []
    },
    methods:{
      existe: function(variable){
        if(typeof(variable) != 'undefined')
          return true;
        else
          return false;
      }
    },
    mounted: function () {
      this.$nextTick(function () {
        let t = this;
        @if($nueva)
          t.role.push({
            nombre: '',
            a_usuario: false,
            b_usuario: false,
            m_usuario: false,
            c_usuario: false,
            a_cliente: false,
            b_cliente: false,
            m_cliente: false,
            c_cliente: false,
            a_role: false,
            b_role: false,
            m_role: false,
            c_role: false,
            a_contrato: false,
            b_contrato: false,
            m_contrato: false,
            c_contrato: false,
            a_conductor: false,
            b_conductor: false,
            m_conductor: false,
            c_conductor: false,
            a_producto: false,
            b_producto: false,
            m_producto: false,
            c_producto: false,
            a_pagos: false,
            b_pagos: false,
            m_pagos: false,
            c_pagos: false,
            v_pagos: false,
          });
        @else
          t.role.push({
            nombre: '{{$role->name}}',
            a_usuario: t.existe({{$role->hasPermissionTo('a_usuario')}}),
            b_usuario: t.existe({{$role->hasPermissionTo('b_usuario')}}),
            m_usuario: t.existe({{$role->hasPermissionTo('m_usuario')}}),
            c_usuario: t.existe({{$role->hasPermissionTo('c_usuario')}}),
            a_cliente: t.existe({{$role->hasPermissionTo('a_cliente')}}),
            b_cliente: t.existe({{$role->hasPermissionTo('b_cliente')}}),
            m_cliente: t.existe({{$role->hasPermissionTo('m_cliente')}}),
            c_cliente: t.existe({{$role->hasPermissionTo('c_cliente')}}),
            a_role: t.existe({{$role->hasPermissionTo('a_role')}}),
            b_role: t.existe({{$role->hasPermissionTo('b_role')}}),
            m_role: t.existe({{$role->hasPermissionTo('m_role')}}),
            c_role: t.existe({{$role->hasPermissionTo('c_role')}}),
            a_contrato: t.existe({{$role->hasPermissionTo('a_contrato')}}),
            b_contrato: t.existe({{$role->hasPermissionTo('b_contrato')}}),
            m_contrato: t.existe({{$role->hasPermissionTo('m_contrato')}}),
            c_contrato: t.existe({{$role->hasPermissionTo('c_contrato')}}),
            a_conductor: t.existe({{$role->hasPermissionTo('a_conductor')}}),
            b_conductor: t.existe({{$role->hasPermissionTo('b_conductor')}}),
            m_conductor: t.existe({{$role->hasPermissionTo('m_conductor')}}),
            c_conductor: t.existe({{$role->hasPermissionTo('c_conductor')}}),
            a_producto: t.existe({{$role->hasPermissionTo('a_producto')}}),
            b_producto: t.existe({{$role->hasPermissionTo('b_producto')}}),
            m_producto: t.existe({{$role->hasPermissionTo('m_producto')}}),
            c_producto: t.existe({{$role->hasPermissionTo('c_producto')}}),
            a_pagos: t.existe({{$role->hasPermissionTo('a_pagos')}}),
            b_pagos: t.existe({{$role->hasPermissionTo('b_pagos')}}),
            m_pagos: t.existe({{$role->hasPermissionTo('m_pagos')}}),
            c_pagos: t.existe({{$role->hasPermissionTo('c_pagos')}}),
            v_pagos: t.existe({{$role->hasPermissionTo('v_pagos')}}),
          });
        @endif
      });
    }
  });
  </script>
@endsection
