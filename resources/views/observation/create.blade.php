@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Create observation
    </h1>
    <form method = 'get' action = '{!!url("observation")!!}'>
        <button class = 'btn blue'>observation Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("observation")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="description" name = "description" type="text" class="validate">
            <label for="description">description</label>
        </div>
        <div class="input-field col s12">
            <select name = 'repair_id'>
                @foreach($repairs as $key => $value) 
                <option value="{{$key}}">{{$value}}</option>
                @endforeach 
            </select>
            <label>repairs Select</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection