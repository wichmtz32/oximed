@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        observation Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("observation")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New observation</button>
        </form>
        <ul id="dropdown" class="dropdown-content">
            <li><a href="http://127.0.0.1:8000/repair">Repair</a></li>
        </ul>
        <a class="col s3 btn dropdown-button #1e88e5 blue darken-1" href="#!" data-activates="dropdown">Associate<i class="mdi-navigation-arrow-drop-down right"></i></a>
    </div>
    <table>
        <thead>
            <th>description</th>
            <th>product</th>
            <th>serialNumber</th>
            <th>faults</th>
            <th>deliverDate</th>
            <th>status</th>
            <th>repairman</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th>deleted_at</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($observations as $observation) 
            <tr>
                <td>{!!$observation->description!!}</td>
                <td>{!!$observation->repair->product!!}</td>
                <td>{!!$observation->repair->serialNumber!!}</td>
                <td>{!!$observation->repair->faults!!}</td>
                <td>{!!$observation->repair->deliverDate!!}</td>
                <td>{!!$observation->repair->status!!}</td>
                <td>{!!$observation->repair->repairman!!}</td>
                <td>{!!$observation->repair->created_at!!}</td>
                <td>{!!$observation->repair->updated_at!!}</td>
                <td>{!!$observation->repair->deleted_at!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/observation/{!!$observation->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/observation/{!!$observation->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/observation/{!!$observation->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $observations->render() !!}

</div>
@endsection