@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show observation
    </h1>
    <form method = 'get' action = '{!!url("observation")!!}'>
        <button class = 'btn blue'>observation Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>description : </i></b>
                </td>
                <td>{!!$observation->description!!}</td>
            </tr>
            <tr>
                <td>
                    <b>
                        <i>product : </i>
                        <b>
                        </td>
                        <td>{!!$observation->repair->product!!}</td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <i>serialNumber : </i>
                                <b>
                                </td>
                                <td>{!!$observation->repair->serialNumber!!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>
                                        <i>faults : </i>
                                        <b>
                                        </td>
                                        <td>{!!$observation->repair->faults!!}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                <i>deliverDate : </i>
                                                <b>
                                                </td>
                                                <td>{!!$observation->repair->deliverDate!!}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>
                                                        <i>status : </i>
                                                        <b>
                                                        </td>
                                                        <td>{!!$observation->repair->status!!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>
                                                                <i>repairman : </i>
                                                                <b>
                                                                </td>
                                                                <td>{!!$observation->repair->repairman!!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>
                                                                        <i>created_at : </i>
                                                                        <b>
                                                                        </td>
                                                                        <td>{!!$observation->repair->created_at!!}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <b>
                                                                                <i>updated_at : </i>
                                                                                <b>
                                                                                </td>
                                                                                <td>{!!$observation->repair->updated_at!!}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <b>
                                                                                        <i>deleted_at : </i>
                                                                                        <b>
                                                                                        </td>
                                                                                        <td>{!!$observation->repair->deleted_at!!}</td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        @endsection