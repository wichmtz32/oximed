@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Create repairdetail
    </h1>
    <form method = 'get' action = '{!!url("repairdetail")!!}'>
        <button class = 'btn blue'>repairdetail Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("repairdetail")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="unitPrice" name = "unitPrice" type="text" class="validate">
            <label for="unitPrice">unitPrice</label>
        </div>
        <div class="input-field col s6">
            <input id="quantity" name = "quantity" type="text" class="validate">
            <label for="quantity">quantity</label>
        </div>
        <div class="input-field col s6">
            <input id="total" name = "total" type="text" class="validate">
            <label for="total">total</label>
        </div>
        <div class="input-field col s6">
            <input id="status" name = "status" type="text" class="validate">
            <label for="status">status</label>
        </div>
        <div class="input-field col s6">
            <input id="warranty" name = "warranty" type="text" class="validate">
            <label for="warranty">warranty</label>
        </div>
        <div class="input-field col s12">
            <select name = 'repair_id'>
                @foreach($repairs as $key => $value) 
                <option value="{{$key}}">{{$value}}</option>
                @endforeach 
            </select>
            <label>repairs Select</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection