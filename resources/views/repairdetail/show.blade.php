@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show repairdetail
    </h1>
    <form method = 'get' action = '{!!url("repairdetail")!!}'>
        <button class = 'btn blue'>repairdetail Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>unitPrice : </i></b>
                </td>
                <td>{!!$repairdetail->unitPrice!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>quantity : </i></b>
                </td>
                <td>{!!$repairdetail->quantity!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>total : </i></b>
                </td>
                <td>{!!$repairdetail->total!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>status : </i></b>
                </td>
                <td>{!!$repairdetail->status!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>warranty : </i></b>
                </td>
                <td>{!!$repairdetail->warranty!!}</td>
            </tr>
            <tr>
                <td>
                    <b>
                        <i>product : </i>
                        <b>
                        </td>
                        <td>{!!$repairdetail->repair->product!!}</td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <i>serialNumber : </i>
                                <b>
                                </td>
                                <td>{!!$repairdetail->repair->serialNumber!!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>
                                        <i>faults : </i>
                                        <b>
                                        </td>
                                        <td>{!!$repairdetail->repair->faults!!}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                <i>deliverDate : </i>
                                                <b>
                                                </td>
                                                <td>{!!$repairdetail->repair->deliverDate!!}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>
                                                        <i>status : </i>
                                                        <b>
                                                        </td>
                                                        <td>{!!$repairdetail->repair->status!!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>
                                                                <i>repairman : </i>
                                                                <b>
                                                                </td>
                                                                <td>{!!$repairdetail->repair->repairman!!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>
                                                                        <i>created_at : </i>
                                                                        <b>
                                                                        </td>
                                                                        <td>{!!$repairdetail->repair->created_at!!}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <b>
                                                                                <i>updated_at : </i>
                                                                                <b>
                                                                                </td>
                                                                                <td>{!!$repairdetail->repair->updated_at!!}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <b>
                                                                                        <i>deleted_at : </i>
                                                                                        <b>
                                                                                        </td>
                                                                                        <td>{!!$repairdetail->repair->deleted_at!!}</td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        @endsection