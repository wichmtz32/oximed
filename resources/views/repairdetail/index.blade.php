@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        repairdetail Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("repairdetail")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New repairdetail</button>
        </form>
        <ul id="dropdown" class="dropdown-content">
            <li><a href="http://127.0.0.1:8000/repair">Repair</a></li>
        </ul>
        <a class="col s3 btn dropdown-button #1e88e5 blue darken-1" href="#!" data-activates="dropdown">Associate<i class="mdi-navigation-arrow-drop-down right"></i></a>
    </div>
    <table>
        <thead>
            <th>unitPrice</th>
            <th>quantity</th>
            <th>total</th>
            <th>status</th>
            <th>warranty</th>
            <th>product</th>
            <th>serialNumber</th>
            <th>faults</th>
            <th>deliverDate</th>
            <th>status</th>
            <th>repairman</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th>deleted_at</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($repairdetails as $repairdetail) 
            <tr>
                <td>{!!$repairdetail->unitPrice!!}</td>
                <td>{!!$repairdetail->quantity!!}</td>
                <td>{!!$repairdetail->total!!}</td>
                <td>{!!$repairdetail->status!!}</td>
                <td>{!!$repairdetail->warranty!!}</td>
                <td>{!!$repairdetail->repair->product!!}</td>
                <td>{!!$repairdetail->repair->serialNumber!!}</td>
                <td>{!!$repairdetail->repair->faults!!}</td>
                <td>{!!$repairdetail->repair->deliverDate!!}</td>
                <td>{!!$repairdetail->repair->status!!}</td>
                <td>{!!$repairdetail->repair->repairman!!}</td>
                <td>{!!$repairdetail->repair->created_at!!}</td>
                <td>{!!$repairdetail->repair->updated_at!!}</td>
                <td>{!!$repairdetail->repair->deleted_at!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/repairdetail/{!!$repairdetail->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/repairdetail/{!!$repairdetail->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/repairdetail/{!!$repairdetail->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $repairdetails->render() !!}

</div>
@endsection