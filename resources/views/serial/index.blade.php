@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        serial Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("serial")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New serial</button>
        </form>
        <ul id="dropdown" class="dropdown-content">
            <li><a href="http://localhost:8000/product">Product</a></li>
        </ul>
        <a class="col s3 btn dropdown-button #1e88e5 blue darken-1" href="#!" data-activates="dropdown">Associate<i class="mdi-navigation-arrow-drop-down right"></i></a>
    </div>
    <table>
        <thead>
            <th>number</th>
            <th>name</th>
            <th>cost</th>
            <th>costmonth</th>
            <th>product_value</th>
            <th>extraprice1</th>
            <th>extraprice2</th>
            <th>extraprice3</th>
            <th>extraprice4</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th>deleted_at</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($serials as $serial) 
            <tr>
                <td>{!!$serial->number!!}</td>
                <td>{!!$serial->product->name!!}</td>
                <td>{!!$serial->product->cost!!}</td>
                <td>{!!$serial->product->costmonth!!}</td>
                <td>{!!$serial->product->product_value!!}</td>
                <td>{!!$serial->product->extraprice1!!}</td>
                <td>{!!$serial->product->extraprice2!!}</td>
                <td>{!!$serial->product->extraprice3!!}</td>
                <td>{!!$serial->product->extraprice4!!}</td>
                <td>{!!$serial->product->created_at!!}</td>
                <td>{!!$serial->product->updated_at!!}</td>
                <td>{!!$serial->product->deleted_at!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/serial/{!!$serial->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/serial/{!!$serial->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/serial/{!!$serial->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $serials->render() !!}

</div>
@endsection