@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show serial
    </h1>
    <form method = 'get' action = '{!!url("serial")!!}'>
        <button class = 'btn blue'>serial Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>number : </i></b>
                </td>
                <td>{!!$serial->number!!}</td>
            </tr>
            <tr>
                <td>
                    <b>
                        <i>name : </i>
                        <b>
                        </td>
                        <td>{!!$serial->product->name!!}</td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <i>cost : </i>
                                <b>
                                </td>
                                <td>{!!$serial->product->cost!!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>
                                        <i>costmonth : </i>
                                        <b>
                                        </td>
                                        <td>{!!$serial->product->costmonth!!}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                <i>product_value : </i>
                                                <b>
                                                </td>
                                                <td>{!!$serial->product->product_value!!}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>
                                                        <i>extraprice1 : </i>
                                                        <b>
                                                        </td>
                                                        <td>{!!$serial->product->extraprice1!!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>
                                                                <i>extraprice2 : </i>
                                                                <b>
                                                                </td>
                                                                <td>{!!$serial->product->extraprice2!!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>
                                                                        <i>extraprice3 : </i>
                                                                        <b>
                                                                        </td>
                                                                        <td>{!!$serial->product->extraprice3!!}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <b>
                                                                                <i>extraprice4 : </i>
                                                                                <b>
                                                                                </td>
                                                                                <td>{!!$serial->product->extraprice4!!}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <b>
                                                                                        <i>created_at : </i>
                                                                                        <b>
                                                                                        </td>
                                                                                        <td>{!!$serial->product->created_at!!}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>
                                                                                                <i>updated_at : </i>
                                                                                                <b>
                                                                                                </td>
                                                                                                <td>{!!$serial->product->updated_at!!}</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <b>
                                                                                                        <i>deleted_at : </i>
                                                                                                        <b>
                                                                                                        </td>
                                                                                                        <td>{!!$serial->product->deleted_at!!}</td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                        @endsection