@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Create serial
    </h1>
    <form method = 'get' action = '{!!url("serial")!!}'>
        <button class = 'btn blue'>serial Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("serial")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="number" name = "number" type="text" class="validate">
            <label for="number">number</label>
        </div>
        <div class="input-field col s12">
            <select name = 'product_id'>
                @foreach($products as $key => $value) 
                <option value="{{$key}}">{{$value}}</option>
                @endforeach 
            </select>
            <label>products Select</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection