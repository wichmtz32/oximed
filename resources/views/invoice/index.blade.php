@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div id="vue-app" class="container">
    <div class="col s12">
      <div class="col m12">
        <h4 style="padding: 0px !important; margin: 0px !important;">Facturas</h4><br>
      </div>
        <form method = 'get' class="col m4" action = '{!!url("invoice")!!}/create'>
            <button   align="right" class = 'btn col m12' type = 'submit'>Nueva</button>
        </form>
        <form method = 'get' class="col m4" action = '{!!url("person")!!}'>
            <button   align="right" class = 'btn col m12' type = 'submit'>Usuarios</button>
        </form>
    </div>
    <div class="col s12">
        <div class="input-field col s4">
            <input type="text" class="datepicker col-4" id="fechaInicial" name="fechaInicial">
            <label for="date">Fecha Inicial:</label>
        </div>
        <div class="input-field col s4">
            <input type="text" class="datepicker col-4" id="fechaFinal" name="fechaFinal">
            <label for="date2">Fecha Final:</label>
        </div>
    </div>
    <div class="col s12">
        <div class="col s2">
            <input type="checkbox" name="REM" id="REM" value="REM" v-model="checkBoxes" checked="checked">
            <label for="REM">REM</label>
        </div>
        <div class="col s2">
            <input type="checkbox" name="NV" id="NV" value="NV" v-model="checkBoxes">
            <label for="NV">NV</label>
        </div>
        <div class="col s2">
            <input type="checkbox" name="FCON" id="FCON" value="FCON" v-model="checkBoxes">
            <label for="FCON">FCON</label>
        </div>
        <div class="col s2">
            <input type="checkbox" name="FCR" id="FCR" value="FCR" v-model="checkBoxes">
            <label for="FCR">FCR</label>
        </div>
        <div class="col s4">
            <button   align="right" class = 'btn col m12' v-on:click="verificar" type = 'button'>Verificar</button>
        </div>
    </div>
    <table id="tabla"  class="display" style="width:100%">
        <thead>
            <th>Tipo</th>
            <th>Factura</th>
            <th>Usuario</th>
            <th>Fecha</th>
        </thead>
        <tbody>
            @foreach($invoices as $invoice)
            <tr >
              @if($invoice->type =="REM")

                  <td style="background-color:#778899">{!!$invoice->type!!}</td>
                  <td  style="background-color:#778899">{!!$invoice->invoiceNumber!!}</td>
                  <td  style="background-color:#778899">{!!$invoice->person_id!!}</td>
                  <td  style="background-color:#778899">{!!$invoice->created_at!!}</td>
              @elseif($invoice->type =="NV")

                <td  style="background-color:#45e3c3">{!!$invoice->type!!}</td>
                <td style="background-color:#45e3c3">{!!$invoice->invoiceNumber!!}</td>
                <td style="background-color:#45e3c3">{!!$invoice->person_id!!}</td>
                <td style="background-color:#45e3c3">{!!$invoice->created_at!!}</td>
              @elseif($invoice->type =="FCON")
                  <td style="background-color:#98FB98">{!!$invoice->type!!}</td>
                  <td  style="background-color:#98FB98">{!!$invoice->invoiceNumber!!}</td>
                  <td  style="background-color:#98FB98">{!!$invoice->person_id!!}</td>
                  <td  style="background-color:#98FB98">{!!$invoice->created_at!!}</td>
              @elseif($invoice->type =="FCR")
                  <td  style="background-color:#b39ddb">{!!$invoice->type!!}</td>
                  <td  style="background-color:#b39ddb">{!!$invoice->invoiceNumber!!}</td>
                  <td style="background-color:#b39ddb">{!!$invoice->person_id!!}</td>
                  <td  style="background-color:#b39ddb">{!!$invoice->created_at!!}</td>
              @endif
            </tr>
            @endforeach
        </tbody>
    </table>
    <div id="modal1" class="modal blue lighten-4">
        <div class="modal-content">
            <h2>Facturas faltantes:</h2>
        <table v-show="vacio != 'vacio'" style="width:100%">
            <thead>
                <th style="background-color:#fff">Tipo</th>
                <th style="background-color:#fff">Factura</th>
            </thead>
            <tbody>
                <tr v-for="f in faltantes">
                    <template v-if="f.type === 'REM'">
                        <td style="background-color:#778899">@{{f.type}}</td>
                        <td  style="background-color:#778899">@{{f.invoiceNumber}}</td>
                    </template>
                    <template v-else-if="f.type === 'NV'">
                        <td style="background-color:#45e3c3">@{{f.type}}</td>
                        <td  style="background-color:#45e3c3">@{{f.invoiceNumber}}</td>
                    </template>
                    <template v-else-if="f.type === 'FCON'">
                        <td style="background-color:#98FB98">@{{f.type}}</td>
                        <td  style="background-color:#98FB98">@{{f.invoiceNumber}}</td>
                    </template>
                    <template v-else-if="f.type === 'FCR'">
                        <td style="background-color:#b39ddb">@{{f.type}}</td>
                        <td  style="background-color:#b39ddb">@{{f.invoiceNumber}}</td>
                    </template>
                </tr>
            </tbody>
        </table>
        <p v-if="vacio == 'vacio'">No se encontraron resultados.</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="green accent-2 modal-action modal-close waves-effect waves-green btn-flat">Aceptar</a>
            <a class="btn red col s3 offset-s1 red darken-4" target="_blank" :href=" '/invoice/printMissing/' + fechaInicial + '/' + fechaFinal + '/' + checkBoxes" style="margin-top: 3px"><i class = 'material-icons'>print</i></a>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">

$(document).ready(function() {
    $('#tabla').dataTable( {
        "paging":   false,
        "order": [[3,'asc']],
        "info":     true,
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "InfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
} ).on( 'draw.dt', function () {
    $('.modal').modal();
} );
} );

var app = new Vue(
    {
        el: '#vue-app',
        data: {
            fechaFinal: '',
            fechaInicial: '',
            checkBoxes: ['REM', 'NV', 'FCON', 'FCR'],
            faltantes: [],
            vacio: '',
        },
        methods: {
            verificar: function (event) {
                let t = this;
                let url = '{!! url("invoice")!!}' + '/missing/';
                $(document).ready(function(){
                    var input = $('#fechaInicial').pickadate();
                    var picker = input.pickadate('picker');
                    t.fechaInicial = picker.get();
                    var inputF = $('#fechaFinal').pickadate();
                    var pickerF = inputF.pickadate('picker');
                    t.fechaFinal = pickerF.get();
                    if(t.fechaInicial != '' && t.fechaFinal != '' && t.checkBoxes.length > 0)
                    {
                        axios.get(url + t.fechaInicial + '/' + t.fechaFinal + "/" + t.checkBoxes).then(function (response) {
                            t.faltantes = [];
                            if(response.data.length > 0)
                            {
                                for (var i = 0; i < response.data.length; i++) {
                                    var reg = {
                                        type: response.data[i].type,
                                        invoiceNumber: response.data[i].invoiceNumber,
                                    }
                                    t.faltantes.push(reg);
                                }
                            }
                            else
                                t.vacio = 'vacio';
                            $(document).ready(function(){
                                $('#modal1').modal('open');
                            });
                            }).catch(function (error) {
                                console.log(error);
                        });
                    }
                    else
                        Materialize.toast("Verificar fechas y tipo(s) de factura.", 3000);
                });
            },
        },
    }
);

</script>

@endsection
