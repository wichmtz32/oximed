@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show invoice
    </h1>
    <form method = 'get' action = '{!!url("invoice")!!}'>
        <button class = 'btn blue'>invoice Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>invoiceNumber : </i></b>
                </td>
                <td>{!!$invoice->invoiceNumber!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>type : </i></b>
                </td>
                <td>{!!$invoice->type!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection