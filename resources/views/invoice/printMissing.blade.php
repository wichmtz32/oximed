<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <title>Facturas Faltantes</title>
    <style media="screen" type="text/css" >
    body{
      font-family:sans-serif !important;
    }
    table tr, td{
      padding-top: 10px !important;
      padding-left: 0px !important;
      padding-right: 0px !important;
      font-size: 10px;
      border-bottom: .5px solid gray !important;
      text-align: center !important;
    }

    .page-break { page-break-inside: always; }

    </style>
  </head>
  <body>
    <div>
      <div style="text-align: center">
        <span style="font-weight: bold; font-size: 14px">
            S O L D E R<br>
            FACTURAS FALTANTES <br><span style="font-size: 12px">
              CANTIDAD: {{count($faltantes)}}<br>
              FECHA: {{Carbon\Carbon::parse($today->format('Y-m-d'))->format('d/m/Y')}}<br>
            </span>
            <br><br>
        </span>
      </div>
      <div>
        <span style="font-weight: bold; font-size: 10px">
        <span style="font-weight: bold; font-size: 12px">Datos de la busqueda.</span><br>
              Fecha Inicial: {{$fechaInicial}} <br>
              Fecha Final: {{$fechaFinal}}<br>
              Tipo(s) de factura: {{$tipos}}<br>
            <br><br>
        </span>
      </div>
      <div class="page-break">
        <table  style="width:100%" >
          <thead style="border-bottom: 2px solid black;">
            <tr>
              <th>TIPO</th>
              <th>FACTURA</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($faltantes as $c)
                <tr>
                  <td>{{$c['type']}}</td>
                  <td>{{$c['invoiceNumber']}}</td>
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </body>
</html>
