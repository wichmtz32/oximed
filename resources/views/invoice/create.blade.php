@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<style>
    .button-font-size {
        font-size: 20px;
    }
</style>

<div id="vue-app">
    <h1>
        Revisar Factura
    </h1>
    <div class="col m12" style="margin-bottom: 200px;">
        <div class="col m3">
            <a class="col m12 waves-effect btn-large blue-grey lighten-2 button-font-size" v-on:click="checa('REM')" :disabled="disabledREM">REM</a>
        </div>
        <div class="col m3">
            <a class="col m12 waves-effect btn-large blue lighten-2 button-font-size" v-on:click="checa('NV')" :disabled="disabledNV">NV</a>
        </div>
        <div class="col m3">
            <a class="col m12 waves-effect waves-light btn-large light-green lighten-2 button-font-size" v-on:click="checa('FCON')" :disabled="disabledFCON">FCON</a>
        </div>
        <div class="col m3">
            <a class="col m12 waves-effect waves-light btn-large deep-purple lighten-2 button-font-size" v-on:click="checa('FCR')" :disabled="disabledFCR">FCR</a>
        </div>
    </div>
    <div class="input-field col m6">
        <input id="invoiceNumber" name = "invoiceNumber" type="number" class="validate" v-model="busqueda" :disabled="disabledFactura">
        <label for="invoiceNumber">Factura:</label>
    </div>
    <div class="input-field col m3">
        <a class="col m12 waves-effect waves-light btn-large green button-font-size modal-trigger" v-on:click="verificar">VERIFICAR</a>
    </div>
    <div id="modal1" class="modal blue lighten-4">
        <div class="modal-content">
            <h1>@{{tipo}}</h1>
            <p v-if="existe == 'SI'"> <span class="green-text">Factura @{{busqueda}} existente.</span> </p>
            <p v-if="existe == 'NO'"> <span class="red-text">Factura @{{busqueda}} no registrada. Desea Registrar</span></p>
            <p v-if="existe == 'VACIA'"> <span class="red-text">Verificar campo factura</span> </p>
            <p v-if="aceptar == 'SI'">Seleccionar Usuario:</p>
            <div v-show="aceptar == 'SI'" class="input-field">
                <select v-model="usuario" id="usuario" onchange="ShowSelected();" >
                    <option value="null">Seleccionar...</option>
                    @foreach ($persons as $p)
                        <option value="{{$p->name}}">{{$p->name}}</option>
                    @endforeach
                </select>
                <label>Usuario</label>
            </div>
        </div>
        <div class="modal-footer">
            <a v-on:click="acepta" href="#!" v-if="aceptar == 'SI'" class="green accent-2 modal-action waves-effect waves-green btn-flat">Aceptar</a>
            <a v-on:click="reinicia" v-if="existe != 'NO'" href="#!" class="blue lighten-4 modal-action modal-close waves-effect waves-green btn-flat">Aceptar</a>
            <a v-on:click="registra" v-if="existe == 'NO' && aceptar == 'NO'" href="#!" class="green accent-2 modal-action waves-effect waves-green btn-flat">Registrar</a>
            <a v-on:click="reinicia" v-if="existe == 'NO' || aceptar == 'SI'" href="#!" class="blue lighten-4 modal-action modal-close waves-effect waves-green btn-flat">En otro momento</a>
        </div>
    </div>

</div>
@endsection

@section('scripts')

<script type="text/javascript">

var app = new Vue(
    {
        el: '#vue-app',
        data: {
            busqueda: '',
            existe: 'NO',
            tipo: '',
            usuario: 'null',
            aceptar: 'NO',
            FCR: false,
            REM: false,
            NV: false,
            FCON: false,
        },
        computed: {
            disabledREM() {
                return this.REM;
            },
            disabledNV() {
                return this.NV;
            },
            disabledFCON() {
                return this.FCON;
            },
            disabledFCR() {
                return this.FCR;
            },
            disabledFactura() {
                if(this.REM === false && this.NV === false && this.FCON === false && this.FCR === false)
                    return true;
                else
                    return false;
            }
        },
        methods: {
            checa: function (val) {
                let t = this;
                t.tipo = val;
                switch (val) {
                    case 'REM':
                        t.NV = true;
                        t.FCON = true;
                        t.FCR = true;
                    break;
                    case 'NV':
                        t.REM = true;
                        t.FCON = true;
                        t.FCR = true;
                    break;
                    case 'FCON':
                        t.REM = true;
                        t.NV = true;
                        t.FCR = true;
                    break;
                    case 'FCR':
                        t.REM = true;
                        t.NV = true;
                        t.FCON = true;
                    break;
                }
            },
            verificar: function (val) {
                let t = this;
                t.aceptar = 'NO';
                let url = '{!! url("invoice")!!}' + '/search/';
                $(document).ready(function(){
                    $('.modal-trigger').leanModal({
                        dimissible: false
                    }); 
                });
                if(t.busqueda == ""){
                    t.existe = 'VACIA';
                    t.abrirModal();
                }
                else
                {
                    axios.get(url + t.busqueda + '/' + t.tipo).then(function (response) {
                        if(response.data.length > 0)
                        {
                            t.existe = 'SI';
                            t.abrirModal();
                        }
                        else
                        {
                            t.existe = 'NO';
                            t.abrirModal();
                        }
                        }).catch(function (error) {
                            console.log(error);
                    });
                }
            },
            registra: function (val) {
                let t = this;
                t.aceptar = 'SI';
            },
            acepta: function (val) {
                let t = this;
                let url = '{!! url("invoice")!!}' + '/store2/';
                if(t.usuario == 'null')
                {
                    Materialize.toast("Error, seleccionar usuario", 3000);
                }
                else
                {
                    axios.post(url + t.busqueda + '/' + t.tipo + '/' + t.usuario).then(function (response) {
                            Materialize.toast("Factura Registrada Correctamente", 3000);
                            t.busqueda = '';
                            t.aceptar = 'NO';
                            t.existe = 'VACIA';
                            $(document).ready(function(){
                                $('#modal1').modal('close');
                            });
                            t.reinicia();
                        }).catch(function (error) {
                            Materialize.toast("Error: Verificar Factura", 3000);
                            $(document).ready(function(){
                                $('#modal1').modal('close');
                            });
                            t.reinicia();
                    });
                }
            },
            abrirModal: function() {
                $(document).ready(function(){
                    $('.modal').modal({
                        dismissible: false, // Modal can be dismissed by clicking outside of the modal
                    });
                    $('#modal1').modal('open');
                });
            },
            reinicia: function () {
                let t = this;
                t.REM = false;
                t.NV = false;
                t.FCON = false;
                t.FCR = false;
            },
            user: function(val) {
                let t = this;
                t.usuario = val;
            }
        },
    }
);

function ShowSelected()
{
    /* Para obtener el valor */
    var cod = document.getElementById("usuario").value;
    app.user(cod);
}

</script>

@endsection