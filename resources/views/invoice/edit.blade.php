@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit invoice
    </h1>
    <form method = 'get' action = '{!!url("invoice")!!}'>
        <button class = 'btn blue'>invoice Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("invoice")!!}/{!!$invoice->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="invoiceNumber" name = "invoiceNumber" type="text" class="validate" value="{!!$invoice->
            invoiceNumber!!}"> 
            <label for="invoiceNumber">invoiceNumber</label>
        </div>
        <div class="input-field col s6">
            <input id="type" name = "type" type="text" class="validate" value="{!!$invoice->
                type!!}"> 
            <label for="type">type</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection