@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')
  <style media="screen">
    body{
      font-size: 13px;
    }
    #tabla tr td, th{
      margin: 0px !important;
      padding: 3px !important;
    }
  </style>


<div>
  <div class="col m10">
    <h4 style="padding: 0px !important; margin: 0px !important;">Conductores</h4><br>
  </div>
  <div class="col m2">
    <form method = 'get' action = '{!!url("driver")!!}/create'>
      @can('a_conductor')
      <button class = 'btn' type = 'submit'>NUEVO</button>
      @endcan
    </form>
  </div>
    <table class="striped" id="tabla">
        <thead>
            <th>Nombre</th>
            <th>Telefono</th>
            <th>Celular/Telefono 2</th>
            <th></th>
        </thead>
        <tbody>
            @foreach($drivers as $driver)
            <tr>
                <td>{!!$driver->name!!}</td>
                <td>{!!$driver->phone!!}</td>
                <td>{!!$driver->cellphone!!}</td>
                <td>
                    <div class = 'row'>
                      @can('m_conductor')
                      <a href='#' class = 'viewEdit btn-floating blue' data-link = '/driver/{!!$driver->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                      @endcan
                      @can('b_conductor')
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/driver/{!!$driver->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                      @endcan
                      <a href='#' class = 'viewShow btn-floating orange' data-link = '/driver/{!!$driver->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $drivers->render() !!}

</div>
@endsection
