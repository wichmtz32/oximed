@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div>
    <h2>Informacion conductor</h2>
    <table class = 'highlight bordered'>
        <thead>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>Nombre: </i></b>
                </td>
                <td>{!!$driver->name!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Telefono: </i></b>
                </td>
                <td>{!!$driver->phone!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Celuar/Telefono2: </i></b>
                </td>
                <td>{!!$driver->cellphone!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Direccion: </i></b>
                </td>
                <td>{!!$driver->address!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Horario: </i></b>
                </td>
                <td>{!!$driver->schedule!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Salario: </i></b>
                </td>
                <td>${{number_format($driver->salary, 2)}}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection
