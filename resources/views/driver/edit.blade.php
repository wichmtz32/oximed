@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div>
    <h2>Editar conductor</h2>
    <br>
    <form method = 'POST' action = '{!! url("driver")!!}/{!!$driver->id!!}/update'>
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s5">
            <input id="name" name = "name" type="text" class="validate" value="{!!$driver->name!!}" required>
            <label for="name">Nombre completo</label>
        </div>
        <div class="input-field col s6">
            <input id="address" name = "address" type="text" class="validate" value="{!!$driver->address!!}">
            <label for="address">Direccion</label>
        </div>
        <div class="input-field col s2">
            <input id="phone" name = "phone" type="text" class="validate" value="{!!$driver->phone!!}">
            <label for="phone">Telefono</label>
        </div>
        <div class="input-field col s2">
            <input id="cellphone" name = "cellphone" type="text"  class="validate" value="{!!$driver->cellphone!!}">
            <label for="cellphone">Celular/Telefono2</label>
        </div>
        <div class="input-field col s4">
            <input id="schedule" name = "schedule" type="text" class="validate" value="{!!$driver->schedule!!}">
            <label for="schedule">Horario</label>
        </div>
        <div class="input-field col s2">
            <input id="salary" name = "salary" type="number" step="any" class="validate" value="{!!$driver->salary!!}">
            <label for="salary">Salario</label>
        </div>
        <div style="clear:both">
  <br><br><br>
        </div>
        <button class = 'btn red' type ='submit'>Guardar</button>
    </form>
</div>
@endsection
