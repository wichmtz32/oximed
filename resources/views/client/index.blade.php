@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<style media="screen">
  body{
    font-size: 13px;
  }
  #tabla tr td, th{
    margin: 0px !important;
    padding: 1px !important;
  }
</style>

<div id="vue-app">
    <div class="col m10">
      <div class="col s4">
        <h4 style="padding: 0px !important; margin: 0px !important;">Clientes</h4>
      </div>
      <div class="input-field col s4 offset-s4">
        <input id="busqueda" placeholder="Busqueda ..." type="text" v-model="busqueda" autocomplete="off">
      </div>
    </div>
    <div class="col m2" style="text-align: center;">
      <form  method = 'get' action = '{!!url("client")!!}/create'><br>
        @can('a_cliente')
        <button class = 'btn' type = 'submit'>NUEVO</button>
        @endcan
      </form>
    </div>
    <table class="striped" id="tabla">
        <thead>
            <th style="width: 20%">Nombre</th>
            <th style="width: 25%">Direccion</th>
            <th style="width: 10%">Telefono</th>
            <th style="width: 10%">Celular</th>
            <th style="width: 15%"></th>
        </thead>
        <tbody>
            <tr v-for="c in clients">
                <td>@{{c.name}}</td>
                <td>@{{c.address}}</td>
                <td>@{{c.phone}}</td>
                <td>@{{c.cellphone}}</td>
                <td>
                    <div class = 'row'>
                        @can('m_cliente')
                        <a :href = 'c.edit' class = 'viewEdit btn-floating blue'><i class = 'material-icons'>edit</i></a>
                        @endcan
                        @can('b_cliente')
                          <a href = '#modal1' class = 'delete btn-floating modal-trigger red' :data-link = "c.delete" ><i class = 'material-icons'>delete</i></a>
                        @endcan
                        <a :href = 'c.info' class = 'viewShow btn-floating orange'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</div>
@endsection

@section('scripts')
  <script type="text/javascript">
  var app = new Vue({
    el: '#vue-app',
    data: {
      clients: [],
      busqueda: '',
    },
    watch: {
      busqueda: function (val) {
        let t = this;
        let url = '{!! url("client")!!}' + '/search/';
        t.clients = [];
        if(val == ""){
          t.getClientsAjax();
        }else{
          axios.get(url + val).then(function (response) {
            t.clients = [];
            for (var i = 0; i < response.data.length; i++) {
              var reg = {
                id: response.data[i].id,
                name: response.data[i].name,
                address: '',
                phone:(response.data[i].phone != null ? response.data[i].phone : ''),
                cellphone: (response.data[i].cellphone != null ? response.data[i].cellphone : ''),
                delete: "/client/" + response.data[i].id + "/deleteMsg",
                edit:"/client/" + response.data[i].id + "/edit",
                info:"/client/" + response.data[i].id,
              };
              if(response.data[i].address != null){
                reg.address += response.data[i].address + ' ';
              }
              if(response.data[i].suburb != null){
                reg.address += response.data[i].suburb + ' ';
              }
              if(response.data[i].cp != null){
                reg.address += response.data[i].cp + ' ';
              }
              t.clients.push(reg);
            }
            console.log(t.clients);
            t.updateHTMLElements();
          }).catch(function (error) {
            console.log(error);
          });
        }
      }
    },
    methods:{
      getClientsAjax: function(){
        let t = this;
        let url = '{!! url("client")!!}/ajax';
        t.clients = [];
        axios.get(url)
        .then(function (response) {
          t.clients = [];
          console.log(response.data);
          $(document).ready(function(){
            for (var i = 0; i < response.data.length; i++) {
              var reg = {
                id: response.data[i].id,
                name: response.data[i].name,
                address: '',
                phone:(response.data[i].phone != null ? response.data[i].phone : ''),
                cellphone: (response.data[i].cellphone != null ? response.data[i].cellphone : ''),
                delete: "/client/" + response.data[i].id + "/deleteMsg",
                edit:"/client/" + response.data[i].id + "/edit",
                info:"/client/" + response.data[i].id,
              }
              if(response.data[i].address != null){
                reg.address += response.data[i].address + ' ';
              }
              if(response.data[i].suburb != null){
                reg.address += response.data[i].suburb + ' ';
              }
              if(response.data[i].cp != null){
                reg.address += response.data[i].cp + ' ';
              }
              t.clients.push(reg);

            }

          });
        })
        .catch(function (error) {
            console.log(error);
        });
      }
    },
    mounted: function () {
      this.$nextTick(function () {

        let t = this;
        t.getClientsAjax();
      });
    }
  });

  </script>

@endsection
