@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div>
  <div class="col m12">
    <h2>Editar cliente</h2>
  </div>
  <br>
    <br>
    <form method = 'POST' action = '{!! url("client")!!}/{!!$client->id!!}/update'>
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s4">
            <input id="name" name = "name" type="text" class="validate" value="{!!$client->name!!}" required>
            <label for="name">Nombre</label>
        </div>
        <div class="input-field col s4">
            <input id="address" name = "address" type="text" class="validate" value="{!!$client->address!!}">
            <label for="address">Direccion</label>
        </div>
        <div class="input-field col s2">
            <input id="suburb" name = "suburb" type="text" class="validate" value="{!!$client->suburb!!}">
            <label for="suburb">Colonia</label>
        </div>
        <div class="input-field col s1">
            <input id="cp" name = "cp" type="text" class="validate" value="{!!$client->cp!!}">
            <label for="cp">CP</label>
        </div>
        <div class="input-field col s4">
          <input id="email" name = "email" type="text" class="validate" value="{!!$client->email!!}">
          <label for="email">Correo electronico</label>
        </div>
        <div class="input-field col s2">
            <input id="phone" name = "phone" type="text" class="validate" value="{!!$client->phone!!}">
            <label for="phone">Telefono</label>
        </div>
        <div class="input-field col s2">
            <input id="cellphone" name = "cellphone" type="text" class="validate" value="{!!$client->cellphone!!}">
            <label for="cellphone">Celular/Telefono 2</label>
        </div>
        <div class="input-field col s3">
          <input id="rfc" name = "rfc" type="text" class="validate" value="{!!$client->rfc!!}">
          <label for="rfc">RFC</label>
        </div>
        <div class="input-field col s3">
          <select id="person" name="person" type="text" class="validate" >
            <option value="fisica" {{$client->person == 'fisica' ? 'selected' : ''}}>Fisica</option>
            <option value="moral" {{$client->person == 'moral' ? 'selected' : ''}}>Moral</option>
          </select>
          <label>Tipo persona</label>
        </div>
        <div class="col m12">
          <br><br><br>
          <button class = 'btn red' type ='submit'>Guardar</button>
        </div>
    </form>
</div>
@endsection
