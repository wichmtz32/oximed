<div id="load" style="position: relative;">
@foreach($clients as $client)
  <tr>
      <td>{!!$client->name!!}</td>
      <td>{!!$client->address!!} {!!$client->suburb!!} @if( $client->cp ) CP. {{$client->cp}} @endif</td>
      <td>{!!$client->phone!!}</td>
      <td>{!!$client->cellphone!!}</td>
      <td>
          <div class = 'row'>
              @can('m_cliente')
              <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/client/{!!$client->id!!}/edit'><i class = 'material-icons'>edit</i></a>
              @endcan
              @can('b_cliente')
                <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/client/{!!$client->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
              @endcan
              <a href = '#' class = 'viewShow btn-floating orange' data-link = '/client/{!!$client->id!!}'><i class = 'material-icons'>info</i></a>
          </div>
      </td>
  </tr>
@endforeach
</div>
{{ $clients->links() }}
