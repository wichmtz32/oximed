@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div>
    <div class="col m12">
      <h2>Nuevo cliente</h2>
    </div>
    <br>
    <form method = 'POST' action = '{!!url("client")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s4">
            <input id="name" name = "name" type="text" class="validate" required>
            <label for="name">Nombre</label>
        </div>
        <div class="input-field col s4">
            <input id="address" name = "address" type="text" class="validate">
            <label for="address">Calle y numero</label>
        </div>
        <div class="input-field col s2">
            <input id="suburb" name = "suburb" type="text" class="validate">
            <label for="suburb">Colonia</label>
        </div>
        <div class="input-field col s1">
            <input id="cp" name = "cp" type="text" class="validate">
            <label for="cp">CP</label>
        </div>
        <div class="input-field col s4">
          <input id="email" name = "email" type="text" class="validate">
          <label for="email">Correo electronico</label>
        </div>
        <div class="input-field col s2">
            <input id="phone" name = "phone" type="text" class="validate">
            <label for="phone">Telefono</label>
        </div>
        <div class="input-field col s2">
            <input id="cellphone" name = "cellphone" type="text" class="validate">
            <label for="cellphone">Celular</label>
        </div>
        <div class="input-field col s3">
          <input id="rfc" name = "rfc" type="text" class="validate">
          <label for="rfc">RFC</label>
        </div>
        <div class="input-field col s3">
          <select id="person" name="person" type="text" class="validate">
            <option value="fisica">Fisica</option>
            <option value="moral">Moral</option>
          </select>
          <label>Tipo persona</label>
        </div>
        <div class="col m12">
          <br><br><br>
          <button class = 'btn red' type ='submit'>Guardar</button>
        </div>
    </form>
</div>
@endsection
