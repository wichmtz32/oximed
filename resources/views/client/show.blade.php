@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class="col m12">
    <h2>
        Informacion cliente
    </h2>
    <table class = 'highlight bordered'>
        <thead>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>Nombre: </i></b>
                </td>
                <td>{!!$client->name!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Direccion: </i></b>
                </td>
                <td>{!!$client->address!!} {!!$client->suburb!!} CP. {!!$client->cp!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Correo electronico: </i></b>
                </td>
                <td>{!!$client->email!!}</td>
            </tr>
            <tr>
              <td>
                <b><i>Telefono: </i></b>
              </td>
              <td>{!!$client->phone!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Celular/Telefono 2: </i></b>
                </td>
                <td>{!!$client->cellphone!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Tipo persona: </i></b>
                </td>
                <td>{!!$client->person!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection
