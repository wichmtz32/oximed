@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div id="vue-app">

  @if($nueva)
  <div class="col m10">
      <h3>Nueva reparación</h3>
  </div>
  <div class="col m2"><br>
    <p style="font-size: 20px; font-weight: bold">No. NA</p>
  </div>
  @elseif($show)
    <div class="col m10">
        <h3>Información contrato</h3>

    </div>

  @else
    <div class="col m10">
        <h3>Editar Reparacion</h3>

    </div>

  @endif
    <br>
    @if($nueva)
    <form method = 'POST' action = '{!!url("repair")!!}'>
    @elseif($show)

    @else
    <form method = 'POST' action = '{!! url("repair")!!}/{!!$repair->id!!}/update'>
    @endif
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="col s12">
          <div class="input-field col s6">
            @if($nueva)
            <input id="date" name = "date" type="text" class="datepicker">
            @else
            <input id="date" name = "date" type="text" class="datepicker" disabled>
            @endif
            <label for="date">Fecha</label>
          </div>

        </div>
        <h5 class="col s10">Datos del cliente</h5>

        <div class="col s12">
          <div class="input-field col s4">
              <input id="client" name = "client" type="text" class="validate cliente" v-model="cliente" required>
              <label for="client">Cliente</label>
          </div>
          <div class="input-field col s4">
              <input id="address" name = "address" type="text" class="validate" v-model="direccion" required>
              <label for="address">Direccion</label>
          </div>
          <div class="input-field col s2">
              <input id="phone" name = "phone" type="text" class="validate" v-model="telefono" required>
              <label for="phone">Telefono</label>
          </div>
          <div class="input-field col s2">
              <input id="cellphone" name = "cellphone" type="text" class="validate" v-model="telefono2" required>
              <label for="cellphone">Celular/Telefono2</label>
          </div>

        </div>
        <h5 class="col s10">Datos Adquiridos</h5>

        <div class="col s12">
          <div class="input-field col s6">
              <input  id="product" name = "product" v-model="equipo" type="text"  required>
              <label >Equipo</label>
          </div>
          <div class="input-field col s6">
              <input  id="nserie" name = "nserie" v-model="nserie" type="text"  required>
              <label >Serie</label>
          </div>

        </div>

        <div class="col s12">
          <div class="input-field col s6">
              <textarea id="faults" name = "faults" v-model="fallas"  rows="4" cols="50">

              </textarea>

              <label>Fallas que presenta</label>
          </div>
          <div class="input-field col s6">
              <input  id="reparador" name = "reparador" v-model="reparador" type="text"  required>
              <label >Reparador</label>
          </div>
        </div>

        <div style="clear:both"><br></div>
        <div>
          <h5 class="col s10">Reparacion Efectuada</h5>
          <div class="col s2 center" >
            <button type="button" class="btn btn-small green  flat" @click="agregaReparacion()">+Reparacion</button>
          </div>
          <div class="col s12" v-for="(r, index) in reparaciones">
            <div class="input-field col s4">
                <input   v-model="r.descripcion" name="descripcion[]" id="descripcion"  type="text"  required>
                <label >Descripcion</label>
            </div>
            <div class="col s2">
              <label >Estatus</label>
                <select v-model="r.estatus"  class="browser-default"  id="estatus" style="height: 40px">
                 <option value="En espera" selected>En Espera</option>
                 <option value="Aceptada">Aceptada</option>
                 <option value="Cancelada">Cancelada</option>
               </select>
               <input   type='hidden' name="estatus[]"   :value="r.estatus">


            </div>
            <div class="input-field col s1">
              <input v-model="r.cantidad" name="cantidad[]" id="cantidad" type="number" step="any" class="validate" @input="actualizaTotal2(index)" required>
              <label for="cost">Cantidad</label>
            </div>

            <div class="input-field col s2">
              <input name = "precio[]" v-model="r.precio"  id="precio" type="number" step="any" class="validate"  @input="actualizaTotal2(index)" required>
              <label>Precio</label>
            </div>

            <div class="input-field col s2">
              <input name = "total[]"  id="total"  v-model="r.total"  type="number" step="any" class="validate"  disabled>
              <label>Total</label>

            </div>
            <input type="hidden" name="totalp[]" :value="r.total">



            <div class="col s1 center">
              <button type="button" class="btn btn-small red "><i class="material-icons" @click="eliminaReparacion(index)">clear</i></button>
            </div>
          </div>
          @verbatim
          <div v-if="reparaciones.length>0" class=" col s12 "  align="right">
            <h6 align="right" class="col s9">SUBTOTAL</h5>
            <h6 align="left" class="col s3">${{subtotal1 }}</h5>

          </div>
          <div v-if="reparaciones.length>0" class=" col s12 "  align="right">

            <h6 align="right" class="col s9">IVA 15%</h5>
            <h6 align="left" class="col s3">${{iva}}</h5>

          </div>
          <div v-if="reparaciones.length>0" class=" col s12 "  align="right">

            <h6 align="right" class="col s9">TOTAL</h5>
           <h6 align="left" class="col s3">${{total1}}</h5>
          @endverbatim
         </div>
          <div class=" col s12">
            <div class="input-field col s4">
              <input id="fecha2" v-model="fecha2" name = "fecha2" type="text" class="datepicker">
              <label for="fecha2">Fecha Entrega</label>
            </div>


          </div>
        </div>
        <div style="clear:both"><br></div>

        <div>
          <h5 class="col s10">Observaciones</h5>
          <div class="col s2 center" >
              <a class="btn btn-small green flat btn modal-trigger" href="#modal1">+Observacion</a>
          </div>
          <div class="col s12" v-for="(o, index) in observaciones">
            <div class="input-field col s8">
                <input  v-model="o.descripcion" name="descripcionO[]" id="descripcionO"  type="text" disabled>
                <input   type="hidden"  v-model="o.descripcion" name="descripcion1[]" id="descripcion1"  type="text" >
            </div>
            <div class="input-field col s2">

              <input :id='index' name = "fechas3[]" v-model="o.fecha"  type="text" class="datepicker" disabled>

              <input :id='index'  type="hidden"   name = "fechas4[]" v-model="o.fecha"    >

            </div>
            <div class="col s1 center">
              <button type="button" class="btn btn-small blue "><i class="material-icons" @click="editaObservacion(index)">edit</i></button>

            </div>
            <div class="col s1 center">
              <button  type="button" class="btn btn-small red "><i class="material-icons" @click="eliminaObservacion(index)">clear</i></button>

            </div>
          </div>
        </div>
        <input type="hidden" name="total" :value="total1">
        <input type="hidden" name="subtotal" :value="subtotal1">
        <input type="hidden" name="iva" :value="iva">
        <div  align="center" class="col m12">
          <br><br><br>
          @if($show)
          @else
            <button type="submit" name="button" class="btn red">Guardar</button>
          @endif
        </div>
    </form>

<!-- Modal Observacion -->
<div id="modal1" class="modal bottom-sheet">
  <div class="modal-content">
    <h4>Agregar Observacion</h4>
    <div>
      <h5 class="col s10"></h5>


        <div class="input-field col s8">
            <input  v-model="descripcion1" name="descripcionO[]" id="descripcionO"  type="text" required>
            <label >Descripcion</label>
        </div>
        <div class="input-field col s2">

          <input id='fecha8' name = "fecha8" v-model="fecha8"  type="text" class="datepicker" required >
          <label for="fecha8">Fecha</label>

        </div>

  </div>
  <div class="modal-footer">
    <a href="#!"class="btn btn-small green  flat" @click="agregaObservacion2()">Aceptar</a>
    <a href="#!"class="btn btn-small red  flat" @click="cierraModal()">Cancelar</a>


  </div>
</div>

</div>

<div id="modal2" class="modal bottom-sheet">
  <div class="modal-content">
    <h4>Agregar Observacion</h4>
    <div>
      <h5 class="col s10"></h5>


        <div class="input-field col s8">
            <input  v-model="descripcion1" name="descripcionO[]" id="descripcionO"  type="text" required>
            <label >Descripcion</label>
        </div>
        <div class="input-field col s2">

          <input id='fecha9' name = "fecha9" v-model="fecha9"  type="text" class="datepicker" required >
          <label for="fecha9">Fecha</label>



        </div>

  </div>
  <div class="modal-footer">
    <a href="#!"class="btn btn-small green  flat" @click="editaObservacion2()">Guardar</a>
    <a href="#!"class="btn btn-small red  flat" @click="cierraModal()">Cancelar</a>


  </div>
</div>

</div>


</div>



@endsection

@section('scripts')
  <script type="text/javascript">

  var app = new Vue({
    el: '#vue-app',
    data: {
      descripcion1: "",
      cliente: '',
      direccion: '',
      telefono: '',
      telefono2: '',
      fecha: '',
      fecha9: '',
      fecha8: '',
      fechaVigencia: '',
      fechaRecoleccion: '',
      observaciones: [],
      equipo:'',
      fallas:'',
      fechas2: [],
      fecha2:'',
      estatus:[],
      nserie:'',
      clientes: [],
      total: 0,
      productos: [],
      reparador:'',
      reparaciones: [],
      band_nuevo: true,
      activa:-1,

    },
    mounted: function () {
      this.$nextTick(function () {
        let t = this;
        var date = new Date();


        var input = $('#date').pickadate();
        var picker = input.pickadate('picker');
        picker.set('select', date.toString(), { format: 'yyyy-mm-dd' });
        $('select').material_select();
        $('#vueSelect').on('change', function () {
            t.$emit("change", this.value)
        });
        $('#vueSelect2').on('change', function () {
            t.$emit("change1", this.value)
        });
        @foreach($clients as $client)
          this.clientes.push({
            nombre: '{{$client->name}}',
            direccion: '{{$client->address}}' + " " + '{!!$client->suburb!!}',
            telefono: '{{$client->phone}}',
            telefono2: '{{$client->cellphone}}',
          });
        @endforeach

        @if(!$nueva)
          this.band_nuevo = false;
        @endif
        $(document).ready(function(){

          var clientes = [];
          @foreach($clients as $client)
            var reg = {
              nombre: '{{$client->name}}'
            };
            clientes.push(reg);
          @endforeach

          var cad = "{";
          for(var i=0; i < clientes.length; i++)
            cad += "\"" + (clientes[i].nombre).toString() + "\": null, ";
          cad = cad.substring(0, cad.length-2) + "}";

          if(clientes.length > 0)
          {
            $('input.cliente').autocomplete({
               data : JSON.parse(cad),
               onAutocomplete: function(txt) {
                 for (var i = 0; i < t.clientes.length; i++) {
                   if(txt == t.clientes[i].nombre){
                     t.direccion = '';
                     t.telefono = '';
                     t.telefono2 = '';
                     t.cliente ="";
                     t.cliente = txt;
                     t.direccion = t.clientes[i].direccion;
                     t.telefono = t.clientes[i].telefono;
                     t.telefono2 = t.clientes[i].telefono2;
                     t.updateHTMLElements();
                   }
                 }
              },
               limit: 20,
               minLength: 1,
             });
           }
        });
        @if($nueva)
          var date = new Date();
          date.setMonth(date.getMonth());
          var date1 = date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + "-" +  ('0' + date.getDate()).slice(-2);
          picker.set('select', date1 , { format: 'yyyy-mm-dd' });
        @else
          t.cliente = '{{$repair->client}}';
          t.direccion = '{{$repair->address}}';
          t.telefono = '{{$repair->phone}}';
          t.telefono2 = '{{$repair->cellphone}}';
          t.fecha = '{{$repair->date}}';
          t.fecha2 = '{{$repair->deliverDate}}';
          t.equipo='{{$repair->product}}';
          t.nserie='{{$repair->serialNumber}}';
          t.fallas='{{$repair->faults}}';
          t.reparador='{{$repair->repairman}}';
          t.productos = [];
          t.observaciones=[];
          $(document).ready(function(){
            var input = $('#fecha2').pickadate();
            var picker = input.pickadate('picker');
            picker.set('select', '{{$repair->deliverDate}}', { format: 'yyyy-mm-dd' });
            t.fecha2 =  $('#fecha2').val();
          });
          this.updateHTMLElements();
          var $input = $('#date').pickadate();
          var picker = $input.pickadate('picker');
          picker.set('select', t.fecha.toString(), { format: 'yyyy-mm-dd' });
          @foreach ($repair->detalles as $r)
            t.reparaciones.push({
              id: '{{$r->id}}',
              descripcion: '{{$r->description}}',
              cantidad: '{{$r->quantity}}',
              estatus: '{{$r->status}}',
              precio: '{{$r->unitPrice}}',
              total:{{$r->total}},
            });
          @endforeach
          @foreach ($repair->observaciones as $index=>$o)
          t.observaciones.push({
            id: {{$o->id}},
            descripcion: '{{$o->description}}',
            fecha:  '{{$o->fecha}}',
          });
            $(document).ready(function(){
              var $input = $('#{{$index}}').pickadate();
              var picker = $input.pickadate('picker');
              picker.set('select','{{$o->fecha}}'.slice(0, 10).toString(), { format: 'yyyy-mm-dd' });
             t.observaciones[{{$index}}].fecha =  $('#{{$index}}').val();

            });
          @endforeach
        @endif
      });
    },
    watch: {
      cliente: function (newVal) {
        @if($nueva)
        for (var i = 0; i < this.clientes.length; i++) {
          if(newVal == this.clientes[i].nombre){
            this.direccion = '';
            this.telefono = '';
            this.telefono2 = '';
            this.direccion = this.clientes[i].direccion;
            this.telefono = this.clientes[i].telefono;
            this.telefono2 = this.clientes[i].telefono2;
            this.updateHTMLElements();
          }
        }
        @endif
      },
    },
    computed: {
      total1: function () {
        this.total = 0;
          for (var i = 0; i < this.reparaciones.length; i++) {
            this.total +=this.reparaciones[i].total;
          }
          return this.total;
      },
      subtotal1: function () {
          this.total = 0;
            for (var i = 0; i < this.reparaciones.length; i++) {
              this.total +=this.reparaciones[i].total;
            }
            return (this.total/1.15).toFixed(2);
      },
      iva: function () {
          this.total = 0;
            for (var i = 0; i < this.reparaciones.length; i++) {
              this.total +=this.reparaciones[i].total;
            }
            return ((this.total/1.15)*.15).toFixed(2);
      }
    },
    methods:{
      updateHTMLElements: function(){
        $(document).ready(function(){
          setTimeout(function(){ Materialize.updateTextFields(); }, 10);
          setTimeout(function(){ $('select').material_select(); }, 10);
        });
      },
      upDate: function(index){
          setTimeout(function(){ $('#' + index).pickadate({selectMonths: true,selectYears: 15,today: 'Today',clear: 'Clear',close: 'Ok',closeOnSelect: true});}, 1000);
      },
      agregaReparacion: function(){

        this.reparaciones.push({
          reparacion_id: 'null',
          estatus: '',
          cantidad: 1,
          precio: 0,
          total: 0,
        });
       this.updateHTMLElements();
      },
      eliminaReparacion: function(index){
        if(this.reparaciones.length > 0){
          this.reparaciones.splice(index, 1);
        }
        console.log(index);
      },
      agregaObservacion: function(){

        this.observaciones.push({
          descripcion: '',
          fecha: '',
          });
          this.upDate((this.observaciones.length-1).toString());
      },
      agregaObservacion2: function(){
        if(this.descripcion1!='' &&  $("#fecha8").val()!='')
        {
          this.observaciones.push({
            descripcion: this.descripcion1,
            fecha: $("#fecha8").val(),
          });
            $('#modal1').modal('close');
            this.descripcion1='';
            this.fecha8 = '';
        }
        else {
          alert("Faltan campos");
        }
      },
      editaObservacion2: function(){
        if(this.descripcion1!='' &&  $("#fecha9").val()!='')
        {
          this.observaciones[this.activa].descripcion = this.descripcion1;
          this.observaciones[this.activa].fecha =  $("#fecha9").val();
          $('#modal2').modal('close');
          this.descripcion1='';
          this.fecha9 = '';
          this.activa = -1;
          this.updateHTMLElements();
        }
        else {
          alert("Faltan campos");
        }
      },
      cierraModal: function(){
        $('#modal1').modal('close');
      },
      eliminaObservacion: function(index){
        if(this.observaciones.length > 0){
          this.observaciones.splice(index, 1);
        }
      },
      editaObservacion: function(index){
         let t=this;
         t.descripcion1=t.observaciones[index].descripcion;
         t.activa = index;
         t.updateHTMLElements();
         t.fecha9= t.observaciones[index].fecha;
         $('#modal2').modal('open');
      },
      agregaProducto: function(){
        this.productos.push({
          producto_id: 'null',
          extra: 'null',
          costo: 0,
          valor_producto: 0,
          serie: 'null',
          extra1: 0,
          extra2: 0,
          extra3: 0,
          extra4: 0,
        });
        this.updateHTMLElements();
      },
      eliminaProducto: function(index){
        if(this.productos.length > 0){
          this.productos.splice(index, 1);
        }
      },
      actualizaTotal2: function(i){
        if(this.reparaciones[i].cantidad<1)
          this.reparaciones[i].cantidad = 1;
          if(this.reparaciones[i].precio<0)
            this.reparaciones[i].precio = 0;

        this.reparaciones[i].total = this.reparaciones[i].precio * this.reparaciones[i].cantidad;
      },

    },
  });
  </script>
@endsection
