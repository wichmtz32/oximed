@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class="container">


    <div class="col s12">
      <div class="col m12">
        <h4 style="padding: 0px !important; margin: 0px !important;">Reparaciones</h4><br>
      </div>

        <form method = 'get' class="col m2" action = '{!!url("repair")!!}/create'>
            <button   align="right" class = 'btn col m12' type = 'submit'>Nueva</button>
        </form>

    </div>
    <table id="tabla"  class="display" style="width:100%">
        <thead>
            <th>Cliente</th>
            <th>Equipo</th>
            <th>Fecha</th>
            <th>Fecha Entrega</th>
            <th>Reparador</th>
            <th>Acciones</th>
        </thead>
        <tbody>
            @foreach($repairs as $repair)
              <td>{!!$repair->client!!}</td>
                <td>{!!$repair->product!!}</td>

                <td>{!!$repair->date!!}</td>
                <td>{!!$repair->deliverDate!!}</td>
                <td>{!!$repair->repairman!!}</td>
                <td>
                    <div class = 'row'>
                      <a href = '/repair/{!!$repair->id!!}/edit' class = 'viewEdit btn-floating blue' ><i class = 'material-icons'>edit</i></a>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/repair/{!!$repair->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>
@endsection

@section('scripts')
<script>
$(document).ready(function(){
  $('.tooltipped').tooltip({delay: 50});
});


$(document).ready(function() {
  $('#tabla').dataTable( {

    "paging":   false,
    "ordering": true,
    "info":     true,
      "language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }

} ).on( 'draw.dt', function () {
$('.modal').modal();

} );
} );
</script>

@endsection
