@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show repair
    </h1>
    <form method = 'get' action = '{!!url("repair")!!}'>
        <button class = 'btn blue'>repair Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>product : </i></b>
                </td>
                <td>{!!$repair->product!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>serialNumber : </i></b>
                </td>
                <td>{!!$repair->serialNumber!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>faults : </i></b>
                </td>
                <td>{!!$repair->faults!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>deliverDate : </i></b>
                </td>
                <td>{!!$repair->deliverDate!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>status : </i></b>
                </td>
                <td>{!!$repair->status!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>repairman : </i></b>
                </td>
                <td>{!!$repair->repairman!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection