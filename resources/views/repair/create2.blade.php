@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Create repair
    </h1>
    <form method = 'get' action = '{!!url("repair")!!}'>
        <button class = 'btn blue'>repair Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("repair")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="product" name = "product" type="text" class="validate">
            <label for="product">product</label>
        </div>
        <div class="input-field col s6">
            <input id="serialNumber" name = "serialNumber" type="text" class="validate">
            <label for="serialNumber">serialNumber</label>
        </div>
        <div class="input-field col s6">
            <input id="faults" name = "faults" type="text" class="validate">
            <label for="faults">faults</label>
        </div>
        <div class="input-field col s6">
            <input id="deliverDate" name = "deliverDate" type="text" class="validate">
            <label for="deliverDate">deliverDate</label>
        </div>
        <div class="input-field col s6">
            <input id="status" name = "status" type="text" class="validate">
            <label for="status">status</label>
        </div>
        <div class="input-field col s6">
            <input id="repairman" name = "repairman" type="text" class="validate">
            <label for="repairman">repairman</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection