@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<style media="screen">
  body{
    font-size: 13px;
  }
  .vencido{
    background-color: #FFC8C8;
  }
  .recoleccion{
    background-color: #EAFFEF;
  }
  .porvalidar{
    background-color: #FFD9B7;
  }
  .cancelado{
    background-color: #E8E8E8;
  }
  #tabla tr td, th{
    margin: 0px !important;
    padding: 3px !important;
  }
</style>

<div id="vue-app">
    <div class="col m10">
      <div class="col m3">
        <h4 style="padding: 0px !important; margin: 0px !important;">Contratos</h4>
      </div>
      <div class="input-field col m2">
        <select v-model="filtro_status" id="filtro_status">
          <option value="activo">Activos</option>
          <option value="porvencer">Por vencer</option>
          <option value="vencidos">Vencidos</option>
          <option value="recoleccion">Recoleccion</option>
          <option value="cancelado">Cancelados</option>
        </select>
        <label>Por estatus</label>
      </div>

      <div class="input-field col s4">
        <select v-model="filtro_producto" id="filtro_producto">
          <option value="null">Seleccionar...</option>
          @foreach ($products as $p)
            <option value="{{$p->id}}">{{$p->name}}</option>
          @endforeach
        </select>
        <label>Por producto</label>
      </div>

      <div class="input-field col s3">
        <input id="busqueda" placeholder="Buscar cliente, domicilio ..." type="text" v-model="busqueda" autocomplete="off">
      </div>
    </div>

    <div class="col s2" style="text-align: center;">
      <form method = 'get' action = '{!!url("contract")!!}/create'>
        @can('a_contrato')
          <button class = 'btn col s12' type = 'submit'>nuevo</button>
        @endcan
      </form>
      <a class="btn red col s8" target="_blank" href="/contracts/report" style="margin-top: 3px">EN RENTA</a>
      <a class="btn red col s3 offset-s1 red darken-4" target="_blank" :href=" '/contract/print/' + filtro_status + '/' + filtro_producto" style="margin-top: 3px"><i class="material-icons">save_alt</i> </a>
    </div>
    <table id="tabla" v-if="filtro_status != 'cancelado'">
        <thead>
            <th style="width: 5%">Contrato</th>
            <th style="width: 16%">Cliente</th>
            <th style="width: 15%">Descripcion</th>
            <th style="width: 16%">Domicilio</th>
            <th style="width: 9%">Fecha</th>
            <th style="width: 9%">Vigencia</th>
            <th style="width: 10%">Estatus</th>
            <th style="width: 20%"></th>
        </thead>
        <tbody>
          <tr v-for="c in contracts" :class="{ vencido: c.color == 1, porvalidar: c.color == 2,recoleccion: c.status == 'recoleccion'}" style="border-bottom: 1px solid grey !important">
            <td>@{{c.folio}}</td>
            <td>@{{c.cliente}}</td>
            <td>
              <span v-for="(p, index) in c.productos">
                - @{{p.name}}
              </span>
            </td>
            <td>@{{c.domicilio}}</td>
            <td>@{{c.fecha}}</td>
            <td>@{{c.fechaVigencia}}</td>
            <td>
              <select v-model="c.status" v-on:change="cambiarStatus(c.id ,c.status)" class="browser-default">
                <option style="padding-top: 10px" value="activo">Activo</option>
                <option style="padding-top: 10px" value="recoleccion">Recoleccion</option>
                <option style="padding-top: 10px" value="cancelado">Cancelado</option>
              </select>
            </td>
            <td>
              <div class = 'row'>
                @can('m_contrato')
                  <a :href='c.edit' class = 'viewEdit btn-floating blue'><i class = 'material-icons'>edit</i></a>
                @endcan
                @can('b_contrato')
                <a href = '#modal1' class = 'delete btn-floating modal-trigger red' :data-link="c.delete" ><i class = 'material-icons'>delete</i></a>
                @endcan
                @can('c_contrato')
                  <a :href='c.info' class = 'viewShow btn-floating orange'><i class = 'material-icons'>info</i></a>
                @endcan

                <a v-if="c.color != 3" :href='c.print' class = 'viewShow btn-floating' target="_blank" style="background-color: #9E0505"><i class = 'material-icons'>print</i></a>
                <a v-if="c.color == 3" class = 'viewShow btn-floating'  style="background-color: #909497" @click="mensajeBloqueado()" ><i class = 'material-icons'>lock</i></a>
              </div>
            </td>
          </tr>
        </tbody>
    </table>

    <!-- Cancelados -->
    <table id="tabla" v-if="filtro_status == 'cancelado'">
        <thead>
            <th style="width: 5%">Contrato</th>
            <th style="width: 20%">Cliente</th>
            <th style="width: 20%">Descripcion</th>
            <th style="width: 15%">Chofer recolecta</th>
            <th style="width: 10%">Fecha Recoleccion</th>
            <th style="width: 10%">Estatus</th>
            <th style="width: 20%"></th>
        </thead>
        <tbody>
          <tr v-for="c in contracts"  class="cancelado" style="border-bottom: 1px solid grey !important">
            <td>@{{c.folio}}</td>
            <td>@{{c.cliente}}</td>
            <td>
              <span v-for="(p, index) in c.productos">
                - @{{p.name}}
              </span>
            </td>
            <td>@{{c.choferRecolecta}}</td>
            <td>@{{c.fechaRecoleccion}}</td>
            <td>
              <select v-model="c.status" v-on:change="cambiarStatus(c.id ,c.status)" class="browser-default">
                <option style="padding-top: 10px" value="activo">Activo</option>
                <option style="padding-top: 10px" value="recoleccion">Recoleccion</option>
                <option style="padding-top: 10px" value="cancelado">Cancelado</option>
              </select>
            </td>
            <td>
              <div class = 'row'>
                @can('m_contrato')
                  <a :href='c.edit' class = 'viewEdit btn-floating blue'><i class = 'material-icons'>edit</i></a>
                @endcan
                @can('b_contrato')
                <a href = '#modal1' class = 'delete btn-floating modal-trigger red' :data-link="c.delete" ><i class = 'material-icons'>delete</i></a>
                @endcan
                @can('c_contrato')
                  <a :href='c.info' class = 'viewShow btn-floating orange'><i class = 'material-icons'>info</i></a>
                @endcan

                <a v-if="c.color != 3" :href='c.print' class = 'viewShow btn-floating' target="_blank" style="background-color: #9E0505"><i class = 'material-icons'>print</i></a>
                <a v-if="c.color == 3" :href='c.print' class = 'viewShow btn-floating' target="_blank" style="background-color: #9E0505"><i class = 'material-icons'>lock</i></a>

              </div>
            </td>
          </tr>
        </tbody>
    </table>

</div>
@endsection

@section('scripts')

<script type="text/javascript">


var app = new Vue({
  el: '#vue-app',
  data: {
    status: 'activo',
    filtro_status: 'activo',
    filtro_producto: 'null',
    contracts: [],
    busqueda: '',
  },
  watch: {
    busqueda: function (val) {
      let t = this;
      let url = '{!! url("contract")!!}' + '/search/';
      console.log(t.filtro_producto);
      t.contracts = [];
      if(val == ""){
        t.getContratosAjax();
      }else{
        axios.get(url + val + "/" + t.filtro_status).then(function (response) {
          t.contracts = [];
          for (var i = 0; i < response.data.length; i++) {
            var reg = {
              id: response.data[i].id,
              folio: response.data[i].folio,
              cliente: response.data[i].client,
              productos: response.data[i].products,
              telefono: response.data[i].phone,
              fecha: t.convertDate(response.data[i].date.substring(0,11)),
              fechaVigencia: t.convertDate(response.data[i].validity.substring(0,11)),
              status: response.data[i].status,
              domicilio: response.data[i].address,
              delete: "/contract/" + response.data[i].id + "/deleteMsg",
              edit:"/contract/" + response.data[i].id + "/edit",
              print:"/contract/" + response.data[i].id + "/print",
              info:"/contract/" + response.data[i].id,
              payments: response.data[i].payments,
              color: t.getColor(response.data[i].validity.substring(0,11), response.data[i].payments, response.data[i].threedays),
            };

            if(response.data[i].recolect_date)
              reg.fechaRecoleccion = t.convertDate(response.data[i].recolect_date.substring(0,11));
            else
              reg.fechaRecoleccion = "-";
            if(response.data[i].driver2_name)
              reg.choferRecolecta = response.data[i].driver2_name;
            else
              reg.choferRecolecta = "-";

            t.contracts.push(reg);
          }
          t.updateHTMLElements();
        }).catch(function (error) {
          console.log(error);
        });
      }
    }
  },
  methods: {
    updateStatus: function(){
      alert(this.status);
    },
    updateHTMLElements: function(){
      $(document).ready(function(){
        setTimeout(function(){ Materialize.updateTextFields(); }, 10);
        setTimeout(function(){ $('select').material_select(); }, 10);
      });
    },
    convertDate: function(date){
      let d = new Date(date.replace(/-/gi, '/'));
      d = ("0" + d.getDate()).slice(-2) + "/" + ("0"+(d.getMonth()+1)).slice(-2) + "/" + d.getFullYear();
      return d;
    },
    getColor: function(vigency, payments, porvencer){ //0.- OK (BLANCO) 1.- vencido (ROJO)  2.- Pagos sin validar (NARANJA) 3.- Pagos sin remision (AMARILLO)
      let d1 = new Date(vigency.replace(/-/gi,'/'));
      let d2 = new Date();

      if((d1.getTime() < d2.getTime()) || porvencer == "1" )
        return 1;
      else{
        if( payments != undefined){
          if(payments.length == 0){
            return 3;
          }
          for (var i = 0; i < payments.length; i++) {
            if(payments[i].validate == 'false'){
              return 2;
            }
          }
        }else{
          return 3;
        }
      }
      return 0;
    },
    cambiarStatus: function(id, status){
      let t = this;
      let url = '{!! url("contract")!!}/'+ id +'/updatestatus/';
      if(status == 'cancelado'){
        @can('b_contrato')
        swal({
          title: "¿Esta Seguro?",
          dangerMode: true,
          icon: "warning",
          text: "El contrato pasara a estado CANCELADO",
          buttons: ["No", "Si, cancelar"],
        })
        .then((willDelete) => {
          if (willDelete) {
            axios.get(url + status).then(function (response) {
              Materialize.toast("Contrato cancelado correctamente", 3000);
              for (var i = 0; i < t.contracts.length; i++) {
                if(t.contracts[i].id == id)
                t.contracts[i].status = 'cancelado';
              }
              t.getContratosAjax();

            }).catch(function (error) {
              console.log(error);
            });
          }
          else{
            for (var i = 0; i < t.contracts.length; i++) {
              if(t.contracts[i].id == id)
              t.contracts[i].status = 'activo';
            }
          }
        });
        @else
          for (var i = 0; i < t.contracts.length; i++) {
            if(t.contracts[i].id == id)
            t.contracts[i].status = 'activo';
          }
          swal("Error!","No tiene los permisos necesarios para realizar esta accion");
        @endcan
      }else{
        axios.get(url + status)
        .then(function (response) {
        })
        .catch(function (error) {
          console.log(error);
        });
        t.getContratosAjax();
      }
    },
    mensajeBloqueado: function(){
      Materialize.toast("Impresion bloqueada, falta registrar el pago inicial del contrato.", 4000);
    },
    getContratosAjax: function(){
      let t = this;
      let url = '{!! url("contract")!!}/ajax';
      t.contracts = [];
      axios.get(url + "/"+ t.filtro_status + "/" + t.filtro_producto)
      .then(function (response) {
        t.contracts = [];
        $(document).ready(function(){
          for (var i = 0; i < response.data.length; i++) {
            var reg = {
              id: response.data[i].id,
              folio: response.data[i].folio,
              cliente: response.data[i].client,
              productos: response.data[i].products,
              telefono: response.data[i].phone,
              domicilio: response.data[i].address,
              fecha: t.convertDate(response.data[i].date.substring(0,11)),
              fechaVigencia: t.convertDate(response.data[i].validity.substring(0,11)),
              status: response.data[i].status,
              delete: "/contract/" + response.data[i].id + "/deleteMsg",
              edit:"/contract/" + response.data[i].id + "/edit",
              print:"/contract/" + response.data[i].id + "/print",
              info:"/contract/" + response.data[i].id,
              payments: response.data[i].payments,
              color: t.getColor(response.data[i].validity.substring(0,11), response.data[i].payments, response.data[i].threedays),
            }

            if(response.data[i].recolect_date)
              reg.fechaRecoleccion = t.convertDate(response.data[i].recolect_date.substring(0,11));
            else
              reg.fechaRecoleccion = "-";
            if(response.data[i].driver2_name)
              reg.choferRecolecta = response.data[i].driver2_name;
            else
              reg.choferRecolecta = "-";
            t.contracts.push(reg);
          }

          t.updateHTMLElements();
        });
      })
      .catch(function (error) {
          console.log(error);
      });
    }
  },
  mounted: function () {
    this.$nextTick(function () {
      let t = this;
      t.getContratosAjax();

      $('#filtro_status').on('change', function () {
          t.$emit("change", this.value)
      });
      t.$on("change", function(data){
          this.filtro_status = data;
          t.getContratosAjax();
      });

      $('#filtro_producto').on('change', function () {
          t.$emit("change1", this.value)
      });
      t.$on("change1", function(data){
          this.filtro_producto = data;
          t.getContratosAjax();
      });
    });
  }
});
</script>



@endsection
