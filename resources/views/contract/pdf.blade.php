<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <title>Contrato</title>
    <style media="screen">
    body{
      font-family:sans-serif !important;
    }
    .llenado{
      font-weight: bold;
      text-decoration: underline;
    }
    .content{
      font-size: 8px;
      text-align: justify;
    }
    hr{
      border-top: .1px solid #8c8b8b;
      margin-top: .3cm
      margin-bottom: .2cm;
    }
    </style>
  </head>
  <body>
    <div class="container" style="page-break-after: always !important;">
      <div style="align: justify">
        <div style="position: absolute; left: 15cm; top: -.5cm">
          <img src="{{public_path() . "/images/logoPDF.png" }}" alt="Logo" height="70px"><br>
          <img style="padding-left: 5px" src="{{public_path() . "/images/subLogo.png" }}" alt="Logo2" height="15px"><br>
          <img style="padding-left: 35px; padding-top: -5px" src="{{public_path() . "/images/sublogo2.png" }}" alt="Logo3" height="10px">
        </div><br>
        <h3 style="color:red; font-weight: light">No. {{$contract->folio}} <span style=" letter-spacing: 4px; font-size: 15px !important; font-weight:bold; margin: 0px !important; color: black; padding-left: 3cm">TELEFONO DE URGENCIAS<br><span style="padding-left: 7.8cm">8-12-25-02</span></span> </h3>
        <h4 style="text-align: center; letter-spacing: 7px; font-weight:bold; margin: 0px !important">CONTRATO DE ARRENDAMIENTO</h4>
        <p class="content">
          CONTRATO DE ARRENDAMIENTO QUE CELEBRAN POR UNA PARTE OXIMED Y/O ROBERTO ACEVES ALVARADO CON DOMICILIO EN PEDRO MORENO #735-F COL. SANTIAGO DEL RIO CP.78049
          EN SAN LUIS POTOSI, S.L.P QUIEN SE LE DENOMINARA "LA ARRENDADORA" Y LA QUE ASI SERA MENCIONADA EN LO SUCESIVO Y POR LA
          OTRA PARTE <span class="llenado" style="font-size: 9px !important">{{strtoupper($contract->client)}}</span> CON DOMICILIO EN <span class="llenado" style="font-size: 9px !important">{{strtoupper($contract->address)}} {{strtoupper($contract->suburb)}} {{strtoupper($contract->cp)}}</span>
          REPRESENTADA COMO "LA ARRENDATARIA", QUIEN ASI SERA MENCIONADA EN EL CURSO DE ESTE INSTRUMENTO DE ACUERDO A LAS SIGUIENTES DECLARACIONES Y CLAUSULAS. <br><br>
          <span style="font-size: 10px">REFERENCIAS: <span class="llenado">{{strtoupper($contract->notes)}}</span>@for ($i=0; $i < 5; $i++) &nbsp; @endfor</span> <br><br>

          <strong>DECLARACIONES</strong> <br><br>
          1. "LA ARRENDADORA" DECLARA SER PERSONA FISICA Y ESTAR CAPACITADA PARA EL ARRENDAMIENTO DE EQUIPO MEDICINAL PARA OXIGENO O TERAPIA DE APNEA DEL SUEÑO Y OTROS EQUIPOS RELACIONADOS CON EL AREA MEDICA.<br>
          2. "LA ARRENDATARIA" DECLARA ESTAR LEGALEMENTE CAPACITADA PARA CONTRATAR Y TENER A SU CARGO EL CUIDADO DE LOS EQUIPOS RENTADOS ASI COMO SU BUEN SUMINISTRO, CON DOMICILIO EN <span class="llenado">{{strtoupper($contract->address)}} {{strtoupper($contract->suburb)}} {{strtoupper($contract->cp)}}</span>
          PARA LO CUAL NECESITA EMPELAR EQUIPO MEDICO EN GENERAL PROPIEDAD DE "LA ARRENDATARIA" CAUSA DE LA CELEBRACION DEL PRESENTE CONTRATO.<br>
          3. AMBAS PARTES DECLARAN QUE CELEBRAN EL PRESENTE CONTRATO, VOLUNTARIA, LIBREMENTE Y SIN COACCION DE NINGUNA NATURALEZA POR LO QUE ESTAN DE ACUERDO EN QUE NO ADOLECE DE VICIO ALGUNO DEL CONSENTIMIENTO,
          QUE PUEDA HACERLA INEFICAZ OTORGANDO EN EFECTO LAS SIGUIENTES CLAUSULAS:<br><br>
          <strong>CLAUSULAS</strong><br><br>
          PRIMERA.- "LA ARRENDADORA" CONCEDE EL USO O GOCE TEMPORAL, EN ARRENDAMIENTO A "LA ARRENDATARIA" Y ESTA RECIBE EN PERFECTO ESTADO LOS EQUIPOS PARA OXIGENOTERAPIA, APNEA Y OTROS APARATOS MEDICOS QUE A
          CONTINUACION SE DESCRIBEN Y SE CONSIDERAN EN TODO TIEMPO PROPIEDAD DE "LA ARRENDADORA", SOLAMENTE TENDRA "LA ARRENDATARIA" EL DERECHO A USARLO Y BAJO LAS CONDICIONES DE USO MEDICINAL.
          <br><br>
          <strong>DESCRIPCION DEL PRODUCTO O SERVICIO</strong> <br><br>


          <span class="llenado" style="text-decoration: none !important; font-size: 9px">
            @foreach ($contract->products as $p)
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- {{strtoupper($p->product->name)}} @if($p->serie != '' && $p->serie != 'null')  CON NUMERO DE SERIE: {{$p->serie}} @endif <br>
            @endforeach
          </span><br>

          VALOR TOTAL DEL EQUIPO $ <span class="llenado">{{number_format($valor_total, 2)}} ( {{$valor_letra}} )</span>&nbsp;@if(!is_null($contract->deposit))DEPOSITO PARA RENTA $<span class="llenado">{{number_format($contract->deposit, 2)}} ( {{$deposit_letra}} )</span>,@endif
          QUE CUBRE DEL <span class="llenado">{{Carbon\Carbon::parse($contract->date)->format('d/m/Y')}}</span> AL <span class="llenado">{{Carbon\Carbon::parse($contract->validity)->format('d/m/Y')}}</span>.
          PAGO DEL PRIMER MES ES DE $ <span class="llenado">{{number_format($contract->cost, 2)}} ( {{$primer_renta_letra}} )</span>&nbsp;, EL IMPORTE DE LA RENTA POR MES ES DE $ <span class="llenado">{{number_format($renta_mensual, 2)}} ( {{$renta_mensual_letra}} )</span>
          @if(!is_null($contract->flete)) Y EL FLETE TIENE UN VALOR DE $<span class="llenado">{{number_format($contract->flete, 2)}} ( {{$flete_letra}} )</span>@endif PRECIOS MAS I.V.A.<br><br>
          SEGUNDA.- ESTE CONTRATO PUEDE SER PRORROGADO A SU VENCIMIENTO A VOLUNTAD DE LAS PARTES CONTRAYENTES, Y EN ESTE CASO, CUALQUIERA DE ELLAS PUEDE DARLO POR VENCIDO CUANDO LO DESEASE, AVISANDO AL CONTRATANTE CON 24 HRS. DE ANTICIPACION, QUEDANDO EN CONOCIMIENTO DE "LA ARRENDATARIA"
          QUE DE NO DAR AVISO DE RECOLECCION, DE EQUIPO, SE COBRARA LA PARTE PROPORCIONAL DEL PERIODO ESTABLECIDO Y AL DEL ARRENDAMIENTO EN EJERCICIO.<br>
          "LA ARRENDADORA" TEDNRA DERECHO A DAR POR TERMINADO EL PRESENTE CONTRATO DE ARRENDAMIENTO, DENTRO DEL PLAZO FORZOSO, POR INCUMPLIMIENTO DE "LA ARRENDATARIA" DE CUALQUIERA DE LAS CLAUSULAS DE ESTE INSTRUMENTO, O EN CASO DE QUE SE VOLVIERA INSOLVENTE, FUERA DECLARADO EN QUIEBRA, O FUERA EJECUTADA
          O EMBARGADA.<br><br>
          <span style="font-size: 10px">TELEFONO: <span class="llenado">{{strtoupper($contract->phone)}}</span>@for ($i=0; $i < 5; $i++) &nbsp; @endfor</span>
          <span style="font-size: 10px">CELULAR: <span class="llenado">{{strtoupper($contract->cellphone)}}</span>@for ($i=0; $i < 19; $i++) &nbsp; @endfor <br><br></span>
          <span style="font-size: 10px">FORMA DE PAGO: <span class="llenado">{{strtoupper($contract->charge)}}</span>&nbsp;</span>

          <br><br>
          HORA:<span class="llenado">@for ($i=0; $i < 15; $i++) &nbsp; @endfor </span>@for ($i=0; $i < 5; $i++) &nbsp; @endfor ENTREGA: <span class="llenado">@for ($i=0; $i < 15; $i++) &nbsp; @endfor </span>@for ($i=0; $i < 19; $i++) &nbsp; @endfor
          HORA:<span class="llenado">@for ($i=0; $i < 15; $i++) &nbsp; @endfor </span>@for ($i=0; $i < 5; $i++) &nbsp; @endfor RECOLECCION: <span class="llenado">@for ($i=0; $i < 15; $i++) &nbsp; @endfor </span>
            <br>
          <hr>

        </p>
          <div style="border: 1px solid black; border-radius: 15px; height: 255px;">
            <h3 style="color:red; font-weight: light; position: absolute; left: 1cm" >No. {{$contract->folio}}</h3>
            <div>
              <br>
              <p class="content" style="padding: 10px">
                @for ($i=0; $i < 40; $i++) &nbsp; @endfor NO. 1/1 @for ($i=0; $i < 40; $i++) &nbsp; @endfor BUENO POR: $ <span class="llenado">&nbsp; &nbsp;&nbsp;&nbsp;{{number_format($valor_total, 2)}} &nbsp;&nbsp;&nbsp;&nbsp;</span>  <br><br><br>
                EN SAN LUIS POTOSI A <span class="llenado">&nbsp;{{Carbon\Carbon::parse($contract->date)->format('d')}}&nbsp;</span> DE
                  @if(Carbon\Carbon::parse($contract->date)->format('m') == 1) <span class="llenado">&nbsp;ENERO&nbsp; </span>@endif
                  @if(Carbon\Carbon::parse($contract->date)->format('m') == 2) <span class="llenado">&nbsp;FEBRERO&nbsp; </span>@endif
                  @if(Carbon\Carbon::parse($contract->date)->format('m') == 3) <span class="llenado">&nbsp;MARZO&nbsp; </span>@endif
                  @if(Carbon\Carbon::parse($contract->date)->format('m') == 4) <span class="llenado">&nbsp;ABRIL&nbsp; </span>@endif
                  @if(Carbon\Carbon::parse($contract->date)->format('m') == 5) <span class="llenado">&nbsp;MAYO&nbsp; </span>@endif
                  @if(Carbon\Carbon::parse($contract->date)->format('m') == 6) <span class="llenado">&nbsp;JUNIO&nbsp; </span>@endif
                  @if(Carbon\Carbon::parse($contract->date)->format('m') == 7) <span class="llenado">&nbsp;JULIO&nbsp; </span>@endif
                  @if(Carbon\Carbon::parse($contract->date)->format('m') == 8) <span class="llenado">&nbsp;AGOSTO&nbsp; </span>@endif
                  @if(Carbon\Carbon::parse($contract->date)->format('m') == 9) <span class="llenado">&nbsp;SEPTIEMBRE&nbsp; </span>@endif
                  @if(Carbon\Carbon::parse($contract->date)->format('m') == 10) <span class="llenado">&nbsp;OCTUBRE&nbsp; </span>@endif
                  @if(Carbon\Carbon::parse($contract->date)->format('m') == 11) <span class="llenado">&nbsp;NOVIEMBRE&nbsp; </span>@endif
                  @if(Carbon\Carbon::parse($contract->date)->format('m') == 12) <span class="llenado">&nbsp;DICIEMBRE&nbsp; </span>@endif
                DEL <span class="llenado">&nbsp;{{Carbon\Carbon::parse($contract->date)->format('Y')}}&nbsp;</span>,<br>
                DEBE(MOS) Y PAGARE(MOS) INCONDICIONALMENTE POR ESTE PAGARE A LA ORDEN DE ROBERTO ACEVES ALVARADO EN SAN LUIS POTOSI, S.L.P.
                LA CANTIDAD DE: <span class="llenado">&nbsp;&nbsp;&nbsp;{{ $valor_letra }} &nbsp;&nbsp;&nbsp;</span>,&nbsp;
                VALOR RECIBIDO A MI, (NUESTRA) ENTERA SATISFACCION, ESTE PAGARE FORMA PARTE DE LA SERIE NUMERADA DEL 11 AL 1 Y TODOS ESTAN SUJETOS A LA CONDICION DE QUE, AL NO PAGARSE CUALQUIERA DE ELLOS A SU VENCIMIENTO,
                SERAN EXIGIBLES TODOS LOS QUE LE SIGAN EN NUMERO ADEMAS DE LOS YA VENCIDOS, DESDE LA FECHA DE VENCIMIENTO DE ESTE DOCUMENTO HASTA EL DIA DE SU LIQUIDACION, CAUSARA INTERES MORATORIOS AL TIPO DE 8% MENSUAL,
                PAGADERO EN ESTA CIUDAD JUNTAMENTE CON EL PRINCIPAL.<br><br>
                @for ($i=0; $i < 30; $i++) &nbsp; @endfor ACEPTO (AMOS) @for ($i=0; $i < 40; $i++) &nbsp; @endfor NOMBRE Y DATOS DEL DEUDOR <br><br>
                @for ($i=0; $i < 64; $i++) &nbsp; @endfor NOMBRE: <span class="llenado" style="font-size: 7px">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;{{strtoupper($contract->client)}}&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</span> <br>
                FIRMA(S)<span class="llenado">@for ($i=0; $i < 25; $i++) &nbsp; @endfor</span>    @for ($i=0; $i < 30; $i++) &nbsp; @endfor   DIRECCION: <span class="llenado" style="font-size: 7px">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;{{strtoupper($contract->address)}}&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> <br>
                @for ($i=0; $i < 64; $i++) &nbsp; @endfor POBLACION: <span class="llenado" style="font-size: 7px">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;SAN LUIS POTOSI, S.L.P.&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</span>TEL: <span class="llenado" style="font-size: 6px">&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{strtoupper($contract->phone)}}&nbsp; &nbsp;&nbsp;&nbsp;</span><br><br><br>
                @for ($i=0; $i < 20; $i++) &nbsp; @endfor <span style="font-size: 14px; font-weight: bold" >NO HAY DEVOLUCIONES DE DINERO POR CANCELACION DE SERVICIO</span>
              </p>
            </div>
          </div>
      </div>
      <p class="content" style="font-size: 10px; font-weight: bold">
        PEDRO MORENO #735-F, SANTIAGO DEL RIO, S.L.P<br>
        MANUEL L. REVUELTA #225, BUROCRATAS DEL ESTADO, S.L.P <br>
        TEL. 8122501
      </p>
    </div>
    <div class="container" style="page-break-after: always !important;">
      <div style="align: justify">
        <p class="content">
          TERCERA.- EL PAGO DE LAS RENTAS DEBERA HACERLO INELUDIBLE "LA ARRENDATARIA" EN LAS OFICINAS DE "LA ARRENDADORA", UBICADAS EN PEDRO MORENO #735-F COL. SANTIAGO DEL RIO, SAN LUIS POTOSI, S.L.P. AL RESPECTO SE ENTIENDE QUE SI "LA ARRENDATARIA"
          CUBIRERA EXTEMPORALMENTE A "LA ARRENDADORA" LAS RENTAS ESTIPULADAS INDEPENDIENTEMENTE DE QUE SE OBLIGARA A PAGAR INTERESES MORATORIOS A RAZON DEL 7% MENSUAL, NO POR ELLO SE TENDRA POR MODIFICADO EL PACTO RELATIVO
          A LA FORMA DE PAGO QUE ASI SE ESTIPULA. <br><br>
          CUARTA.- DURANTE TODO EL TIEMPO DEL ARRENDAMIENTO LOS EQUIPOS ARRENDADOS DEBERAN SER USADOS EN LA OBRA QUE SE MENCIONA EN LA DECLARACION DOS Y SERAN PARA USO EXCLUSIVO OBJETO QUE DETERMINA LA PROPIA NATURALEZA DEL EQUIPO
          ARRENDADO Y "LA ARRENDATARIA" SE OBLIGA A NO TRASLADARLO A OTRO LUGAR SIN EL CONSENTIMIENTO ESCRITO DE "LA ARRENDATARIA".<br><br>
          QUINTA.- "LA ARRENDATARIA" ACEPTA INELUDIBLEMENTE QUE NO SE PODRA TRANSFERIR, SUBARRENDAR, PRESTAR O DAR POR CUALQUIER CONCEPTO A NINGUNA OTRA PERSONA JURIDICA O FISCAL O ENTIDAD DISTINTA DE ELLA, EL EQUIPO MATERIAL DE ESTA
          CONTRATACION Y TAMPOCO PODRA EN CONSECUENCIA CEDER NI TRASPASAR LOS DERECHOS DERIVADOS DEL PRESENTE CONTRATO. <br><br>
          SEXTA.- "LA ARRENDATARIA" ACEPTA QUE TAMPOCO PODRA, SIN EL CONSENTIMIENTO ESCRITO DE "LA ARRENDADORA".<br><br>
          SEPTIMA.- "LA ARRENDATARIA" SE OBLIGA A INSTALAR Y USAR EL EQUIPO EN FORMA SEGURA Y ADECUADA DE CONFORMIDAD CON LOS DISEÑOS Y REGLAS DE SEGURIDAD Y DISPOSICIONES DE "LA ARRENDADORA" QUE MANIFIESTA CONOCER INTEGRAMENTE Y AL RESPECTO.
          "LA ARRENDADORA" NO ASUME RESPONSABILIDAD ALGUNA SOBRE LA FORMA DE INSTALACION Y USO DEL EQUIPO QUE HAGA "LA ARRENDATARIA" PRECISAMENTE POR ESTE CONCEPTO TODA LA RESPONSABILIDAD DE RECLAMACIONES A TERCEROS, PROVENIENTES DE LA INSTALACION,
          USO O POSESION DEL EQUIPO, ARRENDADO Y CONSECUENTEMENTE, RELEVA A "LA ARRENDADORA" DE TODA RESPONSABILIDAD RELACIONADA CON CUALQUIER RECLAMACION.<br><br>
          OCTAVA.- EL ALMACENAMIENTO, FLETES, TRANSPORTE DE CARGA, DEVOLUCION Y DEMAS GASTOS INHERENTES SERAN CUBIERTOS POR "LA ARRENDATARIA" QUIEN DEBERA ENTREGAR EL EQUIPO ARRENDADO EN LAS INSTALACIONES DE "LA ARRENDADORA" AL DARSE POR TERMINADO
          ESTE PACTO CONTRATAL. COMPROMETIENDOSE A DEVOLVER LOS EQUIPOS ARRENDADOS Y ACCESORIOS DE "LA ARRENDADORA", EN LAS MISMAS BUENAS CONDICIONES EN QUE LE FUERON ENTREGADOS A SATISFACCION DE LA PROPIA ARRENDADORA, TENIENDO TAN SOLO EN CUENTA
          EL DESGASTE NATURAL ORIGINADOS POR EL USO RAZONABLE Y ADECUADO DEL MISMO.<br>
          AL VENCIMIENTO DEL PRESENTE CONTRATO, O ANTES SI ESTE SE DIERA POR TERMINADO EN LOS TERMINOS DEL SEGUNDO PARRAFO DE LA CLAUSULA SEGUNDA "LA ARRENDADORA" QUEDARA EN LIBERTAD DE RETIRAR SUS IMPLEMENTOS Y EQUIPOS DEL LUGAR DONDE SE ENCONTRASEN
          , SI A SU REQUERIMIENTO NO LE FUERAN DEVUELTOS O BIEN SI ESTOS FUERAN ABANDONADOS POR EL ARRENDATARIO, POR LO CUAL DESDE AHORA DA SU MAS AMPLIA AUTORIZACION Y PERMITE LA ENTRADA AL LUGAR DONDE PODRA SER RETIRADO LO ARRENDADO, RELEVANDO DE TODA
          RESPONSABILIDAD A TERCERAS PERSONAS POR LA ENTREGA SI EL EQUIPO O IMPLEMENTO SE ENCUENTREN EN POSESION O BAJO CUSTODIA O EN LUGAR PROPIEDAD DE ESTA ULTIMA.<br><br>
          NOVENA.- EL EQUIPO RENTADO ES ENTREGADO POR CUENTA Y RIESGO DE LA ARRENDATARIA, Y EN CASO DE DAÑO, PERDIDA, ROBO O DESTRUCCION PARCIAL O TOTAL "LA ARRENDATARIA" PAGARA A "LA ARRENDADORA" EL IMPORTE DEL PRECIO DEL EQUIPO AFECTADO DE ACUERDO CON EL PRECIO
          ESTIPULADO EN LA CLAUSULA PRIMERA DEL PRESENTE CONTRATO.<br><br>
          DECIMA.- EL ARRENDAMIENTO DEL EQUIPO AMPARADO POR ESTE CONTRATO NO SE CANCELA TOTAL O PARCIALMENTE HASTA EL AVISO TELEFONICO O PERSONALMENTE A NUESTRAS OFICINAS O LA ENTREGA DEL MISMO EN NUESTRO DOMICILIO.<br><br>
          DECIMA PRIMERA.- LA ARRENDATARIA AFIRMA QUE LOS APARATOS AQUI ARRENDADOS FUERON PRESCRITOS CON ANTERIORIDAD POR UN MEDICO CON REGISTRO Y QUE EL MISMO ARRENDATARIO ES EL RESPONSABLE DEL SUMINISTRO DE LOS APARATOS, SIN NO FUERA PRESENTADA LA RECERA MEDICA
          EL ARRENDATARIO SE HACE RESPONSABLE DE SUS USO Y REQUERIMIENTO ASI COMO DE LAS CONSECUENCIAS DERIVADAS DE SU MAL USO.<br><br>
          DECIMA SEGUNDA.- LAS PARTES RENUNCIAN AL FUERO DEL DOMICILIO QUE TENGAN O LLEGARAN A TENER Y SE SOMETEN EXPRESAMENTE A LA COMPETENCIA Y JURISDICCION DE LOS TRIBUNALES COMUNES EN LA CIUDAD DE SAN LUIS POTOSI, S.L.P Y A LAS LEYES VIGENTES DE DICHA
          LOCALIDAD PARA TODO LO RELACIONADO A CUALQUIER CONTROVERSIA QUE SUGIERE CON MOTIVOS DE LA INTERPRETACION Y CUMPLIMIENTO DEL PRESENTE CONTRATO.<br>
          LAS PARTES DEBIDAMENTE ENTERADAS DEL CONTENIDO Y ALCANCE DEL PRESENTE CONTRATO POR SU LECTURA PREVIA Y EXPLICACION SATISFACTORIA, Y ESPECIFICANDO CONOCER A SI MISMO CABALMENTE LAS DISPOSICIONES LEGALES CONTENIDAS EN ESTE INSTRUMENTO LO FIRMAN EN LA CIUDAD
          DE SAN LUIS POTOSI, S.L.P,EL DIA <span class="llenado">{{Carbon\Carbon::parse($contract->date)->format('d/m/Y')}}</span>
          <br><br><br><br>
            @for ($i=0; $i < 20; $i++) &nbsp; @endfor <span class="llenado">@for ($i=0; $i < 30; $i++) &nbsp; @endfor </span>@for ($i=0; $i < 40; $i++) &nbsp; @endfor <span class="llenado">@for ($i=0; $i < 30; $i++) &nbsp; @endfor </span><br>
            @for ($i=0; $i < 26; $i++) &nbsp; @endfor "LA ARRENDADORA"</span>@for ($i=0; $i < 53; $i++) &nbsp; @endfor "LA ARRENDATARIA"<br>
            @for ($i=0; $i < 30; $i++) &nbsp; @endfor OXIMED Y/O</span>@for ($i=0; $i < 53; $i++) &nbsp; @endfor <br>
            @for ($i=0; $i < 22; $i++) &nbsp; @endfor ROBERTO ACEVES ALVARADO</span>@for ($i=0; $i < 53; $i++) &nbsp; @endfor <br>
          <br><br><br><br>
          NOTA: LA ENTREGA DE LOS SERVICIOS REQUIERE POR LO MENOS DE 2 A 3 HRS DE PREPARACION DEL EQUIPO. EL COSTO POR CANCELACION AUN ESTANDO EN EL LUGAR ES DE $500.00 PESOS POR SERVICIO Y EL CLIENTE RECONOCE SU RESPONSABILIDAD POR LAS CONSECUENCIAS DERIVADAS DE SU
          FALTA DE PREVISION.<br><br>
        </p>
        <div style="text-align:center">
          <span style="font-size: 14px; font-weight: bold" >NO HAY DEVOLUCIONES EN RENTA DE EQUIPO MEDICO, POR SU DEVOLUCION ANTES DE QUE </span><br>
          <span style="font-size: 14px; font-weight: bold" >EL CONTRATO VENZA, O POR SU FALTA DE USO (PRODUCTOS) ¡¡¡¡¡GRACIAS!!!!!</span>
        </div>
      </div>
    </div>










    <div class="container" style="page-break-after: always !important;">
      <div style="align: justify">
        <div style="position: absolute; left: 15cm; top: -.5cm">
          <img src="{{public_path() . "/images/logoPDF.png" }}" alt="Logo" height="70px"><br>
          <img style="padding-left: 5px" src="{{public_path() . "/images/subLogo.png" }}" alt="Logo2" height="15px"><br>
          <img style="padding-left: 35px; padding-top: -5px" src="{{public_path() . "/images/sublogo2.png" }}" alt="Logo3" height="10px">
        </div>
        <h4 style="text-align: center; letter-spacing: 1px; font-weight:light; margin: 0px !important; color: red">- COPIA CLIENTE - </h4>
        <h3 style="color:red; font-weight: light">No. {{$contract->folio}} <span style=" letter-spacing: 4px; font-size: 15px !important; font-weight:bold; margin: 0px !important; color: black; padding-left: 3cm">TELEFONO DE URGENCIAS<br><span style="padding-left: 7.8cm">8-12-25-02</span></span> </h3>
        <h4 style="text-align: center; letter-spacing: 7px; font-weight:bold; margin: 0px !important">CONTRATO DE ARRENDAMIENTO</h4>
        <p class="content">
          CONTRATO DE ARRENDAMIENTO QUE CELEBRAN POR UNA PARTE OXIMED Y/O ROBERTO ACEVES ALVARADO CON DOMICILIO EN PEDRO MORENO #735-F COL. SANTIAGO DEL RIO CP.78049
          EN SAN LUIS POTOSI, S.L.P QUIEN SE LE DENOMINARA "LA ARRENDADORA" Y LA QUE ASI SERA MENCIONADA EN LO SUCESIVO Y POR LA
          OTRA PARTE <span class="llenado" style="font-size: 9px !important">{{strtoupper($contract->client)}}</span> CON DOMICILIO EN <span class="llenado" style="font-size: 9px !important">{{strtoupper($contract->address)}} {{strtoupper($contract->suburb)}} {{strtoupper($contract->cp)}}</span>
          REPRESENTADA COMO "LA ARRENDATARIA", QUIEN ASI SERA MENCIONADA EN EL CURSO DE ESTE INSTRUMENTO DE ACUERDO A LAS SIGUIENTES DECLARACIONES Y CLAUSULAS. <br><br>
          <span style="font-size: 10px">REFERENCIAS: <span class="llenado">{{strtoupper($contract->notes)}}</span>@for ($i=0; $i < 5; $i++) &nbsp; @endfor</span> <br><br>
          <strong>DECLARACIONES</strong> <br><br>
          1. "LA ARRENDADORA" DECLARA SER PERSONA FISICA Y ESTAR CAPACITADA PARA EL ARRENDAMIENTO DE EQUIPO MEDICINAL PARA OXIGENO O TERAPIA DE APNEA DEL SUEÑO Y OTROS EQUIPOS RELACIONADOS CON EL AREA MEDICA.<br>
          2. "LA ARRENDATARIA" DECLARA ESTAR LEGALEMENTE CAPACITADA PARA CONTRATAR Y TENER A SU CARGO EL CUIDADO DE LOS EQUIPOS RENTADOS ASI COMO SU BUEN SUMINISTRO, CON DOMICILIO EN <span class="llenado">{{strtoupper($contract->address)}} {{strtoupper($contract->suburb)}} {{strtoupper($contract->cp)}}</span>
          PARA LO CUAL NECESITA EMPELAR EQUIPO MEDICO EN GENERAL PROPIEDAD DE "LA ARRENDATARIA" CAUSA DE LA CELEBRACION DEL PRESENTE CONTRATO.<br>
          3. AMBAS PARTES DECLARAN QUE CELEBRAN EL PRESENTE CONTRATO, VOLUNTARIA, LIBREMENTE Y SIN COACCION DE NINGUNA NATURALEZA POR LO QUE ESTAN DE ACUERDO EN QUE NO ADOLECE DE VICIO ALGUNO DEL CONSENTIMIENTO,
          QUE PUEDA HACERLA INEFICAZ OTORGANDO EN EFECTO LAS SIGUIENTES CLAUSULAS:<br><br>
          <strong>CLAUSULAS</strong><br><br>
          PRIMERA.- "LA ARRENDADORA" CONCEDE EL USO O GOCE TEMPORAL, EN ARRENDAMIENTO A "LA ARRENDATARIA" Y ESTA RECIBE EN PERFECTO ESTADO LOS EQUIPOS PARA OXIGENOTERAPIA, APNEA Y OTROS APARATOS MEDICOS QUE A
          CONTINUACION SE DESCRIBEN Y SE CONSIDERAN EN TODO TIEMPO PROPIEDAD DE "LA ARRENDADORA", SOLAMENTE TENDRA "LA ARRENDATARIA" EL DERECHO A USARLO Y BAJO LAS CONDICIONES DE USO MEDICINAL.
          <br><br>
          <strong>DESCRIPCION DEL PRODUCTO O SERVICIO</strong> <br><br>


          <span class="llenado" style="text-decoration: none !important; font-size: 9px">
            @foreach ($contract->products as $p)
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- {{strtoupper($p->product->name)}} @if($p->serie != '' && $p->serie != 'null') CON NUMERO DE SERIE: {{$p->serie}} @endif <br>
            @endforeach
          </span><br>

          VALOR TOTAL DEL EQUIPO $ <span class="llenado">{{number_format($valor_total, 2)}} ( {{$valor_letra}} )</span>&nbsp;@if(!is_null($contract->deposit))DEPOSITO PARA RENTA $<span class="llenado">{{number_format($contract->deposit, 2)}} ( {{$deposit_letra}} )</span>,@endif
          QUE CUBRE DEL <span class="llenado">{{Carbon\Carbon::parse($contract->date)->format('d/m/Y')}}</span> AL <span class="llenado">{{Carbon\Carbon::parse($contract->validity)->format('d/m/Y')}}</span>.
          PAGO DEL PRIMER MES ES DE $ <span class="llenado">{{number_format($contract->cost, 2)}} ( {{$primer_renta_letra}} )</span>&nbsp;, EL IMPORTE DE LA RENTA POR MES ES DE $ <span class="llenado">{{number_format($renta_mensual, 2)}} ( {{$renta_mensual_letra}} )</span>
          @if(!is_null($contract->flete)) Y EL FLETE TIENE UN VALOR DE $<span class="llenado">{{number_format($contract->flete, 2)}} ( {{$flete_letra}} )</span>@endif PRECIOS MAS I.V.A.<br><br>
          SEGUNDA.- ESTE CONTRATO PUEDE SER PRORROGADO A SU VENCIMIENTO A VOLUNTAD DE LAS PARTES CONTRAYENTES, Y EN ESTE CASO, CUALQUIERA DE ELLAS PUEDE DARLO POR VENCIDO CUANDO LO DESEASE, AVISANDO AL CONTRATANTE CON 24 HRS. DE ANTICIPACION, QUEDANDO EN CONOCIMIENTO DE "LA ARRENDATARIA"
          QUE DE NO DAR AVISO DE RECOLECCION, DE EQUIPO, SE COBRARA LA PARTE PROPORCIONAL DEL PERIODO ESTABLECIDO Y AL DEL ARRENDAMIENTO EN EJERCICIO.<br>
          "LA ARRENDADORA" TEDNRA DERECHO A DAR POR TERMINADO EL PRESENTE CONTRATO DE ARRENDAMIENTO, DENTRO DEL PLAZO FORZOSO, POR INCUMPLIMIENTO DE "LA ARRENDATARIA" DE CUALQUIERA DE LAS CLAUSULAS DE ESTE INSTRUMENTO, O EN CASO DE QUE SE VOLVIERA INSOLVENTE, FUERA DECLARADO EN QUIEBRA, O FUERA EJECUTADA
          O EMBARGADA.<br><br>
            <span style="font-size: 10px">TELEFONO: <span class="llenado">{{strtoupper($contract->phone)}}</span>@for ($i=0; $i < 5; $i++) &nbsp; @endfor</span>
            <span style="font-size: 10px">CELULAR: <span class="llenado">{{strtoupper($contract->cellphone)}}</span>@for ($i=0; $i < 19; $i++) &nbsp; @endfor <br><br></span>
          <span style="font-size: 10px">FORMA DE PAGO: <span class="llenado">{{strtoupper($contract->charge)}}</span>&nbsp;</span>

          <br><br>
          HORA:<span class="llenado">@for ($i=0; $i < 15; $i++) &nbsp; @endfor </span>@for ($i=0; $i < 5; $i++) &nbsp; @endfor ENTREGA: <span class="llenado">@for ($i=0; $i < 15; $i++) &nbsp; @endfor </span>@for ($i=0; $i < 19; $i++) &nbsp; @endfor
          HORA:<span class="llenado">@for ($i=0; $i < 15; $i++) &nbsp; @endfor </span>@for ($i=0; $i < 5; $i++) &nbsp; @endfor RECOLECCION: <span class="llenado">@for ($i=0; $i < 15; $i++) &nbsp; @endfor </span>
            <br>
          <hr>

        </p>
        <div style="border: 1px solid black; border-radius: 15px; height: 255px;">
          <h3 style="color:red; font-weight: light; position: absolute; left: 1cm" >No. {{$contract->folio}}</h3>
          <div>
            <br>
            <p class="content" style="padding: 10px">
              @for ($i=0; $i < 40; $i++) &nbsp; @endfor NO. 1/1 @for ($i=0; $i < 40; $i++) &nbsp; @endfor BUENO POR: $ <span class="llenado">&nbsp; &nbsp;&nbsp;&nbsp;{{number_format($valor_total, 2)}} &nbsp;&nbsp;&nbsp;&nbsp;</span>  <br><br><br>
              EN SAN LUIS POTOSI A <span class="llenado">&nbsp;{{Carbon\Carbon::parse($contract->date)->format('d')}}&nbsp;</span> DE
                @if(Carbon\Carbon::parse($contract->date)->format('m') == 1) <span class="llenado">&nbsp;ENERO&nbsp; </span>@endif
                @if(Carbon\Carbon::parse($contract->date)->format('m') == 2) <span class="llenado">&nbsp;FEBRERO&nbsp; </span>@endif
                @if(Carbon\Carbon::parse($contract->date)->format('m') == 3) <span class="llenado">&nbsp;MARZO&nbsp; </span>@endif
                @if(Carbon\Carbon::parse($contract->date)->format('m') == 4) <span class="llenado">&nbsp;ABRIL&nbsp; </span>@endif
                @if(Carbon\Carbon::parse($contract->date)->format('m') == 5) <span class="llenado">&nbsp;MAYO&nbsp; </span>@endif
                @if(Carbon\Carbon::parse($contract->date)->format('m') == 6) <span class="llenado">&nbsp;JUNIO&nbsp; </span>@endif
                @if(Carbon\Carbon::parse($contract->date)->format('m') == 7) <span class="llenado">&nbsp;JULIO&nbsp; </span>@endif
                @if(Carbon\Carbon::parse($contract->date)->format('m') == 8) <span class="llenado">&nbsp;AGOSTO&nbsp; </span>@endif
                @if(Carbon\Carbon::parse($contract->date)->format('m') == 9) <span class="llenado">&nbsp;SEPTIEMBRE&nbsp; </span>@endif
                @if(Carbon\Carbon::parse($contract->date)->format('m') == 10) <span class="llenado">&nbsp;OCTUBRE&nbsp; </span>@endif
                @if(Carbon\Carbon::parse($contract->date)->format('m') == 11) <span class="llenado">&nbsp;NOVIEMBRE&nbsp; </span>@endif
                @if(Carbon\Carbon::parse($contract->date)->format('m') == 12) <span class="llenado">&nbsp;DICIEMBRE&nbsp; </span>@endif
              DEL <span class="llenado">&nbsp;{{Carbon\Carbon::parse($contract->date)->format('Y')}}&nbsp;</span>,<br>
              DEBE(MOS) Y PAGARE(MOS) INCONDICIONALMENTE POR ESTE PAGARE A LA ORDEN DE ROBERTO ACEVES ALVARADO EN SAN LUIS POTOSI, S.L.P.
              LA CANTIDAD DE: <span class="llenado">&nbsp;&nbsp;&nbsp;{{ $valor_letra }} &nbsp;&nbsp;&nbsp;</span>,&nbsp;
              VALOR RECIBIDO A MI, (NUESTRA) ENTERA SATISFACCION, ESTE PAGARE FORMA PARTE DE LA SERIE NUMERADA DEL 11 AL 1 Y TODOS ESTAN SUJETOS A LA CONDICION DE QUE, AL NO PAGARSE CUALQUIERA DE ELLOS A SU VENCIMIENTO,
              SERAN EXIGIBLES TODOS LOS QUE LE SIGAN EN NUMERO ADEMAS DE LOS YA VENCIDOS, DESDE LA FECHA DE VENCIMIENTO DE ESTE DOCUMENTO HASTA EL DIA DE SU LIQUIDACION, CAUSARA INTERES MORATORIOS AL TIPO DE 8% MENSUAL,
              PAGADERO EN ESTA CIUDAD JUNTAMENTE CON EL PRINCIPAL.<br><br>
              @for ($i=0; $i < 30; $i++) &nbsp; @endfor ACEPTO (AMOS) @for ($i=0; $i < 40; $i++) &nbsp; @endfor NOMBRE Y DATOS DEL DEUDOR <br><br>
              @for ($i=0; $i < 64; $i++) &nbsp; @endfor NOMBRE: <span class="llenado" style="font-size: 7px">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;{{strtoupper($contract->client)}}&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</span> <br>
              FIRMA(S)<span class="llenado">@for ($i=0; $i < 25; $i++) &nbsp; @endfor</span>    @for ($i=0; $i < 30; $i++) &nbsp; @endfor   DIRECCION: <span class="llenado" style="font-size: 7px">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;{{strtoupper($contract->address)}}&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> <br>
              @for ($i=0; $i < 64; $i++) &nbsp; @endfor POBLACION: <span class="llenado" style="font-size: 7px">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;SAN LUIS POTOSI, S.L.P.&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</span>TEL: <span class="llenado" style="font-size: 6px">&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{strtoupper($contract->phone)}}&nbsp; &nbsp;&nbsp;&nbsp;</span><br><br><br>
              @for ($i=0; $i < 20; $i++) &nbsp; @endfor <span style="font-size: 14px; font-weight: bold" >NO HAY DEVOLUCIONES DE DINERO POR CANCELACION DE SERVICIO</span>
            </p>
          </div>
        </div>
      </div>
      <p class="content" style="font-size: 10px; font-weight: bold">
        PEDRO MORENO #735-F, SANTIAGO DEL RIO, S.L.P<br>
        MANUEL L. REVUELTA #225, BUROCRATAS DEL ESTADO, S.L.P <br>
        TEL. 8122501
      </p>
    </div>
    <div class="container">
      <div style="align: justify">
        <p class="content">
          TERCERA.- EL PAGO DE LAS RENTAS DEBERA HACERLO INELUDIBLE "LA ARRENDATARIA" EN LAS OFICINAS DE "LA ARRENDADORA", UBICADAS EN PEDRO MORENO #735-F COL. SANTIAGO DEL RIO, SAN LUIS POTOSI, S.L.P. AL RESPECTO SE ENTIENDE QUE SI "LA ARRENDATARIA"
          CUBIRERA EXTEMPORALMENTE A "LA ARRENDADORA" LAS RENTAS ESTIPULADAS INDEPENDIENTEMENTE DE QUE SE OBLIGARA A PAGAR INTERESES MORATORIOS A RAZON DEL 7% MENSUAL, NO POR ELLO SE TENDRA POR MODIFICADO EL PACTO RELATIVO
          A LA FORMA DE PAGO QUE ASI SE ESTIPULA. <br><br>
          CUARTA.- DURANTE TODO EL TIEMPO DEL ARRENDAMIENTO LOS EQUIPOS ARRENDADOS DEBERAN SER USADOS EN LA OBRA QUE SE MENCIONA EN LA DECLARACION DOS Y SERAN PARA USO EXCLUSIVO OBJETO QUE DETERMINA LA PROPIA NATURALEZA DEL EQUIPO
          ARRENDADO Y "LA ARRENDATARIA" SE OBLIGA A NO TRASLADARLO A OTRO LUGAR SIN EL CONSENTIMIENTO ESCRITO DE "LA ARRENDATARIA".<br><br>
          QUINTA.- "LA ARRENDATARIA" ACEPTA INELUDIBLEMENTE QUE NO SE PODRA TRANSFERIR, SUBARRENDAR, PRESTAR O DAR POR CUALQUIER CONCEPTO A NINGUNA OTRA PERSONA JURIDICA O FISCAL O ENTIDAD DISTINTA DE ELLA, EL EQUIPO MATERIAL DE ESTA
          CONTRATACION Y TAMPOCO PODRA EN CONSECUENCIA CEDER NI TRASPASAR LOS DERECHOS DERIVADOS DEL PRESENTE CONTRATO. <br><br>
          SEXTA.- "LA ARRENDATARIA" ACEPTA QUE TAMPOCO PODRA, SIN EL CONSENTIMIENTO ESCRITO DE "LA ARRENDADORA".<br><br>
          SEPTIMA.- "LA ARRENDATARIA" SE OBLIGA A INSTALAR Y USAR EL EQUIPO EN FORMA SEGURA Y ADECUADA DE CONFORMIDAD CON LOS DISEÑOS Y REGLAS DE SEGURIDAD Y DISPOSICIONES DE "LA ARRENDADORA" QUE MANIFIESTA CONOCER INTEGRAMENTE Y AL RESPECTO.
          "LA ARRENDADORA" NO ASUME RESPONSABILIDAD ALGUNA SOBRE LA FORMA DE INSTALACION Y USO DEL EQUIPO QUE HAGA "LA ARRENDATARIA" PRECISAMENTE POR ESTE CONCEPTO TODA LA RESPONSABILIDAD DE RECLAMACIONES A TERCEROS, PROVENIENTES DE LA INSTALACION,
          USO O POSESION DEL EQUIPO, ARRENDADO Y CONSECUENTEMENTE, RELEVA A "LA ARRENDADORA" DE TODA RESPONSABILIDAD RELACIONADA CON CUALQUIER RECLAMACION.<br><br>
          OCTAVA.- EL ALMACENAMIENTO, FLETES, TRANSPORTE DE CARGA, DEVOLUCION Y DEMAS GASTOS INHERENTES SERAN CUBIERTOS POR "LA ARRENDATARIA" QUIEN DEBERA ENTREGAR EL EQUIPO ARRENDADO EN LAS INSTALACIONES DE "LA ARRENDADORA" AL DARSE POR TERMINADO
          ESTE PACTO CONTRATAL. COMPROMETIENDOSE A DEVOLVER LOS EQUIPOS ARRENDADOS Y ACCESORIOS DE "LA ARRENDADORA", EN LAS MISMAS BUENAS CONDICIONES EN QUE LE FUERON ENTREGADOS A SATISFACCION DE LA PROPIA ARRENDADORA, TENIENDO TAN SOLO EN CUENTA
          EL DESGASTE NATURAL ORIGINADOS POR EL USO RAZONABLE Y ADECUADO DEL MISMO.<br>
          AL VENCIMIENTO DEL PRESENTE CONTRATO, O ANTES SI ESTE SE DIERA POR TERMINADO EN LOS TERMINOS DEL SEGUNDO PARRAFO DE LA CLAUSULA SEGUNDA "LA ARRENDADORA" QUEDARA EN LIBERTAD DE RETIRAR SUS IMPLEMENTOS Y EQUIPOS DEL LUGAR DONDE SE ENCONTRASEN
          , SI A SU REQUERIMIENTO NO LE FUERAN DEVUELTOS O BIEN SI ESTOS FUERAN ABANDONADOS POR EL ARRENDATARIO, POR LO CUAL DESDE AHORA DA SU MAS AMPLIA AUTORIZACION Y PERMITE LA ENTRADA AL LUGAR DONDE PODRA SER RETIRADO LO ARRENDADO, RELEVANDO DE TODA
          RESPONSABILIDAD A TERCERAS PERSONAS POR LA ENTREGA SI EL EQUIPO O IMPLEMENTO SE ENCUENTREN EN POSESION O BAJO CUSTODIA O EN LUGAR PROPIEDAD DE ESTA ULTIMA.<br><br>
          NOVENA.- EL EQUIPO RENTADO ES ENTREGADO POR CUENTA Y RIESGO DE LA ARRENDATARIA, Y EN CASO DE DAÑO, PERDIDA, ROBO O DESTRUCCION PARCIAL O TOTAL "LA ARRENDATARIA" PAGARA A "LA ARRENDADORA" EL IMPORTE DEL PRECIO DEL EQUIPO AFECTADO DE ACUERDO CON EL PRECIO
          ESTIPULADO EN LA CLAUSULA PRIMERA DEL PRESENTE CONTRATO.<br><br>
          DECIMA.- EL ARRENDAMIENTO DEL EQUIPO AMPARADO POR ESTE CONTRATO NO SE CANCELA TOTAL O PARCIALMENTE HASTA EL AVISO TELEFONICO O PERSONALMENTE A NUESTRAS OFICINAS O LA ENTREGA DEL MISMO EN NUESTRO DOMICILIO.<br><br>
          DECIMA PRIMERA.- LA ARRENDATARIA AFIRMA QUE LOS APARATOS AQUI ARRENDADOS FUERON PRESCRITOS CON ANTERIORIDAD POR UN MEDICO CON REGISTRO Y QUE EL MISMO ARRENDATARIO ES EL RESPONSABLE DEL SUMINISTRO DE LOS APARATOS, SIN NO FUERA PRESENTADA LA RECERA MEDICA
          EL ARRENDATARIO SE HACE RESPONSABLE DE SUS USO Y REQUERIMIENTO ASI COMO DE LAS CONSECUENCIAS DERIVADAS DE SU MAL USO.<br><br>
          DECIMA SEGUNDA.- LAS PARTES RENUNCIAN AL FUERO DEL DOMICILIO QUE TENGAN O LLEGARAN A TENER Y SE SOMETEN EXPRESAMENTE A LA COMPETENCIA Y JURISDICCION DE LOS TRIBUNALES COMUNES EN LA CIUDAD DE SAN LUIS POTOSI, S.L.P Y A LAS LEYES VIGENTES DE DICHA
          LOCALIDAD PARA TODO LO RELACIONADO A CUALQUIER CONTROVERSIA QUE SUGIERE CON MOTIVOS DE LA INTERPRETACION Y CUMPLIMIENTO DEL PRESENTE CONTRATO.<br>
          LAS PARTES DEBIDAMENTE ENTERADAS DEL CONTENIDO Y ALCANCE DEL PRESENTE CONTRATO POR SU LECTURA PREVIA Y EXPLICACION SATISFACTORIA, Y ESPECIFICANDO CONOCER A SI MISMO CABALMENTE LAS DISPOSICIONES LEGALES CONTENIDAS EN ESTE INSTRUMENTO LO FIRMAN EN LA CIUDAD
          DE SAN LUIS POTOSI, S.L.P,EL DIA <span class="llenado">{{Carbon\Carbon::parse($contract->date)->format('d/m/Y')}}</span>
          <br><br><br><br>
            @for ($i=0; $i < 20; $i++) &nbsp; @endfor <span class="llenado">@for ($i=0; $i < 30; $i++) &nbsp; @endfor </span>@for ($i=0; $i < 40; $i++) &nbsp; @endfor <span class="llenado">@for ($i=0; $i < 30; $i++) &nbsp; @endfor </span><br>
            @for ($i=0; $i < 26; $i++) &nbsp; @endfor "LA ARRENDADORA"</span>@for ($i=0; $i < 53; $i++) &nbsp; @endfor "LA ARRENDATARIA"<br>
            @for ($i=0; $i < 30; $i++) &nbsp; @endfor OXIMED Y/O</span>@for ($i=0; $i < 53; $i++) &nbsp; @endfor <br>
            @for ($i=0; $i < 22; $i++) &nbsp; @endfor ROBERTO ACEVES ALVARADO</span>@for ($i=0; $i < 53; $i++) &nbsp; @endfor <br>
          <br><br><br><br>
          NOTA: LA ENTREGA DE LOS SERVICIOS REQUIERE POR LO MENOS DE 2 A 3 HRS DE PREPARACION DEL EQUIPO. EL COSTO POR CANCELACION AUN ESTANDO EN EL LUGAR ES DE $500.00 PESOS POR SERVICIO Y EL CLIENTE RECONOCE SU RESPONSABILIDAD POR LAS CONSECUENCIAS DERIVADAS DE SU
          FALTA DE PREVISION.<br><br>
        </p>
        <div style="text-align:center">
          <span style="font-size: 14px; font-weight: bold" >NO HAY DEVOLUCIONES EN RENTA DE EQUIPO MEDICO, POR SU DEVOLUCION ANTES DE QUE </span><br>
          <span style="font-size: 14px; font-weight: bold" >EL CONTRATO VENZA, O POR SU FALTA DE USO (PRODUCTOS) ¡¡¡¡¡GRACIAS!!!!!</span>
        </div>

      </div>
    </div>






  </body>
</html>
