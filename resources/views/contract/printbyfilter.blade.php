<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <title>Equipo en renta</title>
    <style media="screen" type="text/css" >
    body{
      font-family:sans-serif !important;
    }
    table tr, td{
      padding-top: 10px !important;
      padding-left: 0px !important;
      padding-right: 0px !important;
      font-size: 10px;
      border-bottom: .5px solid gray !important;
      text-align: center !important;
    }

    .page-break { page-break-inside: always; }

    </style>
  </head>
  <body>
    <div>
      <div style="text-align: center">
        <span style="font-weight: bold; font-size: 14px">
            O X I M E D<br>
            REPORTE DE CONTRATOS {{$status}}<br><span style="font-size: 12px">
              CANTIDAD: {{count($contracts)}}<br>
              FECHA: {{Carbon\Carbon::parse($today->format('Y-m-d'))->format('d/m/Y')}}<br>
            </span>
            <br><br>
        </span>
      </div>
      <div class="page-break">
        <table  style="width:100%" >
          <thead>
            <tr>
              <th>CONTRATO</th>
              <th>CLIENTE</th>
              <th>PRODUCTOS</th>
              @if($status == 'CANCELADOS')
                <th>RECOLECTO</th>
                <th>FECHA RECOLECCION</th>
              @else
                <th>FECHA</th>
                <th>VIGENCIA</th>
              @endif
            </tr>
          </thead>
          <tbody>
            @foreach ($contracts as $c)
                <tr>
                  <td>{{$c['folio']}}</td>
                  <td>{{$c['client']}}</td>
                  <td>
                    @if(isset($c['products']))
                      @foreach ($c['products'] as $p)
                        <span>
                          - {{$p['name']}}
                        </span>
                      @endforeach
                    @endif
                  </td>
                  @if($status == 'CANCELADOS')
                    <td>@if(isset ($c['driver2_name'])) {{$c['driver2_name']}} @else - @endif</td>
                    <td> @if(isset ($c['recolect_date'])) {{Carbon\Carbon::parse($c['recolect_date'])->format('d/m/Y')}} @else - @endif</td>
                  @else
                    <td>{{Carbon\Carbon::parse($c['date'])->format('d/m/Y')}}</td>
                    <td>{{Carbon\Carbon::parse($c['validity'])->format('d/m/Y')}}</td>
                  @endif
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </body>
</html>
