@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div id="vue-app">
  <div id="ModalPagos" class="modal bottom-sheet" >
    <div class="modal-content">
      <h4 v-if="!b_editpago">Registar pago</h4>
      <h4 v-if="b_editpago">Editar pago</h4>
      <div class="col m12">
        <div class="input-field col m3">
          <input id="periodo" name = "periodo" type="text" class="validate" v-model="pago_periodo">
          <label for="periodo">Periodo</label>
        </div>
        <div class="input-field col m1">
          <input id="cantidad" name = "cantidad" type="number"  step="any" class="validate" v-model="pago_cantidad">
          <label for="cantidad">Cantidad / Abono</label>
        </div>
        <div v-if="!b_editpago" class="input-field col m1">
          <input id="total" name="total" type="text" class="validate" v-model="pago_porpagar" readonly>
          <label for="total">Por pagar</label>
        </div>
        <div class="input-field col m2">
          <input id="factura" name = "factura" type="text" class="validate" v-model="pago_factura">
          <label for="factura">Factura / Remision</label>
        </div>
        <div class="input-field col m2">
          <input id="unidad" name = "unidad" type="text" class="validate" v-model="pago_unidad">
          <label for="unidad">Unidad (transporte)</label>
        </div>
        <div class="input-field col m3">
          <input id="notas" name = "notas" type="text" class="validate" v-model="pago_notas">
          <label for="notas">Detalle pago</label>
        </div>
        <div class="col m3" v-if="!b_editpago && pagos.length > 0">
          <input type="checkbox" name="update_vigency" class="checkbox filled-in" id="update_vigency" v-model="pago_checkbox" >
          <label for="update_vigency">¿Actualizar vigencia del contrato?</label>
        </div>
      </div>
      <br><br><br><br>
    </div>
    <div class="modal-footer">
      <button v-if="!b_editpago" @click="registraPago()" class="btn-flat green white-text">ACEPTAR</button>
      <button v-if="b_editpago" @click="actualizaPago()" class="btn-flat green white-text">ACEPTAR</button>
      <a href="" class="modal-close btn-flat">CANCELAR</a>
    </div>
  </div>
  @if($nueva)
  <div class="col m10">
      <h3>Nuevo contrato</h3>
  </div>
  <div class="col m2"><br>
    <p style="font-size: 20px; font-weight: bold">No. NA</p>
  </div>
  @elseif($show)
    <div class="col m10">
        <h3>Información contrato</h3>
        @if(!is_null($contract->user))
          <h6>Realizado por: {!! $contract->user !!}</h6><br>
        @endif
    </div>
    <div class="col m2"><br>
      <p style="font-size: 20px; font-weight: bold">No. {{sprintf('%05d', $contract->folio)}}</p>
    </div>
  @else
    <div class="col m10">
        <h3>Editar contrato</h3>
        @if(!is_null($contract->user))
          <h6>Realizado por: {!! $contract->user !!}</h6><br>
        @endif
    </div>
    <div class="col m2"><br>
      <p style="font-size: 20px; font-weight: bold">No. {{sprintf('%05d', $contract->folio)}}</p>
    </div>
  @endif
    <br>
    @if($nueva)
    <form method = 'POST' action = '{!!url("contract")!!}'>
    @elseif($show)

    @else
    <form method = 'POST' action = '{!! url("contract")!!}/{!!$contract->id!!}/update'>
    @endif
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s4">
            <input id="client" name = "client" type="text" class="validate cliente" v-model="cliente" required>
            <label for="client">Cliente</label>
        </div>
        <div class="input-field col s4">
            <input id="address" name = "address" type="text" class="validate" v-model="direccion" required>
            <label for="address">Direccion</label>
        </div>
        <div class="input-field col s2">
            <input id="phone" name = "phone" type="text" class="validate" v-model="telefono" required>
            <label for="phone">Telefono</label>
        </div>
        <div class="input-field col s2">
            <input id="cellphone" name = "cellphone" type="text" class="validate" v-model="telefono2" required>
            <label for="cellphone">Celular/Telefono2</label>
        </div>
        <div class="input-field col s2">
          @if($nueva)
            <input id="date" name = "date" type="text" class="datepicker">
          @else
            <input id="date" name = "date" type="text" class="datepicker" disabled>
          @endif
          <label for="date">Fecha inicio contrato</label>
        </div>
        <div class="input-field col s3">
          <select id="vueSelect" v-model="conductor_id" name="driver_id" required>
            <option value="null">Seleccionar...</option>
            @foreach($drivers as $d)
              <option value="{{$d->id}}">{{$d->name}}</option>
            @endforeach
          </select>
          <label>Conductor entrega</label>
        </div>
        <div class="input-field col s2">
          @if(Auth::user()->hasRole('IVONNE') or Auth::user()->hasRole('SUPERVISOR') or $nueva)
            <input id="validity" name = "validity" type="text" class="datepicker" required>
          @else
            <input id="validity" name = "validity" type="text" v-show="false" class="datepicker" >
            <input  name = "validity" type="text" v-model="fechaVigencia_show" disabled>
          @endif
            <label for="validity">Fecha vencimiento </label>
        </div>

        <div class="input-field col s3">
          <select id="vueSelect2" v-model="conductor_id2" name="driver_id2">
            <option value="null">Seleccionar...</option>
            @foreach($drivers as $d)
              <option value="{{$d->id}}">{{$d->name}}</option>
            @endforeach
          </select>
          <label>Conductor recolecta</label>
        </div>
        <div class="input-field col s2">
          <input id="recolect_date" name = "recolect_date" type="text" class="datepicker">
          <label for="recolect_date">Fecha recoleccion</label>
        </div>
        <div class="input-field col s2">
          <input id="charge" name = "charge" type="text" v-model="cobro" required>
          <label for="charge">Tipo cobro</label>
        </div>
        <div class="input-field col s2">
          <input id="deposit" name="deposit" type="number" step="any" v-model="deposito">
          <label for="deposit">Deposito para renta</label>
        </div>
        <div class="input-field col s2">
          <input id="flete" name="flete" type="number"  step="any" v-model="flete">
          <label for="flete">Flete</label>
        </div>
        <div class="input-field col s2">
          <input id="total" type="number" step="any" v-model="total" disabled>
          <label for="total">Costo total</label>
        </div>
        <input type="hidden" name="total" :value="total">
        <div class="input-field col s4">
          <input id="notas" name="notes" type="text" v-model="observaciones">
          <label for="notas">Observaciones</label>
        </div>
        <div style="clear:both"><br></div>
        <div>
          <h5 class="col s10">Productos</h5>
          <div class="col s2 center" style="padding-left: 85px">
            <button type="button" class="btn btn-small green flat" @click="agregaProducto()">agregar</button>
          </div>
          <div class="col s12" v-for="(p, index) in productos">
            <div class="col s3">
              <label>Producto</label>
              <select v-model="p.producto_id" name="product_id[]" class="browser-default"  style="height: 40px" @change="seleccionaProducto(index)">
                  <option value="null" disabled>Seleccionar producto...</option>
                  @foreach($products as $p)
                  <option value="{{$p->id}}">{{$p->name}}</option>
                  @endforeach
              </select>
            </div>
            <div class="input-field col s2">
              <input v-model="p.costo" name="cost[]" id="cost" type="number" step="any" class="validate" @input="actualizaTotal()" required>
              <label for="cost">Costo renta</label>
            </div>
            <div class="col s2">
              <label>Cargo extra</label>
              <select v-model="p.extra" name="extra[]" class="browser-default" style="height: 40px" @change="actualizaTotal()">
                <option value="null">Sin cargo</option>
                <option v-if="p.extra1 != 0" :value="p.extra1">$@{{p.extra1}}</option>
                <option v-if="p.extra2 != 0" :value="p.extra2">$@{{p.extra2}}</option>
                <option v-if="p.extra3 != 0" :value="p.extra3">$@{{p.extra3}}</option>
                <option v-if="p.extra4 != 0" :value="p.extra4">$@{{p.extra4}}</option>
              </select>
            </div>
            <div class="input-field col s2">
              <input name = "product_value[]" type="number" step="any" class="validate" v-model="p.valor_producto" required>
              <label>Valor del equipo</label>
            </div>

            <div class="col s2">
                <label>Numero de serie</label>
                @if($nueva)
                  <select v-model="p.serie" class="browser-default"  style="height: 40px">
                @else
                  <select v-model="p.serie" class="browser-default"  style="height: 40px" >
                @endif

                  <option value="null" >Seleccionar...</option>
                  <option v-for="s in p.serials" :value="s.number">@{{s.number}}</option>
                </select>
            </div>

            <input type="hidden" name="serie[]" :value="p.serie">
            <div class="col s1 center">
              <button type="button" class="btn btn-small red "><i class="material-icons" @click="eliminaProducto(index)">clear</i></button>
            </div>
          </div>
        </div>
        <div style="clear:both"><br></div>
        @can('c_pagos')
        <div class="col m10">
          <h5>Pagos</h5>
        </div>
        <div class="col m2"><br>
        </div>
        <div class="col m12">
          <table class="table highlight bordered">
            <thead>
              <tr>
                <th style="width: 5%; text-align: center">No.</th>
                <th style="width: 10%; text-align: center">Fecha</th>
                <th style="width: 15%; text-align: center">Periodo</th>
                <th style="width: 10%; text-align: center">Cantidad</th>
                <th style="width: 15%; text-align: center">Factura / Remision</th>
                <th style="width: 10%; text-align: center">Unidad</th>
                <th style="width: 20%; text-align: center">Detalles</th>
                @if(!$nueva and!$show)
                @can('v_pagos')<th style="width: 10%; text-align: center">Validado</th>@endcan
                <td colspan="2" style="width: 10%; text-align: center">
                @can('a_pagos')
                <button style="width: 100%" type="button" class="btn btn-small green" name="button" @click="nuevoPago()">NUEVO</button>
                @endcan
                </td>
              @endif
              </tr>
            </thead>
            <tbody>
              <tr v-for="(p, index) in pagos">
                <td>@{{ index + 1 }}</td>
                <td>@{{ p.fecha }}</td>
                <td>@{{p.periodo}}</td>
                <td>$@{{p.cantidad }}</td>
                <td>@{{p.factura }}</td>
                <td>@{{p.unidad }}</td>
                <td>@{{p.notas }}</td>
                @if(!$nueva and!$show)
                @can('v_pagos')
                <td style="padding-top: 20px !important">
                  <input type="checkbox" class="check" name="permisos[]" value="a_usuario" v-model="p.validado" :id="index" @click="validaPago(p.id, p.validado)"/><label :for="index"></label></p>
                </td>
                @endcan
                <td>
                  @can('m_pagos')<button type="button" class="btn btn-small blue" name="button" @click="editarPago(index)">editar</button>@endcan
                </td>
                <td>
                  @can('b_pagos')<button type="button" class="btn btn-small red" name="button" @click="eliminarPago(p.id)">eliminar</button>@endcan
                </td>
              @endif
              </tr>
            </tbody>
          </table>
        </div>
      @endcan
        <div class="col m2">
          <br><br><br>
          @if($show)
          @else
            <button type="submit" name="button" class="btn red">Guardar</button>
          @endif
        </div>
    </form>
</div>
@endsection

@section('scripts')
  <script type="text/javascript">


  var app = new Vue({
    el: '#vue-app',
    data: {
      cliente: '',
      direccion: '',
      telefono: '',
      telefono2: '',
      fecha: '',
      fechaVigencia: '',
      fechaRecoleccion: '',
      observaciones: '',
      conductor_id: 'null',
      conductor_id2: 'null',
      pagos: [],
      clientes: [],
      pago_periodo: '',
      pago_cantidad: '',
      pago_factura: '',
      pago_notas: '',
      pago_unidad: '',
      pago_porpagar: '',
      pago_porpagar_back: '',
      pago_checkbox: true,
      b_editpago: false,
      id_pago: -1,
      cobro: '',
      deposito: '',
      flete: '',
      total: 0,
      porpagar: 0,
      productos: [],
      band_nuevo: true,
      fechaVigencia_show: "",
    },
    mounted: function () {
      this.$nextTick(function () {
        let t = this;
        var date = new Date();
        var input = $('#date').pickadate();
        var picker = input.pickadate('picker');
        picker.set('select', date.toString(), { format: 'yyyy-mm-dd' });

        $('select').material_select();
        $('#vueSelect').on('change', function () {
            t.$emit("change", this.value)
        });
        t.$on("change", function(data){
            this.conductor_id = data
        });
        $('#vueSelect2').on('change', function () {
            t.$emit("change1", this.value)
        });
        t.$on("change1", function(data){
            this.conductor_id2 = data
        });

        @foreach($clients as $client)
          this.clientes.push({
            nombre: '{{$client->name}}',
            direccion: '{{$client->address}}' + " " + '{!!$client->suburb!!}',
            telefono: '{{$client->phone}}',
            telefono2: '{{$client->cellphone}}',
          });
        @endforeach

        @if(!$nueva)
          this.band_nuevo = false;
        @endif

        $(document).ready(function(){

          var clientes = [];
          @foreach($clients as $client)
            var reg = {
              nombre: '{{$client->name}}'
            };
            clientes.push(reg);
          @endforeach

          var cad = "{";
          for(var i=0; i < clientes.length; i++)
            cad += "\"" + (clientes[i].nombre).toString() + "\": null, ";
          cad = cad.substring(0, cad.length-2) + "}";

          if(clientes.length > 0)
          {
            $('input.cliente').autocomplete({
               data : JSON.parse(cad),
               onAutocomplete: function(txt) {
                 for (var i = 0; i < t.clientes.length; i++) {
                   if(txt == t.clientes[i].nombre){
                     t.direccion = '';
                     t.telefono = '';
                     t.telefono2 = '';
                     t.cliente ="";
                     t.cliente = txt;
                     t.direccion = t.clientes[i].direccion;
                     t.telefono = t.clientes[i].telefono;
                     t.telefono2 = t.clientes[i].telefono2;
                     t.updateHTMLElements();
                   }
                 }
              },
               limit: 20,
               minLength: 1,
             });
           }
        });

        t.productos.push({
          producto_id: 'null',
          extra: 'null',
          costo: 0,
          valor_producto: 0,
          serie: 'null',
          extra1: 0,
          extra2: 0,
          extra3: 0,
          extra4: 0
        });
        @if($nueva)

          var date = new Date();
          date.setMonth(date.getMonth()+1);
          var $input = $('#validity').pickadate();
          var picker = $input.pickadate('picker');

          var date1 = date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + "-" +  ('0' + date.getDate()).slice(-2);
          picker.set('select', date1 , { format: 'yyyy-mm-dd' });

        @else
          t.cliente = '{{$contract->client}}';
          t.direccion = '{{$contract->address}}';
          t.telefono = '{{$contract->phone}}';
          t.telefono2 = '{{$contract->cellphone}}';
          t.fecha = '{{$contract->date}}';
          t.fechaVigencia = '{{$contract->validity}}';
          t.fechaVigencia_show = '{{$contract->validity}}'.slice(0,10);
          t.fechaRecoleccion = '{{$contract->recolect_date}}';
          t.total = {{$contract->cost}};
          t.conductor_id = "{{$contract->driver->id or 'null'}}";
          t.conductor_id2 = "{{$contract->driver2->id or 'null'}}";
          t.observaciones = "{{$contract->notes}}";
          t.cobro = "{{$contract->charge}}";
          t.deposito = '{{$contract->deposit}}';
          t.flete = '{{$contract->flete}}';
          t.porpagar = {{$contract->topay or 0}};
          t.productos = [];
          @foreach ($contract->products as $p)
            var reg = {
              producto_id: '{{$p->product_id}}',
              costo: {{$p->cost}},
              extra: '{{$p->extra}}',
              valor_producto: {{$p->product_value}},
              serials: [],
            }

            @foreach ($products as $pr)
            if('{{$pr->id}}' == reg.producto_id){
                reg.extra1 = {{$pr->extraprice1 or 0}};
                reg.extra2 = {{$pr->extraprice2 or 0}};
                reg.extra3 = {{$pr->extraprice3 or 0}};
                reg.extra4 = {{$pr->extraprice4 or 0}};
                @foreach ($serials as $s)
                  if({{$s['id']}} == {{$pr->id}}) {
                    reg.serials.push({
                      number: '{{$s['number']}}'
                    });
                  }
                @endforeach
              }
            @endforeach
            reg.serie = '{{$p->serie or 'null'}}';
            t.productos.push(reg);
          @endforeach

          var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
          this.fechaVigencia_show = new Date(this.fechaVigencia_show);
          this.fechaVigencia_show =  ("0" + this.fechaVigencia_show.getUTCDate()).slice(-2) + " " + monthNames[this.fechaVigencia_show.getUTCMonth()] + ", " + this.fechaVigencia_show.getUTCFullYear();

          this.updateHTMLElements();

          var $input = $('#date').pickadate();
          var picker = $input.pickadate('picker');
          picker.set('select', t.fecha.toString(), { format: 'yyyy-mm-dd' });

          var $input = $('#validity').pickadate();
          var picker = $input.pickadate('picker');
          picker.set('select', t.fechaVigencia.toString(), { format: 'yyyy-mm-dd' });

          if(t.fechaRecoleccion != ''){
            var $input = $('#recolect_date').pickadate();
            var picker = $input.pickadate('picker');
            picker.set('select', t.fechaRecoleccion.toString(), { format: 'yyyy-mm-dd' });
          }

          @foreach ($contract->payments as $p)
            t.pagos.push({
              id: {{$p->id}},
              fecha: '{{$p->date_payment}}'.slice(0, 10),
              periodo: '{{$p->period}}',
              factura: '{{$p->bill}}',
              cantidad: '{{$p->amount}}',
              periodo: '{{$p->period}}',
              notas: '{{$p->notes}}',
              unidad: '{{$p->unity}}',
              validado: {{ $p->validate}},
            });
          @endforeach
        @endif
      });
    },
    watch: {
      cliente: function (newVal) {
        @if($nueva)
        for (var i = 0; i < this.clientes.length; i++) {
          if(newVal == this.clientes[i].nombre){
            this.direccion = '';
            this.telefono = '';
            this.telefono2 = '';
            this.direccion = this.clientes[i].direccion;
            this.telefono = this.clientes[i].telefono;
            this.telefono2 = this.clientes[i].telefono2;
            this.updateHTMLElements();
          }
        }
        @endif
      },
      pago_cantidad: function(newVal){
        let t = this;
        t.pago_porpagar = t.pago_porpagar_back - newVal;
      }
    },
    methods:{
      updateHTMLElements: function(){
        $(document).ready(function(){
          setTimeout(function(){ Materialize.updateTextFields(); }, 10);
          setTimeout(function(){ $('select').material_select(); }, 10);
        });
      },
      agregaProducto: function(){
        this.productos.push({
          producto_id: 'null',
          extra: 'null',
          costo: 0,
          valor_producto: 0,
          serie: 'null',
          extra1: 0,
          extra2: 0,
          extra3: 0,
          extra4: 0,
        });
        this.updateHTMLElements();

      },
      eliminaProducto: function(index){
        if(this.productos.length > 1){
          this.productos.splice(index, 1);
        }
      },
      seleccionaProducto: function(index){
        @foreach ($products as $p)
          if('{{$p->id}}' == this.productos[index].producto_id){
            this.productos[index].costo = {{$p->cost}};
            this.productos[index].extra1 = {{$p->extraprice1 or 0}};
            this.productos[index].extra2 = {{$p->extraprice2 or 0}};
            this.productos[index].extra3 = {{$p->extraprice3 or 0}};
            this.productos[index].extra4 = {{$p->extraprice4 or 0}};
            this.productos[index].valor_producto = {{$p->product_value or 0}};
            this.productos[index].serie = 'null';
            this.productos[index].serials = [];
            @foreach ($serials as $s)
              if({{$p->id}} == {{$s['id']}}){
                this.productos[index].serials.push({
                  number: '{{$s['number']}}'
                });
              }
            @endforeach
          }
        @endforeach
        this.actualizaTotal();
      },
      actualizaTotal: function(){
        this.total = 0;
        for (var i = 0; i < this.productos.length; i++) {
          if(this.productos[i].extra != 'null')
            this.total += (parseFloat(this.productos[i].costo) + parseFloat(this.productos[i].extra));
          else
            this.total += parseFloat(this.productos[i].costo);
        }
      },
      registraPago: function(){
        let t = this;
        @if(!$nueva)
        let url = '{!! url("contract")!!}/{!!$contract->id!!}/addpayment/';
        if(t.pago_periodo == "" || t.pago_cantidad == ""){
          alert("Los campos periodo y cantidad son obligatorios");
          return;
        }
        if(t.pago_factura == "") t.pago_factura = '-';
        if(t.pago_notas == "") t.pago_notas = '-';
        if(t.pago_unidad == "") t.pago_unidad = '-';

        axios.get(url + t.pago_factura + "/" + t.pago_notas + "/" + t.pago_cantidad.toString() + "/" + t.pago_periodo + "/" + t.pago_unidad + "/" + t.pago_porpagar + "/" + t.pago_checkbox)
        .then(function (response) {
          $(document).ready(function(){
            let d = new Date();
            t.pagos.push({
              id:       parseInt(response.data.id),
              fecha:    d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2),
              periodo:  response.data.period,
              factura:  response.data.bill,
              cantidad: response.data.amount,
              notas:    response.data.notes,
              unidad:    response.data.unity,
              validado:   false,
            });
            t.fechaVigencia = response.data.validity.date.substring(0,11);
            var $input = $('#validity').pickadate();
            var picker = $input.pickadate('picker');
            picker.set('select', t.fechaVigencia.toString(), { format: 'yyyy-mm-dd' });
          });
          $('#ModalPagos').modal('close');

          t.fechaVigencia_show = t.fechaVigencia;
          var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
          t.fechaVigencia_show = new Date(t.fechaVigencia_show);
          t.fechaVigencia_show =  ("0" + t.fechaVigencia_show.getUTCDate()).slice(-2) + " " + monthNames[t.fechaVigencia_show.getUTCMonth()] + ", " + t.fechaVigencia_show.getUTCFullYear();

          if(t.pago_porpagar <= 0)
            t.porpagar = 0;
          else
            t.porpagar = t.pago_porpagar;


          t.pago_factura = '';
          t.pago_periodo = '';
          t.pago_cantidad = '';
          t.pago_notas = '';
          t.pago_unidad = '';
          Materialize.toast("Pago agregado correctamente", 4000);

        })
        .catch(function (error) {
            console.log(error);
        });
        @endif
      },
      actualizaPago: function(){
        let t = this;
        @if(!$nueva)
        let url = '{!! url("contract")!!}/{!!$contract->id!!}/updatepayment/';
        if(t.pago_periodo == "" || t.pago_cantidad == ""){
          alert("Los campos periodo y cantidad son obligatorios");
          return;
        }
        if(t.pago_factura == "") t.pago_factura = '-';
        if(t.pago_notas == "") t.pago_notas = '-';
        if(t.pago_unidad == "") t.pago_unidad = '-';

        axios.get(url + t.pago_factura + "/" + t.pago_notas + "/" + t.pago_cantidad.toString() + "/" + t.pago_periodo + "/" + t.pago_unidad + "/" + t.id_pago )
        .then(function (response) {
          $(document).ready(function(){
            t.pagos = [];
            for (var i = 0; i < response.data.length; i++) {
              t.pagos.push({
                id:       parseInt(response.data[i].id),
                fecha:    response.data[i].date_payment,
                periodo:  response.data[i].period,
                factura:  response.data[i].bill,
                cantidad: response.data[i].amount,
                notas:    response.data[i].notes,
                unidad:   response.data[i].unity,
                validado: t.checarValidez(response.data[i].validate),
              });
            }
          });
          $('#ModalPagos').modal('close');
          t.id_pago = -1;
          Materialize.toast("Pago modificado correctamente", 3000);
          location.reload();
        })
        .catch(function (error) {
            console.log(error);
        });
        @endif
      },
      checarValidez: function(band){
        if(band == 'true')
          return true;
        else
          return false;
      },
      editarPago: function(index){
        let t = this;
        t.pago_checkbox = true;
        t.b_editpago = true;
        t.id_pago = t.pagos[index].id;
        t.pago_factura = t.pagos[index].factura;
        t.pago_periodo = t.pagos[index].periodo;
        t.pago_cantidad = t.pagos[index].cantidad;
        t.pago_notas =  t.pagos[index].notas;
        t.pago_unidad = t.pagos[index].unidad;

        $('#ModalPagos').modal('open');
        t.updateHTMLElements();
      },
      eliminarPago: function(id){
        let t = this;
        @if(!$nueva)

        swal({
          title: "Actualizar vigencia",
          text: "¿Desea actualizar la vigencia al mes anterior?",
          buttons: ["No", "Si"],
        })
        .then((willDelete) => {
          if (willDelete) {
            let url = '{!! url("contract")!!}/{!!$contract->id!!}/deletepayment/true/';
            axios.get(url + id)
            .then(function (response) {
              $(document).ready(function(){
                if(t.pagos[t.pagos.length-1].id == id)
                  t.porpagar = 0;
                t.pagos = [];
                for (var i = 0; i < response.data.length; i++) {
                  t.pagos.push({
                    id:       parseInt(response.data[i].id),
                    fecha:    response.data[i].date_payment,
                    periodo:  response.data[i].period,
                    factura:  response.data[i].bill,
                    cantidad: response.data[i].amount,
                    notas:    response.data[i].notes,
                    validado: t.checarValidez(response.data[i].validate),
                  });
                }
              });
              $('#ModalPagos').modal('close');
              Materialize.toast("Pago eliminado correctamente", 3000);
              location.reload();
            })
            .catch(function (error) {
                console.log(error);
            });
          }
          else{
            let url = '{!! url("contract")!!}/{!!$contract->id!!}/deletepayment/false/' + id;
            axios.get(url)
            .then(function (response) {
              $(document).ready(function(){
                if(t.pagos[t.pagos.length-1].id == id)
                  t.porpagar = 0;

                t.pagos = [];
                for (var i = 0; i < response.data.length; i++) {
                  t.pagos.push({
                    id:       parseInt(response.data[i].id),
                    fecha:    response.data[i].date_payment,
                    periodo:  response.data[i].period,
                    factura:  response.data[i].bill,
                    cantidad: response.data[i].amount,
                    notas:    response.data[i].notes,
                    validado: t.checarValidez(response.data[i].validate),
                  });
                }
              });
              $('#ModalPagos').modal('close');
              Materialize.toast("Pago eliminado correctamente", 3000);
              location.reload();

            })
            .catch(function (error) {
                console.log(error);
            });
          }
        });




        @endif
      },
      nuevoPago: function(){
        let t = this;
        t.b_editpago = false;
        t.pago_factura = '';
        t.pago_periodo = '';
        t.pago_unidad = '';
        t.pago_notas = '';
        t.pago_cantidad = 0;
        t.pago_checkbox = true;
        if(t.porpagar == 0){
          for (var i = 0; i < t.productos.length; i++) {
            @foreach ($products as $p)
            if('{{$p->id}}' == t.productos[i].producto_id){
              t.pago_cantidad += {{$p->costmonth}};
            }
            @endforeach
            t.pago_porpagar = 0;
            t.pago_porpagar_back = t.pago_cantidad;
          }
        }else{
          t.pago_cantidad = t.porpagar;
          t.pago_porpagar = 0;
          t.pago_porpagar_back = t.porpagar;
        }
        $('#ModalPagos').modal('open');
        t.updateHTMLElements();
      },
      validaPago: function(id, bandera){
        @if(!$nueva)
        let t = this;
        let message = '';
        swal({
          title: "¿Esta Seguro?",
          text: "Se actualizara el estado del pago",
          buttons: ["Cancelar", "Si"],
        })
        .then((willDelete) => {
          if (willDelete) {
            let url = '{!! url("contract")!!}/{!!$contract->id!!}/statuspayment/';
            axios.get(url + id + "/" + bandera )
            .then(function (response) {
              Materialize.toast("Pago actualizado correctamente", 3000);
            })
            .catch(function (error) {
              console.log(error);
            });
          }
          else{
            for (var i = 0; i < t.pagos.length; i++) {
              if(t.pagos[i].id == id)
                t.pagos[i].validado = bandera;
            }
          }
        });
        @endif
      }
    },
  });
  </script>
@endsection
