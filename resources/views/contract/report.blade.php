<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <title>Equipo en renta</title>
    <style media="screen" type="text/css" >
    body{
      font-family:sans-serif !important;
    }
    table tr td{
      padding-top: 10px !important;
      border-bottom: .5px solid gray;
      padding-left: 0px !important,
      padding-right: 0px !important,
    }
    </style>
  </head>
  <body>
    <div class="container">
      <div style="position: absolute; left: 15cm; top: -.5cm">
        <img src="{{public_path() . "/images/logoPDF.png" }}" alt="Logo" height="70px"><br>
        <img style="padding-left: 5px" src="{{public_path() . "/images/subLogo.png" }}" alt="Logo" height="15px">
      </div>
      <div style="position: absolute; top: .3cm; text-align: center">
          <h4>REPORTE <br>EQUIPOS EN RENTA</h4>
      </div>
      <div style="position: absolute; text-align: center; top: 3cm">
        <table  style="width:100%">
          <thead>
            <tr>
              <th>NOMBRE EQUIPO</th>
              <th style="text-align: center">CANT. EN RENTA</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($arr as $equipo)
            <tr>
              <td>{{$equipo->EQUIPO}}</td>
              <td style="text-align: center">{{$equipo->CONTADOR}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </body>
</html>
