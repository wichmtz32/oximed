@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show contract
    </h1>
    <form method = 'get' action = '{!!url("contract")!!}'>
        <button class = 'btn blue'>contract Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>date : </i></b>
                </td>
                <td>{!!$contract->date!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>validity : </i></b>
                </td>
                <td>{!!$contract->validity!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>description : </i></b>
                </td>
                <td>{!!$contract->description!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>status : </i></b>
                </td>
                <td>{!!$contract->status!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>recolect_date : </i></b>
                </td>
                <td>{!!$contract->recolect_date!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>md : </i></b>
                </td>
                <td>{!!$contract->md!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>cilinder : </i></b>
                </td>
                <td>{!!$contract->cilinder!!}</td>
            </tr>
            <tr>
                <td>
                    <b>
                        <i>name : </i>
                        <b>
                        </td>
                        <td>{!!$contract->client->name!!}</td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <i>address : </i>
                                <b>
                                </td>
                                <td>{!!$contract->client->address!!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>
                                        <i>phone : </i>
                                        <b>
                                        </td>
                                        <td>{!!$contract->client->phone!!}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                <i>email : </i>
                                                <b>
                                                </td>
                                                <td>{!!$contract->client->email!!}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>
                                                        <i>cellphone : </i>
                                                        <b>
                                                        </td>
                                                        <td>{!!$contract->client->cellphone!!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>
                                                                <i>person : </i>
                                                                <b>
                                                                </td>
                                                                <td>{!!$contract->client->person!!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>
                                                                        <i>created_at : </i>
                                                                        <b>
                                                                        </td>
                                                                        <td>{!!$contract->client->created_at!!}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <b>
                                                                                <i>updated_at : </i>
                                                                                <b>
                                                                                </td>
                                                                                <td>{!!$contract->client->updated_at!!}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <b>
                                                                                        <i>deleted_at : </i>
                                                                                        <b>
                                                                                        </td>
                                                                                        <td>{!!$contract->client->deleted_at!!}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>
                                                                                                <i>name : </i>
                                                                                                <b>
                                                                                                </td>
                                                                                                <td>{!!$contract->driver->name!!}</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <b>
                                                                                                        <i>address : </i>
                                                                                                        <b>
                                                                                                        </td>
                                                                                                        <td>{!!$contract->driver->address!!}</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <b>
                                                                                                                <i>schedule : </i>
                                                                                                                <b>
                                                                                                                </td>
                                                                                                                <td>{!!$contract->driver->schedule!!}</td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <b>
                                                                                                                        <i>salary : </i>
                                                                                                                        <b>
                                                                                                                        </td>
                                                                                                                        <td>{!!$contract->driver->salary!!}</td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <b>
                                                                                                                                <i>created_at : </i>
                                                                                                                                <b>
                                                                                                                                </td>
                                                                                                                                <td>{!!$contract->driver->created_at!!}</td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <b>
                                                                                                                                        <i>updated_at : </i>
                                                                                                                                        <b>
                                                                                                                                        </td>
                                                                                                                                        <td>{!!$contract->driver->updated_at!!}</td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td>
                                                                                                                                            <b>
                                                                                                                                                <i>deleted_at : </i>
                                                                                                                                                <b>
                                                                                                                                                </td>
                                                                                                                                                <td>{!!$contract->driver->deleted_at!!}</td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </div>
                                                                                                                                @endsection