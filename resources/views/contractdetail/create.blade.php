@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Create contractdetail
    </h1>
    <form method = 'get' action = '{!!url("contractdetail")!!}'>
        <button class = 'btn blue'>contractdetail Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("contractdetail")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="cost" name = "cost" type="text" class="validate">
            <label for="cost">cost</label>
        </div>
        <div class="input-field col s6">
            <input id="product_value" name = "product_value" type="text" class="validate">
            <label for="product_value">product_value</label>
        </div>
        <div class="input-field col s6">
            <input id="extra" name = "extra" type="text" class="validate">
            <label for="extra">extra</label>
        </div>
        <div class="input-field col s6">
            <input id="serie" name = "serie" type="text" class="validate">
            <label for="serie">serie</label>
        </div>
        <div class="input-field col s12">
            <select name = 'contract_id'>
                @foreach($contracts as $key => $value) 
                <option value="{{$key}}">{{$value}}</option>
                @endforeach 
            </select>
            <label>contracts Select</label>
        </div>
        <div class="input-field col s12">
            <select name = 'product_id'>
                @foreach($products as $key => $value) 
                <option value="{{$key}}">{{$value}}</option>
                @endforeach 
            </select>
            <label>products Select</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection