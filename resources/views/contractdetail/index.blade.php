@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        contractdetail Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("contractdetail")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New contractdetail</button>
        </form>
        <ul id="dropdown" class="dropdown-content">
            <li><a href="http://localhost:8000/contract">Contract</a></li>
            <li><a href="http://localhost:8000/product">Product</a></li>
        </ul>
        <a class="col s3 btn dropdown-button #1e88e5 blue darken-1" href="#!" data-activates="dropdown">Associate<i class="mdi-navigation-arrow-drop-down right"></i></a>
    </div>
    <table>
        <thead>
            <th>cost</th>
            <th>product_value</th>
            <th>extra</th>
            <th>serie</th>
            <th>date</th>
            <th>status</th>
            <th>charge</th>
            <th>serie</th>
            <th>recolect_date</th>
            <th>client</th>
            <th>address</th>
            <th>phone</th>
            <th>cellphone</th>
            <th>notes</th>
            <th>cost</th>
            <th>extra</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th>deleted_at</th>
            <th>name</th>
            <th>cost</th>
            <th>costmonth</th>
            <th>extraprice1</th>
            <th>extraprice2</th>
            <th>extraprice3</th>
            <th>extraprice4</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th>deleted_at</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($contractdetails as $contractdetail) 
            <tr>
                <td>{!!$contractdetail->cost!!}</td>
                <td>{!!$contractdetail->product_value!!}</td>
                <td>{!!$contractdetail->extra!!}</td>
                <td>{!!$contractdetail->serie!!}</td>
                <td>{!!$contractdetail->contract->date!!}</td>
                <td>{!!$contractdetail->contract->status!!}</td>
                <td>{!!$contractdetail->contract->charge!!}</td>
                <td>{!!$contractdetail->contract->serie!!}</td>
                <td>{!!$contractdetail->contract->recolect_date!!}</td>
                <td>{!!$contractdetail->contract->client!!}</td>
                <td>{!!$contractdetail->contract->address!!}</td>
                <td>{!!$contractdetail->contract->phone!!}</td>
                <td>{!!$contractdetail->contract->cellphone!!}</td>
                <td>{!!$contractdetail->contract->notes!!}</td>
                <td>{!!$contractdetail->contract->cost!!}</td>
                <td>{!!$contractdetail->contract->extra!!}</td>
                <td>{!!$contractdetail->contract->created_at!!}</td>
                <td>{!!$contractdetail->contract->updated_at!!}</td>
                <td>{!!$contractdetail->contract->deleted_at!!}</td>
                <td>{!!$contractdetail->product->name!!}</td>
                <td>{!!$contractdetail->product->cost!!}</td>
                <td>{!!$contractdetail->product->costmonth!!}</td>
                <td>{!!$contractdetail->product->extraprice1!!}</td>
                <td>{!!$contractdetail->product->extraprice2!!}</td>
                <td>{!!$contractdetail->product->extraprice3!!}</td>
                <td>{!!$contractdetail->product->extraprice4!!}</td>
                <td>{!!$contractdetail->product->created_at!!}</td>
                <td>{!!$contractdetail->product->updated_at!!}</td>
                <td>{!!$contractdetail->product->deleted_at!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/contractdetail/{!!$contractdetail->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/contractdetail/{!!$contractdetail->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/contractdetail/{!!$contractdetail->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $contractdetails->render() !!}

</div>
@endsection