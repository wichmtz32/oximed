@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit contractdetail
    </h1>
    <form method = 'get' action = '{!!url("contractdetail")!!}'>
        <button class = 'btn blue'>contractdetail Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("contractdetail")!!}/{!!$contractdetail->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="cost" name = "cost" type="text" class="validate" value="{!!$contractdetail->
            cost!!}"> 
            <label for="cost">cost</label>
        </div>
        <div class="input-field col s6">
            <input id="product_value" name = "product_value" type="text" class="validate" value="{!!$contractdetail->
            product_value!!}"> 
            <label for="product_value">product_value</label>
        </div>
        <div class="input-field col s6">
            <input id="extra" name = "extra" type="text" class="validate" value="{!!$contractdetail->
            extra!!}"> 
            <label for="extra">extra</label>
        </div>
        <div class="input-field col s6">
            <input id="serie" name = "serie" type="text" class="validate" value="{!!$contractdetail->
            serie!!}"> 
            <label for="serie">serie</label>
        </div>
        <div class="input-field col s12">
            <select name = 'contract_id'>
                @foreach($contracts as $key => $value) 
                <option value="{{$key}}">{{$value}}</option>
                @endforeach 
            </select>
            <label>contracts Select</label>
        </div>
        <div class="input-field col s12">
            <select name = 'product_id'>
                @foreach($products as $key => $value) 
                <option value="{{$key}}">{{$value}}</option>
                @endforeach 
            </select>
            <label>products Select</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection