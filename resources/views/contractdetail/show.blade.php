@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show contractdetail
    </h1>
    <form method = 'get' action = '{!!url("contractdetail")!!}'>
        <button class = 'btn blue'>contractdetail Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>cost : </i></b>
                </td>
                <td>{!!$contractdetail->cost!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>product_value : </i></b>
                </td>
                <td>{!!$contractdetail->product_value!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>extra : </i></b>
                </td>
                <td>{!!$contractdetail->extra!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>serie : </i></b>
                </td>
                <td>{!!$contractdetail->serie!!}</td>
            </tr>
            <tr>
                <td>
                    <b>
                        <i>date : </i>
                        <b>
                        </td>
                        <td>{!!$contractdetail->contract->date!!}</td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <i>status : </i>
                                <b>
                                </td>
                                <td>{!!$contractdetail->contract->status!!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>
                                        <i>charge : </i>
                                        <b>
                                        </td>
                                        <td>{!!$contractdetail->contract->charge!!}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                <i>serie : </i>
                                                <b>
                                                </td>
                                                <td>{!!$contractdetail->contract->serie!!}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>
                                                        <i>recolect_date : </i>
                                                        <b>
                                                        </td>
                                                        <td>{!!$contractdetail->contract->recolect_date!!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>
                                                                <i>client : </i>
                                                                <b>
                                                                </td>
                                                                <td>{!!$contractdetail->contract->client!!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>
                                                                        <i>address : </i>
                                                                        <b>
                                                                        </td>
                                                                        <td>{!!$contractdetail->contract->address!!}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <b>
                                                                                <i>phone : </i>
                                                                                <b>
                                                                                </td>
                                                                                <td>{!!$contractdetail->contract->phone!!}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <b>
                                                                                        <i>cellphone : </i>
                                                                                        <b>
                                                                                        </td>
                                                                                        <td>{!!$contractdetail->contract->cellphone!!}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>
                                                                                                <i>notes : </i>
                                                                                                <b>
                                                                                                </td>
                                                                                                <td>{!!$contractdetail->contract->notes!!}</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <b>
                                                                                                        <i>cost : </i>
                                                                                                        <b>
                                                                                                        </td>
                                                                                                        <td>{!!$contractdetail->contract->cost!!}</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <b>
                                                                                                                <i>extra : </i>
                                                                                                                <b>
                                                                                                                </td>
                                                                                                                <td>{!!$contractdetail->contract->extra!!}</td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <b>
                                                                                                                        <i>created_at : </i>
                                                                                                                        <b>
                                                                                                                        </td>
                                                                                                                        <td>{!!$contractdetail->contract->created_at!!}</td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <b>
                                                                                                                                <i>updated_at : </i>
                                                                                                                                <b>
                                                                                                                                </td>
                                                                                                                                <td>{!!$contractdetail->contract->updated_at!!}</td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <b>
                                                                                                                                        <i>deleted_at : </i>
                                                                                                                                        <b>
                                                                                                                                        </td>
                                                                                                                                        <td>{!!$contractdetail->contract->deleted_at!!}</td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td>
                                                                                                                                            <b>
                                                                                                                                                <i>name : </i>
                                                                                                                                                <b>
                                                                                                                                                </td>
                                                                                                                                                <td>{!!$contractdetail->product->name!!}</td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <b>
                                                                                                                                                        <i>cost : </i>
                                                                                                                                                        <b>
                                                                                                                                                        </td>
                                                                                                                                                        <td>{!!$contractdetail->product->cost!!}</td>
                                                                                                                                                    </tr>
                                                                                                                                                    <tr>
                                                                                                                                                        <td>
                                                                                                                                                            <b>
                                                                                                                                                                <i>costmonth : </i>
                                                                                                                                                                <b>
                                                                                                                                                                </td>
                                                                                                                                                                <td>{!!$contractdetail->product->costmonth!!}</td>
                                                                                                                                                            </tr>
                                                                                                                                                            <tr>
                                                                                                                                                                <td>
                                                                                                                                                                    <b>
                                                                                                                                                                        <i>extraprice1 : </i>
                                                                                                                                                                        <b>
                                                                                                                                                                        </td>
                                                                                                                                                                        <td>{!!$contractdetail->product->extraprice1!!}</td>
                                                                                                                                                                    </tr>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td>
                                                                                                                                                                            <b>
                                                                                                                                                                                <i>extraprice2 : </i>
                                                                                                                                                                                <b>
                                                                                                                                                                                </td>
                                                                                                                                                                                <td>{!!$contractdetail->product->extraprice2!!}</td>
                                                                                                                                                                            </tr>
                                                                                                                                                                            <tr>
                                                                                                                                                                                <td>
                                                                                                                                                                                    <b>
                                                                                                                                                                                        <i>extraprice3 : </i>
                                                                                                                                                                                        <b>
                                                                                                                                                                                        </td>
                                                                                                                                                                                        <td>{!!$contractdetail->product->extraprice3!!}</td>
                                                                                                                                                                                    </tr>
                                                                                                                                                                                    <tr>
                                                                                                                                                                                        <td>
                                                                                                                                                                                            <b>
                                                                                                                                                                                                <i>extraprice4 : </i>
                                                                                                                                                                                                <b>
                                                                                                                                                                                                </td>
                                                                                                                                                                                                <td>{!!$contractdetail->product->extraprice4!!}</td>
                                                                                                                                                                                            </tr>
                                                                                                                                                                                            <tr>
                                                                                                                                                                                                <td>
                                                                                                                                                                                                    <b>
                                                                                                                                                                                                        <i>created_at : </i>
                                                                                                                                                                                                        <b>
                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        <td>{!!$contractdetail->product->created_at!!}</td>
                                                                                                                                                                                                    </tr>
                                                                                                                                                                                                    <tr>
                                                                                                                                                                                                        <td>
                                                                                                                                                                                                            <b>
                                                                                                                                                                                                                <i>updated_at : </i>
                                                                                                                                                                                                                <b>
                                                                                                                                                                                                                </td>
                                                                                                                                                                                                                <td>{!!$contractdetail->product->updated_at!!}</td>
                                                                                                                                                                                                            </tr>
                                                                                                                                                                                                            <tr>
                                                                                                                                                                                                                <td>
                                                                                                                                                                                                                    <b>
                                                                                                                                                                                                                        <i>deleted_at : </i>
                                                                                                                                                                                                                        <b>
                                                                                                                                                                                                                        </td>
                                                                                                                                                                                                                        <td>{!!$contractdetail->product->deleted_at!!}</td>
                                                                                                                                                                                                                    </tr>
                                                                                                                                                                                                                </tbody>
                                                                                                                                                                                                            </table>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        @endsection