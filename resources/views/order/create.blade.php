@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div id="vue-app">
  @if($nueva)
  <div class="col m10">
      <h3>Nueva orden de reparación</h3>
  </div>
  <div class="col m2"><br>
    <p style="font-size: 20px; font-weight: bold">No.   {{$folio}}</p>
  </div>
  @elseif($show)
    <div class="col m10">
        <h3>Información contrato</h3>
    </div>
    <div class="col m2"><br>
      <p style="font-size: 20px; font-weight: bold">No.   {{$order->folio}}</p>
    </div>
  @else
    <div class="col m10">
        <h3>Editar Orden de  Reparacion</h3>
    </div>
    <div class="col m2"><br>
      <p style="font-size: 20px; font-weight: bold">No.   {{$order->folio}}</p>
    </div>
  @endif
    <br>
    @if($nueva)
    <form method = 'POST' action = '{!!url("order")!!}'>
    @elseif($show)
    @else
    <form method = 'POST' action = '{!! url("order")!!}/{!!$order->id!!}/update'>
    @endif
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="col s12">
          <div class="input-field col s6">
            @if($nueva)
            <input id="date" name = "date" type="text" class="datepicker">
            @else
            <input id="date" name = "date" type="text" class="datepicker" disabled>
            @endif
            <label for="date">Fecha</label>
          </div>
        </div>
        <h5 class="col s10">Datos del cliente</h5>
        <div class="col s12">
          <div class="input-field col s4">
              <input id="client" name = "client" type="text" class="validate cliente" v-model="cliente" required>
              <label for="client">Cliente</label>
          </div>
          <div class="input-field col s4">
              <input id="address" name = "address" type="text" class="validate" v-model="direccion" required>
              <label for="address">Direccion</label>
          </div>
          <div class="input-field col s2">
              <input id="phone" name = "phone" type="text" class="validate" v-model="telefono" required>
              <label for="phone">Telefono</label>
          </div>
          <div class="input-field col s2">
              <input id="cellphone" name = "cellphone" type="text" class="validate" v-model="telefono2" required>
              <label for="cellphone">Celular/Telefono2</label>
          </div>
        </div>
        <div style="clear:both"><br></div>

          <div class=" col s12">
            <div class="input-field col s4">
              <input id="fecha2" v-model="fecha2" name = "fecha2" type="text" class="datepicker">
              <label for="fecha2">Fecha Entrega</label>
            </div>
          </div>
          @verbatim
          <div  class="col s12 "  align="right">
            <h6 align="right" class="col s9">SUBTOTAL</h5>
            <h6 align="left" class="col s3">${{subtotal1 }}</h5>

          </div>
          <div class=" col s12 "  align="right">

            <h6 align="right" class="col s9">IVA 15%</h5>
            <h6 align="left" class="col s3">${{iva}}</h5>

          </div>
          <div  class=" col s12 "  align="right">

            <h6 align="right" class="col s9">TOTAL</h5>
           <h6 align="left" class="col s3">${{total1}}</h5>
          @endverbatim
         </div>
        <div style="clear:both"><br></div>
        <h5 class="col s10">Equipos</h5>
        <div class="col s2 center" >
            <a class="btn btn-small green flat btn modal-trigger"  @click="agregaEquipo()">+Equipo</a>
        </div>
        <div  align="center" class="col m12">
        <ul class="collapsible" data-collapsible="expandable" >
          <li v-for="(e, index) in equipos" :key="index">
            <div class="collapsible-header" ><i class="material-icons">filter_drama</i>Equipo @{{index + 1}}  </div>
            <div class="collapsible-body col m12">
              <h5 align="left" class="col s10">Datos Adquiridos</h5>
              <div class="col s1 center">
                <button  type="button" class="btn btn-small red "><i class="material-icons" @click="eliminaEquipo(index)">clear</i></button>
              </div>

              <div  align="center" class="col m12">
                <div class="col s12">
                  <div class="input-field col s6">
                      <input  id="product[]" name = "product[]" v-model="e.productos" type="text"  required>
                      <label >Equipo</label>
                  </div>
                  <div class="input-field col s6">
                      <input  id="nserie[]" name = "nserie[]" v-model="e.nserie" type="text"  required>
                      <label >Serie</label>
                  </div>
                </div>
                <div class="col s12">
                  <div class="input-field col s6">
                      <textarea id="faults[]" name = "faults[]" v-model="e.fallas"  rows="4" cols="50">
                      </textarea>
                      <label>Fallas que presenta</label>
                  </div>
                  <div class="input-field col s6">
                      <input  id="reparador[]" name = "reparador[]" v-model="e.reparador" type="text"  required>
                      <label >Reparador</label>
                  </div>
                  <div class="col s3">
                    <label >Estatus</label>
                      <select v-model="e.estatus"  class="browser-default"  id="estatus" style="height: 40px">
                       <option value="En espera">En Espera</option>
                       <option value="Terminada">Terminada</option>
                       <option value="Cancelada">Cancelada</option>
                       <option value="En proceso">En proceso</option>
                     </select>
                     <input   type="hidden"  name="estatus[]"   :value="e.estatus">
                  </div>
                  <div class="col s3">
                    <label >Tipo</label>
                      <select v-model="e.tipo"  class="browser-default"  id="tipo" style="height: 40px">
                       <option value="Garantia">Garantia</option>
                       <option value="Reparacion">Reparacion</option>
                       <option value="Mantenimiento">Mantenimiento</option>
                     </select>
                     <input   type="hidden"  name="tipos[]"   :value="e.tipo">
                  </div>
                  <div class="input-field col s6">
                      <input  id="ppago[]" name = "ppago[]" v-model="e.ppago"   type="text" step="any" class="validate">
                      <label >Pago a reparador</label>
                  </div>
                </div>
            </div>
             <h5 align="left" class="col s10">Reparaciones </h5>
                 <div class="col s2 center" >
                   <button type="button" class="btn btn-small green  flat" @click="agregaReparacion(index)">+Reparacion</button>
                 </div>
                 <div class="col s12" v-for="(r, index2) in equipos[index].reparaciones">
                   <div class="input-field col s7">
                     <input   v-model="r.descripcion" :name="'descripcion['+index+'][]'" id="r.descripcion"  type="text"  required>
                       <label >Descripcion</label>
                   </div>

                   <div class="input-field col s2">
                     <input :name="'precio['+index+'][]'" v-model="r.precio"   id="precio" type="number" step="any" class="validate"  @input="actualizaTotal2(index,index2)" required>
                     <label>Precio</label>
                   </div>

                   <input type="hidden" :name="'total['+index+'][]'":value="r.total">
                   <div class="col s1 center">
                     <button type="button" class="btn btn-small red "><i class="material-icons" @click="eliminaReparacion(index,index2)">clear</i></button>
                   </div>
                   <input  type="hidden" name="total" :value="e.total">
                 </div>
                 <div>
                  <h5 align="left" class="col s10">Observaciones</h5>
                  <div class="col s2 center" >
                      <a class="btn btn-small green flat btn modal-trigger" @click="modal1(index)">+Observacion</a>
                  </div>
                  <div class="col s12" v-for="(o, index3) in e.observaciones">
                    <div class="input-field col s8">
                        <input  v-model="o.descripcion" :name="'descripcion0['+index+'][]'"  id="o.descripcionO"  type="text" disabled>
                        <input   type="hidden"  v-model="o.descripcion"  :name="'descripcion1['+index+'][]'" id="o.descripcion1"  type="text" >
                    </div>
                    <div class="input-field col s2">
                      <input :id='index + "t" +index3'  :name="'fechas3['+index+'][]'"  v-model="o.fecha"  type="text" class="datepicker" disabled>
                      <input :id='index + "t" +index3'  type="hidden"    :name="'fechas4['+index+'][]'"  v-model="o.fecha">
                    </div>
                     <div class="col s1 center">
                      <button type="button" class="btn btn-small blue "><i class="material-icons" @click="editaObservacion(index,index3)">edit</i></button>
                    </div>
                    <div class="col s1 center">
                      <button  type="button" class="btn btn-small red "><i class="material-icons" @click="eliminaObservacion(index,index3)">clear</i></button>
                    </div>
                  </div>
                </div>
               </div>
              </div>
          </li>
        </ul>
        <div  align="center" class="col m12">
          <br><br><br>
          @if($show)
          @else
            <button type="submit" name="button" class="btn red" @click="faltanCampos">Guardar</button>
          @endif
        </div>
    </form>
<!-- Modal Observacion -->
<div id="modal11" class="modal bottom-sheet">
  <div class="modal-content">
    <h4>Agregar Observacion</h4>
    <div>
      <h5 class="col s10"></h5>
        <div class="input-field col s8">
            <input  v-model="descripcion1" name="descripcionO[]" id="descripcionO"  type="text" required>
            <label >Descripcion</label>
        </div>
        <div class="input-field col s2">
          <input id='fecha8' name = "fecha8" v-model="fecha8"  type="text" class="datepicker" required >
          <label for="fecha8">Fecha</label>
        </div>
  </div>
  <div class="modal-footer">
    <a  class="btn btn-small green  flat" @click="agregaObservacion2()">Aceptar</a>
    <a  class="btn btn-small red  flat" @click="cierraModal()">Cancelar</a>
  </div>
</div>
</div>
<div id="modal2" class="modal bottom-sheet">
  <div class="modal-content">
    <h4>Agregar Observacion</h4>
    <div>
      <h5 class="col s10"></h5>
        <div class="input-field col s8">
            <input  v-model="descripcion1" name="descripcionO[]" id="descripcionO"  type="text" required>
            <label >Descripcion</label>
        </div>
        <div class="input-field col s2">
          <input id='fecha9' name = "fecha9" v-model="fecha9"  type="text" class="datepicker" required >
          <label for="fecha9">Fecha</label>
        </div>
  </div>
  <div class="modal-footer">
    <a class="btn btn-small green  flat" @click="editaObservacion2()">Guardar</a>
    <a class="btn btn-small red  flat" @click="cierraModal()">Cancelar</a>
  </div>
</div>
</div>
</div>
@endsection
@section('scripts')
  <script type="text/javascript">

  var app = new Vue({
    el: '#vue-app',
    data: {
      descripcion1: "",
      cliente: '',
      direccion: '',
      telefono: '',
      telefono2: '',
      fecha: '',
      fecha9: '',
      fecha8: '',
      fechaVigencia: '',
      fechaRecoleccion: '',
      observaciones: [],
      equipo:'',
      fallas:'',
      fechas2: [],
      fecha2:'',
      estatus:[],
      nserie:'',
      clientes: [],
      total: 0,
      productos: [],
      reparador:'',
      reparaciones: [],
      band_nuevo: true,
      activa:-1,
      products:[],
      nserie:[],
      equipos:[],
      fallas:[],
      reparador:[],
      ppago:[],
      index33:-1,
    },
    mounted: function () {
      this.$nextTick(function () {
        let t = this;
        var date = new Date();
        var input = $('#date').pickadate();
        var picker = input.pickadate('picker');
        picker.set('select', date.toString(), { format: 'yyyy-mm-dd' });
        $('select').material_select();
        $('#vueSelect').on('change', function () {
            t.$emit("change", this.value)
        });
        $('#vueSelect2').on('change', function () {
            t.$emit("change1", this.value)
        });
        @foreach($clients as $client)
          this.clientes.push({
            nombre: '{{$client->name}}',
            direccion: '{{$client->address}}' + " " + '{!!$client->suburb!!}',
            telefono: '{{$client->phone}}',
            telefono2: '{{$client->cellphone}}',
          });
        @endforeach

        @if(!$nueva)
          this.band_nuevo = false;
        @endif
        $(document).ready(function(){
          $('.collapsible').collapsible({
              accordion: false,
          });

          var clientes = [];
          @foreach($clients as $client)
            var reg = {
              nombre: '{{$client->name}}'
            };
            clientes.push(reg);
          @endforeach
          var cad = "{";
          for(var i=0; i < clientes.length; i++)
            cad += "\"" + (clientes[i].nombre).toString() + "\": null, ";
          cad = cad.substring(0, cad.length-2) + "}";
          if(clientes.length > 0)
          {
            $('input.cliente').autocomplete({
               data : JSON.parse(cad),
               onAutocomplete: function(txt) {
                 for (var i = 0; i < t.clientes.length; i++) {
                   if(txt == t.clientes[i].nombre){
                     t.direccion = '';
                     t.telefono = '';
                     t.telefono2 = '';
                     t.cliente ="";
                     t.cliente = txt;
                     t.direccion = t.clientes[i].direccion;
                     t.telefono = t.clientes[i].telefono;
                     t.telefono2 = t.clientes[i].telefono2;
                     t.updateHTMLElements();
                   }
                 }
              },
               limit: 20,
               minLength: 1,
             });
           }
        });
        @if($nueva)
          var date = new Date();
          date.setMonth(date.getMonth());
          var date1 = date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + "-" +  ('0' + date.getDate()).slice(-2);
          picker.set('select', date1 , { format: 'yyyy-mm-dd' });
        @else
          t.cliente = '{{$order->client}}';
          t.direccion = '{{$order->address}}';
          t.telefono = '{{$order->phone}}';
          t.telefono2 = '{{$order->cellphone}}';
          t.fecha = '{{$order->date}}';

          @if($order->deliverDate)
            t.fecha2 = '{{$order->deliverDate}}';
            $(document).ready(function(){
              var input = $('#fecha2').pickadate();
              var picker = input.pickadate('picker');
              picker.set('select', '{{$order->deliverDate}}', { format: 'yyyy-mm-dd' });
              t.fecha2 =  $('#fecha2').val();
            });
          @endif
          this.updateHTMLElements();
          var $input = $('#date').pickadate();
          var picker = $input.pickadate('picker');
          picker.set('select', t.fecha.toString(), { format: 'yyyy-mm-dd' });
          @foreach ($order->reparaciones as $indets=>$repas)
            var arreglito =
            {
              descripcion2:'{{$repas->description}}',
              reparaciones:[],
              observaciones:[],
              estatus:'En Espera',
              total:'',
              productos:'{{$repas->product}}',
              nserie:'{{$repas->serialNumber}}',
              fallas:'{{$repas->faults}}',
              reparador:'{{$repas->repairman}}',
              estatus:'{{$repas->status}}',
              tipo:'{{$repas->tipo}}',
              ppago:'',
            }
            @foreach ($repas->detalles as $det)
              arreglito.reparaciones.push({
                reparacion_id:'null',
                estatus: '{{$det->status}}',
                cantidad: 0,
                precio:{{$det->unitPrice}},
                descripcion:'{{$det->description}}',
                total: 0,
            });
            arreglito.total += {{$det->unitPrice}};
            @endforeach
            @foreach($repas->observaciones as $index=>$o)
              arreglito.observaciones.push({
                id: {{$o->id}},
                descripcion: '{{$o->description}}',
                fecha:  '{{$o->fecha}}',
              });
            @endforeach
          this.equipos.push(arreglito);
          @endforeach
          @foreach ($order->reparaciones as $indets=>$repas)
            @foreach($repas->observaciones as $index=>$o)
              $(document).ready(function(){
                var $input = $('#{{$indets}}t{{$index}}').pickadate();
                var picker = $input.pickadate('picker');
                picker.set('select','{{$o->fecha}}'.slice(0, 10).toString(), { format: 'yyyy-mm-dd' });
                t.equipos[{{$indets}}].observaciones[{{$index}}].fecha =  $('#{{$indets}}t{{$index}}').val();
              });
            @endforeach
          @endforeach
        @endif
      });
    },
    watch: {
      cliente: function (newVal) {
        @if($nueva)
        for (var i = 0; i < this.clientes.length; i++) {
          if(newVal == this.clientes[i].nombre){
            this.direccion = '';
            this.telefono = '';
            this.telefono2 = '';
            this.direccion = this.clientes[i].direccion;
            this.telefono = this.clientes[i].telefono;
            this.telefono2 = this.clientes[i].telefono2;
            this.updateHTMLElements();
          }
        }
        @endif
      },
    },
    computed: {
      total1: function () {
        let total = 0;
        for(var j = 0; j<this.equipos.length;j++)
        {
          total +=parseFloat(this.equipos[j].total);
        }

          return  (total*1.16).toFixed(2);
      },
      subtotal1: function () {
        let total = 0;
        for(var j = 0; j<this.equipos.length;j++)
          total +=parseFloat(this.equipos[j].total);
          return  (total*1).toFixed(2);
      },
      iva: function () {
        let total = 0;
        for(var j = 0; j<this.equipos.length;j++)
          total +=parseFloat(this.equipos[j].total);
          return  (total*.16).toFixed(2);
      }
    },
    methods:{
      updateHTMLElements: function(){
        $(document).ready(function(){
          setTimeout(function(){ Materialize.updateTextFields(); }, 10);
          setTimeout(function(){ $('select').material_select(); }, 10);
        });
      },
      upDate: function(index){
          setTimeout(function(){ $('#' + index).pickadate({selectMonths: true,selectYears: 15,today: 'Today',clear: 'Clear',close: 'Ok',closeOnSelect: true});}, 1000);
      },
      agregaReparacion: function(i){
          this.equipos[i].reparaciones.push({
          reparacion_id: 'null',
          estatus: '',
          cantidad: 1,
          precio: 0,
          total: 0,
        });
       this.updateHTMLElements();
      },
      eliminaReparacion: function(index,index2){
        if(this.equipos[index].reparaciones.length > 0){
          this.equipos[index].reparaciones.splice(index2, 1);
        }
      },
      eliminaEquipo: function(index){
        if(this.equipos.length> 0){
          this.equipos.splice(index, 1);
        }
      },
      agregaObservacion: function(){

        this.observaciones.push({
          descripcion: '',
          fecha: '',
          });
          this.upDate((this.observaciones.length-1).toString());
      },
      agregaEquipo: function(){
        this.equipos.push({
          descripcion2:[],
          reparaciones:[],
          observaciones:[],
          estatus:'En Espera',
          total:0,
          productos:[],
          nserie:[],
          fallas:[],
          reparador:[],
          ppago:'',
          tipo:'',
        });
      },
      totales: function(im)
      {
        let tots = 0;
        for (var i = 0; i < this.equipos[im].reparaciones.length; i++){
          tots+=this.equipos[im].reparaciones[i].total;
        }
        this.equipos[im].total =tots;
      },
      agregaObservacion2: function(){
        if(this.descripcion1!='' &&  $("#fecha8").val()!='')
        {
          this.equipos[this.indice33].observaciones.push({
            descripcion: this.descripcion1,
            fecha: $("#fecha8").val(),
          });

            $('#modal11').modal('close');
            this.descripcion1='';
            this.fecha8 = '';
            this.indice33 = -1;
        }
        else {
          alert("Faltan campos");
        }
      },
      modal1: function(indets)
      {
        this.indice33 = indets;
        $('#modal11').modal('open');
      },
      editaObservacion2: function(){
        if(this.descripcion1!='' &&  $("#fecha9").val()!='')
        {
          this.equipos[this.indice33].observaciones[this.activa].descripcion = this.descripcion1;
          this.equipos[this.indice33].observaciones[this.activa].fecha =  $("#fecha9").val();
          $('#modal2').modal('close');
          this.descripcion1='';
          this.fecha9 = '';
          this.activa = -1;
          this.indice33 = -1;
          this.updateHTMLElements();
        }
        else {
          alert("Faltan campos");
        }
      },
      cierraModal: function(){
        $('#modal11').modal('close');
        $('#modal2').modal('close');
        this.descripcion1='';
        this.fecha8 = '';
      },
      eliminaObservacion: function(index,index2){
        if(this.equipos[index].observaciones.length > 0){
          this.equipos[index].observaciones.splice(index2, 1);
        }
      },
      editaObservacion: function(index,index2){

         let t=this;
         t.descripcion1=t.equipos[index].observaciones[index2].descripcion;
         t.activa = index2;
         t.updateHTMLElements();
         t.indice33 = index;
         t.fecha9= t.equipos[index].observaciones[index2].fecha;
         $('#modal2').modal('open');
      },
      agregaProducto: function(){
        this.productos.push({
          producto_id: 'null',
          extra: 'null',
          costo: 0,
          valor_producto: 0,
          serie: 'null',
          extra1: 0,
          extra2: 0,
          extra3: 0,
          extra4: 0,
        });
        this.updateHTMLElements();
      },
      eliminaProducto: function(index){
        if(this.productos.length > 0){
          this.productos.splice(index, 1);
        }
      },
      actualizaTotal2: function(i,i2){
          if(this.equipos[i].reparaciones[i2].precio<0)
            this.equipos[i].reparaciones[i2].precio = 0;
        this.equipos[i].reparaciones[i2].total = this.equipos[i].reparaciones[i2].precio * 1;
        this.totales(i);
      },
      faltanCampos: function(val) {
        Materialize.toast("En caso de no cargar a la siguiente página, verificar campos.",5000);
      },
    },
  });
  </script>
@endsection
