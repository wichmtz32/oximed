@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show order
    </h1>
    <form method = 'get' action = '{!!url("order")!!}'>
        <button class = 'btn blue'>order Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>client : </i></b>
                </td>
                <td>{!!$order->client!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>address : </i></b>
                </td>
                <td>{!!$order->address!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>phone : </i></b>
                </td>
                <td>{!!$order->phone!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>cellphone : </i></b>
                </td>
                <td>{!!$order->cellphone!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>date : </i></b>
                </td>
                <td>{!!$order->date!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>deliverDate : </i></b>
                </td>
                <td>{!!$order->deliverDate!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection