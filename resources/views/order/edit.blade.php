@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit order
    </h1>
    <form method = 'get' action = '{!!url("order")!!}'>
        <button class = 'btn blue'>order Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("order")!!}/{!!$order->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="client" name = "client" type="text" class="validate" value="{!!$order->
            client!!}"> 
            <label for="client">client</label>
        </div>
        <div class="input-field col s6">
            <input id="address" name = "address" type="text" class="validate" value="{!!$order->
            address!!}"> 
            <label for="address">address</label>
        </div>
        <div class="input-field col s6">
            <input id="phone" name = "phone" type="text" class="validate" value="{!!$order->
            phone!!}"> 
            <label for="phone">phone</label>
        </div>
        <div class="input-field col s6">
            <input id="cellphone" name = "cellphone" type="text" class="validate" value="{!!$order->
            cellphone!!}"> 
            <label for="cellphone">cellphone</label>
        </div>
        <div class="input-field col s6">
            <input id="date" name = "date" type="text" class="validate" value="{!!$order->
            date!!}"> 
            <label for="date">date</label>
        </div>
        <div class="input-field col s6">
            <input id="deliverDate" name = "deliverDate" type="text" class="validate" value="{!!$order->
            deliverDate!!}"> 
            <label for="deliverDate">deliverDate</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection