@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div  id="vue-app" class = 'container'>
    <h1>
        Ordenes de reparacion
    </h1>
    <div  class="row">

        <form class = 'col s3' method = 'get' action = '{!!url("order")!!}/create'>
            <button class = 'btn red' type = 'submit'>+ORDEN</button>
        </form>
    </div>
    <table id="tabla">
        <thead>
            <th>Cliente</th>
            <th>Folio</th>
            <th>Telefono</th>
            <th>Celular</th>
            <th>Fecha</th>
            <th>Acciones</th>
        </thead>
        <tbody>
              <tr v-for="(o, index3) in ordenes" v-bind:style="{'background-color':colores[index3]}">
                <td>@{{o.nombre}}</td>
                <td>@{{o.folio}}</td>
                <td>@{{o.telefono}}</td>
                <td>@{{o.celular}}</td>
                <td>@{{o.fecha}}</td>

                <td>
                    <div  class = 'row'>
                      <a :href = 'o.edit' class = 'viewEdit btn-floating blue' ><i class = 'material-icons'>edit</i></a>
                      <a href = '#modal1' class = 'delete btn-floating modal-trigger red' :data-link = "o.delete" ><i class = 'material-icons'>delete</i></a>

                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</div>
@endsection


@section('scripts')
<script>

</script>


<script type="text/javascript">

var app = new Vue({
  el: '#vue-app',
  data: {
    colores: [],
    color:"red",
    ordenes:[],

  },
  mounted: function () {
    let t = this;

    $(document).ready(function(){
      $('.tooltipped').tooltip({delay: 50});
    });

    $(document).ready(function() {
      $('#tabla').dataTable( {

        "paging":   false,
        "order": [[4,'asc']],
        "info":     true,
          "language": {
              "sProcessing":     "Procesando...",
              "sLengthMenu":     "Mostrar _MENU_ registros",
              "sZeroRecords":    "No se encontraron resultados",
              "sEmptyTable":     "Ningún dato disponible",
              "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":    "",
              "sSearch":         "Buscar:",
              "sUrl":            "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }

    } ).on( 'draw.dt', function () {
    $('.modal').modal();

    } );
    } );


    @foreach($orders as $o)
      this.ordenes.push({
        id:'{{$o->id}}',
        nombre: '{{$o->client}}',
        folio: '{{$o->folio}}',
        telefono: '{{$o->phone}}',
        celular: '{{$o->cellphone}}',
        fecha: '{{$o->date}}',
        edit: '/order/{{$o->id}}/edit',
        delete: '/order/{{$o->id}}/deleteMsg'

      });
    @endforeach

    @foreach($orders as $index=>$o)
        this.colores.push("#32E107");
    @endforeach

    t.asignacolor("En proceso","yellow");
    t.asignacolor("Cancelada","gray");
    t.asignacolor("En espera","red");


  },
  methods:{
    asignacolor: function(estatuss,ccolor){
      @foreach($orders as $index=>$o)
        @foreach($o->reparaciones as $r)
            if('{{$r->status }}'== estatuss)
              this.colores[{{$index}}] = ccolor;
         @endforeach
      @endforeach

    },


  },
});
</script>

@endsection
