@extends('scaffold-interface.layouts.defaultMaterialize')


@section('content')
<div class="container">
      <br><br>
      <h3>Iniciar sesión</h3>
      <form class="form-horizontal" method="POST" action="{{ route('login') }}">
          {{ csrf_field() }}
          <div class="form-group">
              <label for="email" class="control-label">Correo electronico</label>
              <div>
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong style="color:red">Los datos ingresados no son correctos</strong>
                      </span>
                  @endif
              </div>
          </div>
          <br>
          <div class="form-group">
              <label for="password" class="control-label">Contraeña</label>
              <div class="">
                  <input id="password" type="password" class="form-control" name="password" required>
                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong style="color:red">La contraseña no es correcta</strong>
                      </span>
                  @endif
              </div>
          </div>
          <br>
          <div>
              <div>
                  <button type="submit" class="btn">Entrar</button>
                  <a class="btn btn-link" href="{{ route('password.request') }}">
                      ¿Olvidaste tu contraseña?
                  </a>
              </div>
          </div>
      </form>
</div>
@endsection
