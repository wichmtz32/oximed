@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit person
    </h1>
    <form method = 'get' action = '{!!url("person")!!}'>
        <button class = 'btn blue'>Regresar</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("person")!!}/{!!$person->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="name" name = "name" type="text" class="validate" value="{!!$person->
            name!!}"> 
            <label for="name">name</label>
        </div>
        <button class = 'btn red' type ='submit'>Actualizar</button>
    </form>
</div>
@endsection