@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        Usuario Facturas
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("person")!!}/create'>
            <button class = 'btn red' type = 'submit'>Nuevo</button>
        </form>
    </div>
    <table>
        <thead>
            <th>Nombre</th>
        </thead>
        <tbody>
            @foreach($people as $person) 
            <tr>
                <td>{!!$person->name!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/person/{!!$person->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/person/{!!$person->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/person/{!!$person->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $people->render() !!}

</div>
@endsection