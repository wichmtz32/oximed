@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <form method = 'get' action = '{!!url("person")!!}'>
        <button class = 'btn blue'>Regresar</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("person")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="name" name = "name" type="text" class="validate">
            <label for="name">name</label>
        </div>
        <button class = 'btn red' type ='submit'>Crear</button>
    </form>
</div>
@endsection