@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show person
    </h1>
    <form method = 'get' action = '{!!url("person")!!}'>
        <button class = 'btn blue'>Regresar</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>name : </i></b>
                </td>
                <td>{!!$person->name!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection