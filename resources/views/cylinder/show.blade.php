@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show cylinder
    </h1>
    <form method = 'get' action = '{!!url("cylinder")!!}'>
        <button class = 'btn blue'>cylinder Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>sizeOrWeight : </i></b>
                </td>
                <td>{!!$cylinder->sizeOrWeight!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>description : </i></b>
                </td>
                <td>{!!$cylinder->description!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>quantity : </i></b>
                </td>
                <td>{!!$cylinder->quantity!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection