@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit cylinder
    </h1>
    <form method = 'get' action = '{!!url("cylinder")!!}'>
        <button class = 'btn blue'>cylinder Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("cylinder")!!}/{!!$cylinder->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="sizeOrWeight" name = "sizeOrWeight" type="text" class="validate" value="{!!$cylinder->
            sizeOrWeight!!}"> 
            <label for="sizeOrWeight">sizeOrWeight</label>
        </div>
        <div class="input-field col s6">
            <input id="description" name = "description" type="text" class="validate" value="{!!$cylinder->
            description!!}"> 
            <label for="description">description</label>
        </div>
        <div class="input-field col s6">
            <input id="quantity" name = "quantity" type="text" class="validate" value="{!!$cylinder->
            quantity!!}"> 
            <label for="quantity">quantity</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection