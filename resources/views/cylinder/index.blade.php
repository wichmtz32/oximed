@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        cylinder Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("cylinder")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New cylinder</button>
        </form>
    </div>
    <table>
        <thead>
            <th>sizeOrWeight</th>
            <th>description</th>
            <th>quantity</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($cylinders as $cylinder) 
            <tr>
                <td>{!!$cylinder->sizeOrWeight!!}</td>
                <td>{!!$cylinder->description!!}</td>
                <td>{!!$cylinder->quantity!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/cylinder/{!!$cylinder->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/cylinder/{!!$cylinder->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/cylinder/{!!$cylinder->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $cylinders->render() !!}

</div>
@endsection