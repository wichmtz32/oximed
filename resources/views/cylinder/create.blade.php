@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Create cylinder
    </h1>
    <form method = 'get' action = '{!!url("cylinder")!!}'>
        <button class = 'btn blue'>cylinder Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("cylinder")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="sizeOrWeight" name = "sizeOrWeight" type="text" class="validate">
            <label for="sizeOrWeight">sizeOrWeight</label>
        </div>
        <div class="input-field col s6">
            <input id="description" name = "description" type="text" class="validate">
            <label for="description">description</label>
        </div>
        <div class="input-field col s6">
            <input id="quantity" name = "quantity" type="text" class="validate">
            <label for="quantity">quantity</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection