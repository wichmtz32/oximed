<?php

use Faker\Generator as Faker;

$factory->define(App\Driver::class, function (Faker $faker) {
    return [
      'name'  => $faker->name,
      'address' => $faker->streetAddress,
      'schedule' => $faker->dayOfWeek($max = 'now'),
      'salary' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 5),
      'phone' => $faker->phoneNumber,
      'cellphone' => $faker->phoneNumber,
      'unity' => $faker->randomDigitNotNull,
    ];
});
