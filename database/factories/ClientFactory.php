<?php

use Faker\Generator as Faker;

$factory->define(App\Client::class, function (Faker $faker) {
    return [
      'name' => $faker->name,
      'address' => $faker->streetAddress,
      'suburb'=> $faker->streetName,
      'cp' => $faker->postcode,
      'phone' => $faker->phoneNumber,
      'cellphone'=> $faker->phoneNumber,
      'person' => 'fisica',
      'rfc' => str_random(15),
      'email' => $faker->unique()->safeEmail
    ];
});
