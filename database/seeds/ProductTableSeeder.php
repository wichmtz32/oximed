<?php

use Illuminate\Database\Seeder;
use App\Product;



class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $product = new Product();
      $product->name = "KIT OXI MED 415 LTS.";
      $product->cost = 900;
      $product->costmonth = 350;
      $product->extraprice1 = 300;
      $product->extraprice2 = 500;
      $product->extraprice3 = 700;
      $product->extraprice4 = 1000;
      $product->product_value = 0;
      $product->save();

      $product = new Product();
      $product->name = "KIT OXI MED 682 LTS.";
      $product->cost = 900;
      $product->costmonth = 350;
      $product->extraprice1 = 300;
      $product->extraprice2 = 500;
      $product->extraprice3 = 700;
      $product->extraprice4 = 1000;
      $product->product_value = 0;
      $product->save();

      $product = new Product();
      $product->name = "KIT OXI MED 1725 LTS.";
      $product->cost = 900;
      $product->costmonth = 350;
      $product->extraprice1 = 300;
      $product->extraprice2 = 500;
      $product->extraprice3 = 700;
      $product->extraprice4 = 1000;
      $product->product_value = 0;
      $product->save();

      $product = new Product();
      $product->name = "KIT OXI MED 8.5 M3";
      $product->cost = 900;
      $product->costmonth = 200;
      $product->extraprice1 = 300;
      $product->extraprice2 = 500;
      $product->extraprice3 = 700;
      $product->extraprice4 = 1000;
      $product->product_value = 0;
      $product->save();

      $product = new Product();
      $product->name = "CONCENTRADOR OXI MED 5 LTS.";
      $product->cost = 1800;
      $product->costmonth = 1800;
      $product->extraprice1 = 200;
      $product->extraprice2 = 300;
      $product->extraprice3 = 500;
      $product->extraprice4 = 1000;
      $product->product_value = 0;
      $product->save();

      $product = new Product();
      $product->name = "CONCENTRADOR OXI MED 10 LTS.";
      $product->cost = 2600;
      $product->costmonth = 2600;
      $product->extraprice1 = 200;
      $product->extraprice2 = 300;
      $product->extraprice3 = 500;
      $product->extraprice4 = 1000;
      $product->product_value = 0;
      $product->save();

      $product = new Product();
      $product->name = "CONCENTRADOR OXI MED PORTATIL";
      $product->cost = 3500;
      $product->costmonth = 3500;
      $product->extraprice1 = 200;
      $product->extraprice2 = 300;
      $product->extraprice3 = 500;
      $product->extraprice4 = 1000;
      $product->product_value = 0;
      $product->save();

      $product = new Product();
      $product->name = "ASPIRADOR DE FLEMAS";
      $product->cost = 800;
      $product->costmonth = 800;
      $product->extraprice1 = 200;
      $product->extraprice2 = 300;
      $product->extraprice3 = 500;
      $product->extraprice4 = 1000;
      $product->product_value = 0;
      $product->save();


      $product = new Product();
      $product->name = "BPAP";
      $product->cost = 1600;
      $product->costmonth = 1600;
      $product->extraprice1 = 200;
      $product->extraprice2 = 300;
      $product->extraprice3 = 500;
      $product->extraprice4 = 1000;
      $product->product_value = 0;
      $product->save();

      $product = new Product();
      $product->name = "CPAP";
      $product->cost = 1600;
      $product->costmonth = 1600;
      $product->extraprice1 = 200;
      $product->extraprice2 = 300;
      $product->extraprice3 = 500;
      $product->extraprice4 = 1000;
      $product->product_value = 0;
      $product->save();

      $product = new Product();
      $product->name = "SILLA DE RUEDAS";
      $product->cost = 700;
      $product->costmonth = 700;
      $product->extraprice1 = 200;
      $product->extraprice2 = 300;
      $product->extraprice3 = 500;
      $product->extraprice4 = 1000;
      $product->product_value = 0;
      $product->save();

      $product = new Product();
      $product->name = "CAMA MANUAL";
      $product->cost = 800;
      $product->costmonth = 800;
      $product->costmonth = 350;
      $product->extraprice1 = 500;
      $product->extraprice2 = 1000;
      $product->product_value = 0;
      $product->save();

      $product = new Product();
      $product->name = "CAMA ELECTRICA";
      $product->cost = 1700;
      $product->costmonth = 1700;
      $product->extraprice1 = 500;
      $product->extraprice2 = 1000;
      $product->product_value = 0;
      $product->save();
    }
}
