<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $role = Role::create(['name' => 'ADMINISTRADOR']);
      $role->givePermissionTo(
        'a_usuario',
        'b_usuario',
        'm_usuario',
        'c_usuario',
        'a_cliente',
        'b_cliente',
        'm_cliente',
        'c_cliente',
        'a_role',
        'b_role',
        'm_role',
        'c_role',
        'a_contrato',
        'b_contrato',
        'm_contrato',
        'c_contrato',
        'a_conductor',
        'b_conductor',
        'm_conductor',
        'c_conductor',
        'a_producto',
        'b_producto',
        'm_producto',
        'c_producto',
        'a_pagos',
        'b_pagos',
        'm_pagos',
        'c_pagos',
        'v_pagos'
      );
    }
}
