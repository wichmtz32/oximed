<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user = new User();
      $user->name = 'ADMINISTRADOR';
      $user->email = 'admin@oximed.mx';
      $user->password = bcrypt('oximed2018');
      $user->save();
      $user->assignRole('ADMINISTRADOR');
    }
}
