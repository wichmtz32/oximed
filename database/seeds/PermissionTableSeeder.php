<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;


class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Permission::create(['name' => 'a_usuario']);
      Permission::create(['name' => 'b_usuario']);
      Permission::create(['name' => 'm_usuario']);
      Permission::create(['name' => 'c_usuario']);

      Permission::create(['name' => 'a_cliente']);
      Permission::create(['name' => 'b_cliente']);
      Permission::create(['name' => 'm_cliente']);
      Permission::create(['name' => 'c_cliente']);

      Permission::create(['name' => 'a_role']);
      Permission::create(['name' => 'b_role']);
      Permission::create(['name' => 'm_role']);
      Permission::create(['name' => 'c_role']);

      Permission::create(['name' => 'a_contrato']);
      Permission::create(['name' => 'b_contrato']);
      Permission::create(['name' => 'm_contrato']);
      Permission::create(['name' => 'c_contrato']);

      Permission::create(['name' => 'a_conductor']);
      Permission::create(['name' => 'b_conductor']);
      Permission::create(['name' => 'm_conductor']);
      Permission::create(['name' => 'c_conductor']);

      Permission::create(['name' => 'a_producto']);
      Permission::create(['name' => 'b_producto']);
      Permission::create(['name' => 'm_producto']);
      Permission::create(['name' => 'c_producto']);

      Permission::create(['name' => 'a_pagos']);
      Permission::create(['name' => 'b_pagos']);
      Permission::create(['name' => 'm_pagos']);
      Permission::create(['name' => 'c_pagos']);
      Permission::create(['name' => 'v_pagos']);
    }
}
