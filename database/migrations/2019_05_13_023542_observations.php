<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Observations.
 *
 * @author  The scaffold-interface created at 2019-05-13 02:35:42pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Observations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('observations',function (Blueprint $table){

        $table->increments('id');

        $table->String('description');
        $table->date('fecha');

        /**
         * Foreignkeys section
         */

        $table->integer('repair_id')->unsigned()->nullable();
        $table->foreign('repair_id')->references('id')->on('repairs')->onDelete('cascade');


        $table->timestamps();


        $table->softDeletes();

        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('observations');
    }
}
