<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Repairdetails.
 *
 * @author  The scaffold-interface created at 2019-05-13 02:32:03pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Repairdetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('repairdetails',function (Blueprint $table){

        $table->increments('id');
        $table->String('description');

        $table->float('unitPrice');

        $table->integer('quantity')->nullable()->default(null);

        $table->float('total')->nullable()->default(null);

        $table->String('status')->nullable()->default(null);

        $table->boolean('warranty')->nullable()->default(null);

        /**
         * Foreignkeys section
         */

        $table->integer('repair_id')->unsigned()->nullable();
        $table->foreign('repair_id')->references('id')->on('repairs')->onDelete('cascade');


        $table->timestamps();


        $table->softDeletes();

        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('repairdetails');
    }
}
