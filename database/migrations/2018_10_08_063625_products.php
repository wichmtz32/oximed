<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Products.
 *
 * @author  The scaffold-interface created at 2018-10-24 04:37:41pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('products',function (Blueprint $table){
        $table->increments('id');
        $table->String('name');
        $table->float('cost');
        $table->float('costmonth');
        $table->float('product_value');
        $table->float('extraprice1')->nullable();
        $table->float('extraprice2')->nullable();
        $table->float('extraprice3')->nullable();
        $table->float('extraprice4')->nullable();

        /**
         * Foreignkeys section
         */
        $table->timestamps();
        $table->softDeletes();
        // type your addition here
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('products');
    }


}
