<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Drivers.
 *
 * @author  The scaffold-interface created at 2018-10-08 06:20:03pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Drivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('drivers',function (Blueprint $table){

        $table->increments('id');
        $table->String('name');
        $table->String('address')->nullable();
        $table->String('schedule')->nullable();
        $table->float('salary')->nullable();
        $table->String('phone')->nullable();
        $table->String('cellphone')->nullable();
        $table->String('unity')->nullable();



        /**
         * Foreignkeys section
         */

        $table->timestamps();
        $table->softDeletes();

        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('drivers');
    }
}
