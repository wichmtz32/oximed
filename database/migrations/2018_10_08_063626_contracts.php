<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Contracts.
 *
 * @author  The scaffold-interface created at 2018-10-08 06:36:26pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Contracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('contracts',function (Blueprint $table){
        $table->increments('id');
        $table->String('folio');
        $table->date('date');
        $table->String('status');
        $table->String('charge');
        $table->String('validity');
        $table->date('recolect_date')->nullable()->default(null);
        $table->String('client');
        $table->String('address');
        $table->String('phone');
        $table->String('cellphone')->nullable();
        $table->String('notes')->nullable();
        $table->String('user')->nullable();
        $table->float('topay')->nullable();
        $table->float('cost');
        $table->float('flete')->nullable();
        $table->float('deposit')->nullable();
        $table->integer('driver_id')->unsigned()->nullable();
        $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
        $table->integer('driver_id2')->unsigned()->nullable();
        $table->foreign('driver_id2')->references('id')->on('drivers')->onDelete('cascade');
        $table->timestamps();
        $table->softDeletes();
        // type your addition here
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('contracts');
    }
}
