<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Orders.
 *
 * @author  The scaffold-interface created at 2019-05-27 01:28:39pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('orders',function (Blueprint $table){
        $table->increments('id');
        $table->integer('folio');
        $table->String('client');
        $table->longText('address');
        $table->String('phone');
        $table->String('cellphone');
        $table->date('date');
        $table->date('deliverDate')->nullable()->default(null);
        $table->timestamps();
        $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
