<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Repairs.
 *
 * @author  The scaffold-interface created at 2019-05-13 02:23:56pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Repairs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('repairs',function (Blueprint $table){

        $table->increments('id');

        $table->integer('order_id')->unsigned()->nullable();
        $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');


        $table->String('client')->nullable()->default(null);
        $table->String('address')->nullable()->default(null);
        $table->String('phone')->nullable()->nullable()->default(null);
        $table->String('cellphone')->nullable()->default(null);
        $table->date('date')->nullable()->default(null);

        $table->String('product');

        $table->String('serialNumber');

        $table->String('faults');
        $table->date('deliverDate')->nullable()->default(null);
        $table->String('status');
        $table->String('tipo')->nullable()->default(null);
        $table->String('repairman');
        $table->float('pago_reparador')->nullable()->default(null);

        /**
         * Foreignkeys section
         */


        $table->timestamps();


        $table->softDeletes();

        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('repairs');
    }
}
