<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Serials.
 *
 * @author  The scaffold-interface created at 2019-01-30 05:32:11pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Serials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('serials',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('number');
        
        /**
         * Foreignkeys section
         */
        
        $table->integer('product_id')->unsigned()->nullable();
        $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        
        
        $table->timestamps();
        
        
        $table->softDeletes();
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('serials');
    }
}
