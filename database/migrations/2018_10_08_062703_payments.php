<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Payments.
 *
 * @author  The scaffold-interface created at 2018-10-08 06:27:03pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Payments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('payments',function (Blueprint $table){
        $table->increments('id');
        $table->date('date_payment');
        $table->String('period');
        $table->String('bill');
        $table->String('notes');
        $table->float('amount');
        $table->String('validate');
        $table->String('unity');


        /**
         * Foreignkeys section
         */
         $table->integer('contract_id')->unsigned();
        $table->timestamps();
        $table->softDeletes();

        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
