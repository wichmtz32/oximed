<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Invoices.
 *
 * @author  The scaffold-interface created at 2019-05-31 03:37:33pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Invoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('invoices',function (Blueprint $table){

        $table->increments('id');
        
        $table->integer('invoiceNumber');

        $table->String('type');

        $table->String('person_id');
        
        /**
         * Foreignkeys section
         */
        
        
        $table->timestamps();
        
        
        $table->softDeletes();
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
