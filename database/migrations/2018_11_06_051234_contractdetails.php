<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Contractdetails.
 *
 * @author  The scaffold-interface created at 2018-11-06 05:12:34pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Contractdetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('contractdetails',function (Blueprint $table){
        $table->increments('id');
        $table->float('cost');
        $table->float('product_value');
        $table->String('extra');
        $table->String('serie')->nullable();
        /**
         * Foreignkeys section
         */
        $table->integer('contract_id')->unsigned()->nullable();
        $table->foreign('contract_id')->references('id')->on('contracts')->onDelete('cascade');
        $table->integer('product_id')->unsigned()->nullable();
        $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        $table->timestamps();
        $table->softDeletes();

        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('contractdetails');
    }
}
