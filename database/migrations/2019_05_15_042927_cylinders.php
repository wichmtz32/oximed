<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Cylinders.
 *
 * @author  The scaffold-interface created at 2019-05-15 04:29:30pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Cylinders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('cylinders',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('sizeOrWeight');
        
        $table->String('description');
        
        $table->String('quantity');
        
        /**
         * Foreignkeys section
         */
        
        
        $table->timestamps();
        
        
        $table->softDeletes();
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('cylinders');
    }
}
